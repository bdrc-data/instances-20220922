@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW20749 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG50FB8155A92C74F3
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG53D7F28C61CCC44D
        a                   adm:UpdateData ;
        adm:logDate         "2013-02-26T12:57:22.475000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type and source printery"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW20749  a          adm:AdminData ;
        adm:adminAbout      bdr:MW20749 ;
        adm:facetIndex      16 ;
        adm:gitPath         "8e/MW20749.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW20749 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG50FB8155A92C74F3 , bda:LG53D7F28C61CCC44D , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CRA72EBA16B0066F1F
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P617 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV57151B7B8683CB19
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1978/1985"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID6A02E68D94244B29
        a                   bdr:HarvardShelfId ;
        rdf:value           "008196266-5" .
    
    bdr:ID96877D6B37797725
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7679.4 .P325 1978" .
    
    bdr:IDF3D6A34BE1B9EFE8
        a                   bf:Lccn ;
        rdf:value           "78905650" .
    
    bdr:IDF68831A0868423E1
        a                   bdr:HollisId ;
        rdf:value           "014258220" .
    
    bdr:MW20749  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID6A02E68D94244B29 , bdr:ID96877D6B37797725 , bdr:IDF3D6A34BE1B9EFE8 , bdr:IDF68831A0868423E1 ;
        bdo:authorshipStatement  "collected and arranged by la-dwags khrid-dpon 'khrul-zig padma-chos-rgyal"@en ;
        bdo:biblioNote      "carved on to xylographic blocks at la-stod rtsib-ri from 1934 through about 1958 ; reproduced from a set of prints from the rtsib-ri blocks"@en ;
        bdo:creator         bdr:CRA72EBA16B0066F1F ;
        bdo:extentStatement  "31 v." ;
        bdo:hasSourcePrintery  bdr:G3JT12552 ;
        bdo:hasTitle        bdr:TT1776AD1412649F25 , bdr:TT22751477ADAE4DDF , bdr:TTB5B5A88593544641 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV57151B7B8683CB19 ;
        bdo:instanceHasReproduction  bdr:W20749 ;
        bdo:instanceOf      bdr:WA20749 ;
        bdo:note            bdr:NT3A5B3315157FC14D ;
        bdo:numberOfVolumes  31 ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:publisherLocation  "darjeeling"@en ;
        bdo:publisherName   "kargyu sungrab nyamso khang"@en ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "rtsib-ri spar-ma : the collected instructional material on the practice of the teachings of the dkar-brgyud-pa and rdzogs-chen traditions"@en-x-mixed ;
        skos:prefLabel      "dkar rnying gi skyes chen du ma'i phyag rdzogs kyi gdams ngag gnad bsdus nyer mkho rin po che'i gter mdzod/（rtsibs ri'i par ma/）"@bo-x-ewts .
    
    bdr:NT3A5B3315157FC14D
        a                   bdo:Note ;
        bdo:noteText        "-1294"@en .
    
    bdr:TT1776AD1412649F25
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dkar rnying gi skyes chen du ma'i phyag rdzogs kyi gdams ngag gnad bsdus nyer mkho rin po che'i gter mdzod/（rtsibs ri'i par ma/）"@bo-x-ewts .
    
    bdr:TT22751477ADAE4DDF
        a                   bdo:CoverTitle ;
        rdfs:label          "Dkar Rñiṅ gi skyes chen du maʾi Phyag Rdzogs kyi gdams ṅag gnad bsdus ñer mkho rin po cheʾi gter mdzod Rtsibs-riʾi par ma."@en-x-mixed .
    
    bdr:TTB5B5A88593544641
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rtsib-ri spar-ma : the collected instructional material on the practice of the teachings of the dkar-brgyud-pa and rdzogs-chen traditions"@en-x-mixed .
}
