@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21627 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG9FDB2C8AE61E6435
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGA56F6C9217B79D8B
        a                   adm:UpdateData ;
        adm:logDate         "2008-06-19T09:33:15.593000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00003 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21627  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21627 ;
        adm:facetIndex      10 ;
        adm:gitPath         "31/MW21627.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21627 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG9FDB2C8AE61E6435 , bda:LGA56F6C9217B79D8B , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV62E4BBA26383F83F
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2001"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID96F630A5F48E094E
        a                   bdr:HollisId ;
        rdf:value           "014258369" .
    
    bdr:IDMW21627_LALIE5AGF0H1
        a                   bf:Isbn ;
        rdf:value           "7542107704" .
    
    bdr:IDMW21627_OR5OQ8U4FEI4
        a                   bf:Isbn ;
        rdf:value           "9787542107701" .
    
    bdr:MW21627  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID96F630A5F48E094E , bdr:IDMW21627_LALIE5AGF0H1 , bdr:IDMW21627_OR5OQ8U4FEI4 ;
        bdo:authorshipStatement  "smin gling gra phyi 'gyur med rab rgyas kyis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:extentStatement  "6, 153 p." ;
        bdo:hasTitle        bdr:TT1F930CC2DC2EF8EC , bdr:TT9133A225E8F77219 ;
        bdo:illustrations   "ill." ;
        bdo:instanceEvent   bdr:EV62E4BBA26383F83F ;
        bdo:instanceHasReproduction  bdr:W21627 ;
        bdo:instanceOf      bdr:WA21627 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "zang zu hui hua yi shu"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "bod kyi srol rgyun ri mo'i skor brjod pa'i ngo mtshar gtam phreng /"@bo-x-ewts .
    
    bdr:TT1F930CC2DC2EF8EC
        a                   bdo:CoverTitle ;
        rdfs:label          "zang zu hui hua yi shu"@zh-latn-pinyin-x-ndia .
    
    bdr:TT9133A225E8F77219
        a                   bdo:Title ;
        rdfs:label          "bod kyi srol rgyun ri mo'i skor brjod pa'i ngo mtshar gtam phreng /"@bo-x-ewts .
}
