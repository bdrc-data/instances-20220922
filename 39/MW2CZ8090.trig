@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW2CZ8090 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG9C483C5BEE53133A
        a                   adm:InitialDataCreation ;
        adm:logDate         "2010-01-21T15:42:59.437000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:LGDF189E8CBBD14B3E
        a                   adm:UpdateData ;
        adm:logDate         "2013-06-18T11:39:52.427000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added title page title"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW2CZ8090  a        adm:AdminData ;
        adm:adminAbout      bdr:MW2CZ8090 ;
        adm:facetIndex      16 ;
        adm:gitPath         "39/MW2CZ8090.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW2CZ8090 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG9C483C5BEE53133A , bda:LGDF189E8CBBD14B3E , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVE647171D9E009405
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1988"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID05FED4B173B130D5
        a                   bdr:HollisId ;
        rdf:value           "014260347" .
    
    bdr:ID67D5A53ACC4B75ED
        a                   bf:ShelfMarkLcc ;
        rdf:value           "DS731.T56 W4618 1988" .
    
    bdr:ID6F214E9BA79569B6
        a                   bf:Isbn ;
        rdf:value           "7540900733" .
    
    bdr:IDCD5858769E9A1FED
        a                   bf:Lccn ;
        rdf:value           "93927620" .
    
    bdr:MW2CZ8090  a        bdo:Instance ;
        bf:identifiedBy     bdr:ID05FED4B173B130D5 , bdr:ID67D5A53ACC4B75ED , bdr:ID6F214E9BA79569B6 , bdr:IDCD5858769E9A1FED ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_TypeSet ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:extentStatement  "5, 198 p. ; 19 cm." ;
        bdo:hasTitle        bdr:TT6BE4F7E4E58000AD , bdr:TTA16A6941289ECFB8 , bdr:TTA3D95E20A04DC189 , bdr:TTD50AD67C2AFFCDC8 ;
        bdo:instanceEvent   bdr:EVE647171D9E009405 ;
        bdo:instanceHasReproduction  bdr:W2CZ8090 ;
        bdo:instanceOf      bdr:WA2CZ8090 ;
        bdo:note            bdr:NT8C66090ABE1BDCBC , bdr:NTDB1D7AD6FA147767 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "chengdu"@en ;
        bdo:publisherName   "si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "藏族史话"@zh-hans ;
        skos:prefLabel      "kha ba ri pa'i lo rgyus/"@bo-x-ewts .
    
    bdr:NT8C66090ABE1BDCBC
        a                   bdo:Note ;
        bdo:noteText        "copy made available from the library of tibetan works and archives"@en .
    
    bdr:NTDB1D7AD6FA147767
        a                   bdo:Note ;
        bdo:noteText        "in tibetan"@en .
    
    bdr:TT6BE4F7E4E58000AD
        a                   bdo:Title ;
        rdfs:label          "kha ba ri pa'i lo rgyus/"@bo-x-ewts .
    
    bdr:TTA16A6941289ECFB8
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "zang zu shi hua"@zh-latn-pinyin-x-ndia .
    
    bdr:TTA3D95E20A04DC189
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "藏族史话"@zh-hans .
    
    bdr:TTD50AD67C2AFFCDC8
        a                   bdo:TitlePageTitle ;
        rdfs:label          "kha ba ri pa'i lo rgyus yid dga'i phreng ba/"@bo-x-ewts .
}
