@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG21706 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG46BF1C79C071B70B
        a                   adm:InitialDataCreation ;
        adm:logDate         "2015-04-17T14:41:31.485000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:MW1KG21706  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG21706 ;
        adm:facetIndex      11 ;
        adm:gitPath         "73/MW1KG21706.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG21706 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG46BF1C79C071B70B ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVAE01F46B122DD0FD
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1999"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID03026546B1907328
        a                   bf:Lccn ;
        rdf:value           "2001294725" .
    
    bdr:ID27F9193DBED3A61D
        a                   bf:ShelfMarkLcc ;
        rdf:value           "R127.5 .G92 1999" .
    
    bdr:IDMW1KG21706_NC3TR01JRMDX
        a                   bf:Isbn ;
        rdf:value           "9787542106933" .
    
    bdr:IDMW1KG21706_PPO494ZSOBZK
        a                   bf:Isbn ;
        rdf:value           "7542106937" .
    
    bdr:MW1KG21706  a       bdo:Instance ;
        bf:identifiedBy     bdr:ID03026546B1907328 , bdr:ID27F9193DBED3A61D , bdr:IDMW1KG21706_NC3TR01JRMDX , bdr:IDMW1KG21706_PPO494ZSOBZK ;
        bdo:authorshipStatement  "g.yu thog gsar ma yon tan mgon pos mdzad/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2 v." ;
        bdo:hasTitle        bdr:TT393AFCF4FD03E3C0 , bdr:TT80FF0297EBAA7ED2 , bdr:TT85ECEAA04FF0ED53 , bdr:TTA5BFB6932D39FF6D ;
        bdo:instanceEvent   bdr:EVAE01F46B122DD0FD ;
        bdo:instanceHasReproduction  bdr:W1KG21706 ;
        bdo:instanceOf      bdr:WA1GS11 ;
        bdo:note            bdr:NTDC89A9125D78CEAD ;
        bdo:numberOfVolumes  2 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "藏医十八分支"@zh-hans ;
        skos:prefLabel      "cha lag bco brgyad/"@bo-x-ewts .
    
    bdr:NTDC89A9125D78CEAD
        a                   bdo:Note ;
        bdo:noteText        "copy made available from the library of tibetan works and archives; ltwa acc. no. 8757, 8758"@en .
    
    bdr:TT393AFCF4FD03E3C0
        a                   bdo:OtherTitle ;
        rdfs:label          "g.yu thog cha lag bco brgyad/"@bo-x-ewts .
    
    bdr:TT80FF0297EBAA7ED2
        a                   bdo:Title ;
        rdfs:label          "cha lag bco brgyad/"@bo-x-ewts .
    
    bdr:TT85ECEAA04FF0ED53
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "zang yi shi ba fen zhi"@zh-latn-pinyin-x-ndia .
    
    bdr:TTA5BFB6932D39FF6D
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "藏医十八分支"@zh-hans .
}
