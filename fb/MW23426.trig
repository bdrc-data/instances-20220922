@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW23426 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG1AA5ACDA6F03D2A9
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-23T10:16:44.154000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGA0319B9244F74119
        a                   adm:UpdateData ;
        adm:logDate         "2012-02-01T10:33:04.370000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added catalog info: Made available through Tibetan Buddhist Resource Center with authorization by His Holiness The 33rd Menri Trizin Lungtok Tenpai Nyima and the gracious support of the Bon Foundation"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LGADF41F838E8964F4
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW23426  a          adm:AdminData ;
        adm:adminAbout      bdr:MW23426 ;
        adm:facetIndex      13 ;
        adm:gitPath         "fb/MW23426.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW23426 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG1AA5ACDA6F03D2A9 , bda:LGA0319B9244F74119 , bda:LGADF41F838E8964F4 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV0250A0CDE113BC28
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1982"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:IDACAEBFCBBADB4716
        a                   bdr:HollisId ;
        rdf:value           "014258903" .
    
    bdr:IDBEF603AF3AAFCD3C
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7980.2 .S46 1982" .
    
    bdr:IDED7289B1715C6715
        a                   bf:Lccn ;
        rdf:value           "83900575" .
    
    bdr:MW23426  a          bdo:Instance ;
        bf:identifiedBy     bdr:IDACAEBFCBBADB4716 , bdr:IDBEF603AF3AAFCD3C , bdr:IDED7289B1715C6715 ;
        bdo:authorshipStatement  "by mnam-med ses-rab-rgyal-mtshan and bstan-'dzin-rnam-dag"@en ;
        bdo:biblioNote      "reproduced from rare manuscripts from the library of the bonpo monastic centre"@en ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:extentStatement  "572 p." ;
        bdo:hasTitle        bdr:TTA377F4A95C7C4456 , bdr:TTDE00419ECC132D1D ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV0250A0CDE113BC28 ;
        bdo:instanceHasReproduction  bdr:W23426 ;
        bdo:instanceOf      bdr:WA23426 ;
        bdo:note            bdr:NT92EB51E989EDC347 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "dolanji, h.p."@en ;
        bdo:publisherName   "bonpo monastic centre"@en ;
        bdo:script          bdr:ScriptDbuCan ;
        bdo:serialInstanceOf  bdr:WAS23424 ;
        skos:altLabel       "the bon po approach in abhidharma : texts from the sman-ri yig-cha"@en-x-mixed ;
        skos:prefLabel      "bon po'i yig cha las mngon pa'i skor/"@bo-x-ewts .
    
    bdr:NT92EB51E989EDC347
        a                   bdo:Note ;
        bdo:noteText        "scanned by khedup gyatso for the sman ri monastery and the bon foundation usa."@en .
    
    bdr:TTA377F4A95C7C4456
        a                   bdo:Title ;
        rdfs:label          "bon po'i yig cha las mngon pa'i skor/"@bo-x-ewts .
    
    bdr:TTDE00419ECC132D1D
        a                   bdo:TitlePageTitle ;
        rdfs:label          "the bon po approach in abhidharma : texts from the sman-ri yig-cha"@en-x-mixed .
}
