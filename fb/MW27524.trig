@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW27524 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGF716FBF2522538FA
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW27524  a          adm:AdminData ;
        adm:adminAbout      bdr:MW27524 ;
        adm:facetIndex      9 ;
        adm:gitPath         "fb/MW27524.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW27524 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LGF716FBF2522538FA , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV7F8162EE5F1FA835
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2001"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID7B40B379BCEF1EFE
        a                   bdr:HollisId ;
        rdf:value           "014259683" .
    
    bdr:IDMW27524_20Y7BDZDKGY4
        a                   bf:Isbn ;
        rdf:value           "7540921498" .
    
    bdr:IDMW27524_8VPUOGMCSTK1
        a                   bf:Isbn ;
        rdf:value           "9787540921491" .
    
    bdr:MW27524  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID7B40B379BCEF1EFE , bdr:IDMW27524_20Y7BDZDKGY4 , bdr:IDMW27524_8VPUOGMCSTK1 ;
        bdo:authorshipStatement  "si tu chos kyi rgya mtshos brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:extentStatement  "14, 544 p." ;
        bdo:hasTitle        bdr:TT599DC282567548CE , bdr:TT6E1C0063E7AAB366 ;
        bdo:instanceEvent   bdr:EV7F8162EE5F1FA835 ;
        bdo:instanceHasReproduction  bdr:IE27524 , bdr:W27524 ;
        bdo:instanceOf      bdr:WA27524 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:prefLabel      "dbus gtsang gnas yig"@bo-x-ewts .
    
    bdr:TT599DC282567548CE
        a                   bdo:Title ;
        rdfs:label          "dbus gtsang gnas yig"@bo-x-ewts .
    
    bdr:TT6E1C0063E7AAB366
        a                   bdo:TitlePageTitle ;
        rdfs:label          "kaH thog si tu'i dbus gtsang gnas yig"@bo-x-ewts .
}
