@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW30191 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGACDC7E69765BAA7B
        a                   adm:UpdateData ;
        adm:logDate         "2013-03-21T10:35:12.765000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type and source printery"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGE09BB04404B0D8FD
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW30191  a          adm:AdminData ;
        adm:adminAbout      bdr:MW30191 ;
        adm:facetIndex      15 ;
        adm:gitPath         "6f/MW30191.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW30191 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LGACDC7E69765BAA7B , bda:LGE09BB04404B0D8FD , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV20B661D310ED2285
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1970"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID2E057708FF28F6A0
        a                   bdr:HollisId ;
        rdf:value           "014260694" .
    
    bdr:ID5287F8C9AB022E36
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ5405 .D45 1970" .
    
    bdr:ID7B7B8FD01108C3E5
        a                   bf:Lccn ;
        rdf:value           "78912761" .
    
    bdr:IDD7F8F6CD1F679D61
        a                   bdr:HarvardShelfId ;
        rdf:value           "002407931-6" .
    
    bdr:MW30191  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID2E057708FF28F6A0 , bdr:ID5287F8C9AB022E36 , bdr:ID7B7B8FD01108C3E5 , bdr:IDD7F8F6CD1F679D61 ;
        bdo:biblioNote      "1947 print, \"po ta la'i zhol bka' 'gyur par khang du yig thob 'dra min rnams kyang 'bras spungs par rnying nyid sor bzhag spar slog zhus te gsar\""@en ;
        bdo:extentStatement  "1121 columns, [1] p." ;
        bdo:hasSourcePrintery  bdr:G1PD129185 ;
        bdo:hasTitle        bdr:TT2021D8B91EC6305E , bdr:TT383AC1BD663793C8 , bdr:TT3A7C984818E6222F , bdr:TT3F23171AD3AF9825 ;
        bdo:illustrations   "ill." ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV20B661D310ED2285 ;
        bdo:instanceHasReproduction  bdr:W30191 ;
        bdo:instanceOf      bdr:WA1KG5182 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:publisherLocation  "delhi"@en ;
        bdo:publisherName   "neychhung & lhakhar"@en ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "canonical stories exemplifying the conduct to be emulated by followers of lord buddha"@en ;
        skos:prefLabel      "'dul ba'i gleng 'bum/"@bo-x-ewts .
    
    bdr:TT2021D8B91EC6305E
        a                   bdo:Subtitle ;
        rdfs:label          "legs par gsungs pa'i dam pa'i chos 'dul ba'i gleng gzhi dang rtogs pa brjod pa lung sde bzhi kun las btus pa rin po che'i mdzod/"@bo-x-ewts .
    
    bdr:TT383AC1BD663793C8
        a                   bdo:Title ;
        rdfs:label          "'dul ba'i gleng 'bum/"@bo-x-ewts .
    
    bdr:TT3A7C984818E6222F
        a                   bdo:CoverTitle ;
        rdfs:label          "ʾDul ba gleng ʾbum :Legs par gsungs paʾi dam paʾi chos ʾdul baʾi gleng gzhi dang rtogs pa brjod pa lung sde bzhi kun las btus pa rin po cheʾi mdzod ; canonical stories exemplifying the conduct to be emulated by followers of Lord Buddha /retold by His Holiness Dge-ʾdun-grub."@en-x-mixed .
    
    bdr:TT3F23171AD3AF9825
        a                   bdo:Subtitle ;
        rdfs:label          "canonical stories exemplifying the conduct to be emulated by followers of lord buddha"@en .
}
