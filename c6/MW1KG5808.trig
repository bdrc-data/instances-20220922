@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG5808 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG318A3DD774DE27F9
        a                   adm:InitialDataCreation ;
        adm:logDate         "2010-08-24T13:06:39.351000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG5808  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1KG5808 ;
        adm:facetIndex      10 ;
        adm:gitPath         "c6/MW1KG5808.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG5808 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG318A3DD774DE27F9 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV257B3F8FAA78F49B
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2009"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:IDEC6A25ABA3B5FE4E
        a                   bf:Isbn ;
        rdf:value           "9787540943462" .
    
    bdr:IDEE96C2C604D65970
        a                   bdr:HollisId ;
        rdf:value           "014257199" .
    
    bdr:MW1KG5808  a        bdo:Instance ;
        bf:identifiedBy     bdr:IDEC6A25ABA3B5FE4E , bdr:IDEE96C2C604D65970 ;
        bdo:authorshipStatement  "ngag dbang tshogs gnyis rgya mtshos brtsams/"@bo-x-ewts ;
        bdo:biblioNote      "printed from computerized input"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "5, 11, 11, 367 p." ;
        bdo:hasTitle        bdr:TT351C8084DB06FBB4 , bdr:TTB44DF13D945A5554 ;
        bdo:instanceEvent   bdr:EV257B3F8FAA78F49B ;
        bdo:instanceHasReproduction  bdr:W1KG5808 ;
        bdo:instanceOf      bdr:WA1KG5808 ;
        bdo:note            bdr:NTCDE1FF093C7BD7AB ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron dpe skrun tshogs pa/ si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG5808 ;
        skos:prefLabel      "ngag dbang tshogs gnyis rgya mtsho'i dbu ma gzhan stong phyogs bsgrigs/"@bo-x-ewts , "jue nang cuo nie jia cuo wen ji"@zh-latn-pinyin-x-ndia .
    
    bdr:NTCDE1FF093C7BD7AB
        a                   bdo:Note ;
        bdo:noteText        "biography of ngawang tsoknyi gyatso is written in english and tibetan at the beginning of the text"@en .
    
    bdr:TT351C8084DB06FBB4
        a                   bdo:ColophonTitle ;
        rdfs:label          "jue nang cuo nie jia cuo wen ji"@zh-latn-pinyin-x-ndia .
    
    bdr:TTB44DF13D945A5554
        a                   bdo:Title ;
        rdfs:label          "ngag dbang tshogs gnyis rgya mtsho'i dbu ma gzhan stong phyogs bsgrigs/"@bo-x-ewts .
}
