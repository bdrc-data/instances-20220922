@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG16565 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG2881B871B7BEBEA4
        a                   adm:UpdateData ;
        adm:logDate         "2017-04-20T00:02:15.911000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00021 .
    
    bda:LG62CF59CD7631109E
        a                   adm:UpdateData ;
        adm:logDate         "2017-03-28T14:15:51.889000+00:00"^^xsd:dateTime ;
        adm:logMessage      "reviewed and released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGA411DE8C65B33EAE
        a                   adm:UpdateData ;
        adm:logDate         "2016-11-22T02:14:40.750000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00021 .
    
    bda:LGF5BB88678826BD9B
        a                   adm:InitialDataCreation ;
        adm:logDate         "2014-01-23T10:40:50.050000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:MW1KG16565  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG16565 ;
        adm:facetIndex      12 ;
        adm:gitPath         "6a/MW1KG16565.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG16565 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG2881B871B7BEBEA4 , bda:LG62CF59CD7631109E , bda:LGA411DE8C65B33EAE , bda:LGF5BB88678826BD9B , bda:LGIM6B433BA01E ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV38BF7272EB7B7F7C
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2012"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:IDDCAE3123052D9713
        a                   bf:Isbn ;
        rdf:value           "9787542117809" .
    
    bdr:MW1KG16565  a       bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:IDDCAE3123052D9713 ;
        bdo:authorshipStatement  "gu ru rgyal mtshan gyis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "1, 2, 1, 1, 204 p." ;
        bdo:hasTitle        bdr:TT04B0602CEEFA644B , bdr:TT2B04AA191AB5D80F , bdr:TTA1D4D89C8EEB8BEC , bdr:TTD994B9D5E5AB822A ;
        bdo:instanceEvent   bdr:EV38BF7272EB7B7F7C ;
        bdo:instanceHasReproduction  bdr:W1KG16565 ;
        bdo:instanceOf      bdr:WA1KG16565 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG16562 ;
        bdo:seriesNumber    "[not available]" ;
        skos:altLabel       "木雅竹子宗"@zh-hans ;
        skos:prefLabel      "mi nyag smyug rdzong /"@bo-x-ewts .
    
    bdr:TT04B0602CEEFA644B
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "mu ya zhu zi zong"@zh-latn-pinyin-x-ndia .
    
    bdr:TT2B04AA191AB5D80F
        a                   bdo:Title ;
        rdfs:label          "mi nyag smyug rdzong /"@bo-x-ewts .
    
    bdr:TTA1D4D89C8EEB8BEC
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "木雅竹子宗"@zh-hans .
    
    bdr:TTD994B9D5E5AB822A
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "'dzam gling skyes mchog seng chen nor bu dgra 'dul gyi rnam par thar ba mu tig phreng mdzes las/ mi nyag smyug rdzong /"@bo-x-ewts .
}
