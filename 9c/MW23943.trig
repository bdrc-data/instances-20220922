@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW23943 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG2482D1B608A11A90
        a                   adm:UpdateData ;
        adm:logDate         "2013-03-07T10:56:18.626000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type and source printery"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LG3C870FE16FAE75EB
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG55F6269FF7A05760
        a                   adm:UpdateData ;
        adm:logDate         "2012-06-11T12:00:30.316000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added full title"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW23943  a          adm:AdminData ;
        adm:adminAbout      bdr:MW23943 ;
        adm:facetIndex      14 ;
        adm:gitPath         "9c/MW23943.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW23943 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG2482D1B608A11A90 , bda:LG3C870FE16FAE75EB , bda:LG55F6269FF7A05760 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV80075E0CBD8A261C
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1980"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID011F1A019903BA9E
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7679.4 .M4 1980" .
    
    bdr:ID770CB72D68A320FA
        a                   bdr:HollisId ;
        rdf:value           "014259251" .
    
    bdr:IDAAE82BE73DF1FB4A
        a                   bf:Lccn ;
        rdf:value           "81900492" .
    
    bdr:MW23943  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID011F1A019903BA9E , bdr:ID770CB72D68A320FA , bdr:IDAAE82BE73DF1FB4A ;
        bdo:authorshipStatement  "by h.h. the eighth karma-pa mi-bskyod-rdo-rje"@en ;
        bdo:biblioNote      "reproduced from prints from the dpal-spuns blocks from rumtek monastery"@en ;
        bdo:extentStatement  "633 p." ;
        bdo:hasSourcePrintery  bdr:G3JT12497 ;
        bdo:hasTitle        bdr:TT8325D2B3F7C1272A , bdr:TT8C34DAD97E0536D8 , bdr:TTC5F6CB61ECC0DD74 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV80075E0CBD8A261C ;
        bdo:instanceHasReproduction  bdr:W23943 ;
        bdo:instanceOf      bdr:WA23943 ;
        bdo:note            bdr:NTE4D0734CB7B42A3D ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:publisherLocation  "delhi"@en ;
        bdo:publisherName   "delhi karmapae chodhey gyalwae sungrab partun khang"@en ;
        bdo:script          bdr:ScriptTibt ;
        bdo:workPrintery    "1533-1541?"@bo-x-ewts ;
        skos:altLabel       "rlun sems dbyer med kyi khrid yig chen mo : a presentation with explanatory annotations of a treasured upadesa of the kam-tshan bka'-brgyud-pa tradition for the practice of the na ro chos drug"@en ;
        skos:prefLabel      "rlung sems dbyer med kyi khrid yig"@bo-x-ewts .
    
    bdr:NTE4D0734CB7B42A3D
        a                   bdo:Note ;
        bdo:noteText        "this work was written over a 10 year period, from the author's 27th to 36th years"@en .
    
    bdr:TT8325D2B3F7C1272A
        a                   bdo:Title ;
        rdfs:label          "rlung sems dbyer med kyi khrid yig"@bo-x-ewts .
    
    bdr:TT8C34DAD97E0536D8
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rlun sems dbyer med kyi khrid yig chen mo : a presentation with explanatory annotations of a treasured upadesa of the kam-tshan bka'-brgyud-pa tradition for the practice of the na ro chos drug"@en .
    
    bdr:TTC5F6CB61ECC0DD74
        a                   bdo:FullTitle ;
        rdfs:label          "rje btsun dam pa dbyangs can bzang pos nye bar stsal ba'i rlung sems dbyer med kyi khrid/"@bo-x-ewts .
}
