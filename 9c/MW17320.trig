@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW17320 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG713A255A25C2390D
        a                   adm:UpdateData ;
        adm:logDate         "2008-06-19T09:09:07.859000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bzo bcos brgyab"@en ;
        adm:logWho          bdu:U00003 .
    
    bda:LG7BE5AFE294CBFF14
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGD955C8BE757371F0
        a                   adm:UpdateData ;
        adm:logDate         "2021-03-02T10:15:02.231000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added pub info and changed topics"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW17320  a          adm:AdminData ;
        adm:adminAbout      bdr:MW17320 ;
        adm:facetIndex      12 ;
        adm:gitPath         "9c/MW17320.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW17320 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG713A255A25C2390D , bda:LG7BE5AFE294CBFF14 , bda:LGD955C8BE757371F0 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVBC63062C6A99A0ED
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1987,1996"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID63678AED42C0AFD2
        a                   bdr:HollisId ;
        rdf:value           "014254787" .
    
    bdr:IDDCF64A90813E126A
        a                   bf:Isbn ;
        rdf:value           "754090027X" .
    
    bdr:MW17320  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID63678AED42C0AFD2 , bdr:IDDCF64A90813E126A ;
        bdo:authorshipStatement  "gter chen U rgyan gling pas gter nas bton pa/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "13, 792 p." ;
        bdo:hasTitle        bdr:TT14D47DDC34903304 , bdr:TTBD7DBE0A127C5E53 ;
        bdo:illustrations   "ill. col." ;
        bdo:instanceEvent   bdr:EVBC63062C6A99A0ED ;
        bdo:instanceHasReproduction  bdr:W17320 ;
        bdo:instanceOf      bdr:WA17320 ;
        bdo:note            bdr:NT4A31B5CB8103C771 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:prefLabel      "pad+ma bka' thang /"@bo-x-ewts .
    
    bdr:NT4A31B5CB8103C771
        a                   bdo:Note ;
        bdo:noteText        "\"Sde dge par khang gi shing brkos par ma ltar bsgrigs nas dpar du bskrun pa...\"\nle'u 108.\nZur bkod 1. Slob dpon chen po padma 'byung gnas kyi gsol 'debs le'u bdun pa.  pp. 723-772.\nZur bkod 2. Rten bskyed skor. pp. [773]-777.\nZur bkod 3. Gu ru rin po che'i tshig bdun gsol 'debs sogs. pp. [779]-792."@en .
    
    bdr:TT14D47DDC34903304
        a                   bdo:TitlePageTitle ;
        rdfs:label          "u rgyan gu ru pad+ma 'byung gnas kyi skyes rabs rnam par thar pa rgyas par bkod pa/"@bo-x-ewts .
    
    bdr:TTBD7DBE0A127C5E53
        a                   bdo:Title ;
        rdfs:label          "pad+ma bka' thang /"@bo-x-ewts .
}
