@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW22010 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG3E0F8FC35AB49050
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW22010  a          adm:AdminData ;
        adm:adminAbout      bdr:MW22010 ;
        adm:facetIndex      11 ;
        adm:gitPath         "e1/MW22010.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW22010 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG3E0F8FC35AB49050 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVF27A5BF75AAF7B93
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1979,1992printing"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID2394D3B831EBAEB6
        a                   bf:Lccn ;
        rdf:value           "93927615" .
    
    bdr:ID556C144832CDAAB3
        a                   bdr:HollisId ;
        rdf:value           "014258554" .
    
    bdr:ID89A947AF31C384F3
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3611 .B73 1979" .
    
    bdr:IDA89EBA3E88E5426A
        a                   bf:Isbn ;
        rdf:value           "7540908542" .
    
    bdr:MW22010  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID2394D3B831EBAEB6 , bdr:ID556C144832CDAAB3 , bdr:ID89A947AF31C384F3 , bdr:IDA89EBA3E88E5426A ;
        bdo:authorshipStatement  "dmu dge bsam gtan gyis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "ti 1 pan/"@bo-x-ewts ;
        bdo:extentStatement  "5, 257 p." ;
        bdo:hasTitle        bdr:TTE651601E856B4B8E , bdr:TTE8C53A334699C0E7 ;
        bdo:instanceEvent   bdr:EVF27A5BF75AAF7B93 ;
        bdo:instanceHasReproduction  bdr:W22010 ;
        bdo:instanceOf      bdr:WA1KG14786 , bdr:WA22010 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS19959 ;
        skos:altLabel       "tsang wen wen fa kai lun"@zh-latn-wadegile ;
        skos:prefLabel      "bod kyi yi ge'i spyi rnam blo gsal 'jug ngogs/"@bo-x-ewts .
    
    bdr:TTE651601E856B4B8E
        a                   bdo:Title ;
        rdfs:label          "bod kyi yi ge'i spyi rnam blo gsal 'jug ngogs/"@bo-x-ewts .
    
    bdr:TTE8C53A334699C0E7
        a                   bdo:ColophonTitle ;
        rdfs:label          "tsang wen wen fa kai lun"@zh-latn-wadegile .
}
