@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21990 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGC5B008CC0AD53856
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21990  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21990 ;
        adm:facetIndex      12 ;
        adm:gitPath         "98/MW21990.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21990 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LGC5B008CC0AD53856 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVEE143CCD59E4F7F6
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1997"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID15461BCD4FFE691B
        a                   bdr:HarvardShelfId ;
        rdf:value           "008611066-7" .
    
    bdr:ID88206755F6000191
        a                   bdr:HollisId ;
        rdf:value           "014258539" .
    
    bdr:IDB211EB5360B15EDA
        a                   bf:Isbn ;
        rdf:value           "7542105558" .
    
    bdr:MW21990  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID15461BCD4FFE691B , bdr:ID88206755F6000191 , bdr:IDB211EB5360B15EDA ;
        bdo:authorshipStatement  "cha ris skal bzang thogs med/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "6, 221 p." ;
        bdo:hasTitle        bdr:TTAE9C788829525C4C , bdr:TTC04B753AB2F29AC6 , bdr:TTC69058F29AB4F4D6 ;
        bdo:instanceEvent   bdr:EVEE143CCD59E4F7F6 ;
        bdo:instanceHasReproduction  bdr:W21990 ;
        bdo:instanceOf      bdr:WA21990 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS21916 ;
        skos:altLabel       "Rgyal ba'i bstan pa rin po che 'dzam gliṅ gi yul gru so sor dar ba'i tshul mdor bsdus su bkod pa dris lan nor bu'i gter mdzod ces bya ba bźugs so /Cha-ris Skal-bzaṅ-thogs-med."@en-x-mixed ;
        skos:prefLabel      "rgyal ba'i bstan pa rin po che 'dzam gling gi yul gru so sor dar ba'i tshul mdor bsdus su bkod pa dris lan nor bu'i gter mdzod/"@bo-x-ewts .
    
    bdr:TTAE9C788829525C4C
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rgyal ba'i bstan pa rin po che 'dzam gling gi yul gru so sor dar ba'i tshul mdor bsdus su bkod pa dris lan nor bu'i gter mdzod/"@bo-x-ewts .
    
    bdr:TTC04B753AB2F29AC6
        a                   bdo:CoverTitle ;
        rdfs:label          "Rgyal ba'i bstan pa rin po che 'dzam gliṅ gi yul gru so sor dar ba'i tshul mdor bsdus su bkod pa dris lan nor bu'i gter mdzod ces bya ba bźugs so /Cha-ris Skal-bzaṅ-thogs-med."@en-x-mixed .
    
    bdr:TTC69058F29AB4F4D6
        a                   bdo:Title ;
        rdfs:label          "rgya bstan yul gru so sor dar tshul/"@bo-x-ewts .
}
