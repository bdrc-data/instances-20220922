@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1PD137833 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG22C77D1D10C2194F
        a                   adm:UpdateData ;
        adm:logDate         "2021-03-30T17:10:31.841000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added info"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG43425664F53F8A31
        a                   adm:UpdateData ;
        adm:logDate         "2013-03-28T13:06:44.399000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00021 .
    
    bda:LG85732A3D6A162B19
        a                   adm:UpdateData ;
        adm:logDate         "2016-11-29T21:04:03.052000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00021 .
    
    bda:LGC12E7DA91E6AA5BE
        a                   adm:InitialDataCreation ;
        adm:logDate         "2012-08-30T15:36:30.849000+00:00"^^xsd:dateTime ;
        adm:logMessage      "acquiring"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:MW1PD137833  a      adm:AdminData ;
        adm:adminAbout      bdr:MW1PD137833 ;
        adm:facetIndex      10 ;
        adm:gitPath         "c3/MW1PD137833.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1PD137833 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG22C77D1D10C2194F , bda:LG43425664F53F8A31 , bda:LG85732A3D6A162B19 , bda:LGC12E7DA91E6AA5BE , bda:LGIM6B433BA01E ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV9C0C456790E88473
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2009"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:IDMW1PD137833_K4SCFRJBTAU9
        a                   bf:Isbn ;
        rdf:value           "9787540941871" .
    
    bdr:MW1PD137833  a      bdo:Instance ;
        bf:identifiedBy     bdr:IDMW1PD137833_K4SCFRJBTAU9 ;
        bdo:authorshipStatement  "《甘孜藏族自治州教育志》编委会 编"@zh-hans ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightUndetermined ;
        bdo:editionStatement  "第一版"@zh-hans ;
        bdo:extentStatement  "4, 3, 267 P." ;
        bdo:hasTitle        bdr:TT2880137358681E70 , bdr:TTAF92D432F42FA489 , bdr:TTDB42659E657486AD ;
        bdo:instanceEvent   bdr:EV9C0C456790E88473 ;
        bdo:instanceHasReproduction  bdr:W1PD137833 ;
        bdo:instanceOf      bdr:WA1PD137833 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "成都"@zh-hans ;
        bdo:publisherName   "四川出版集团 四川民族出版社"@zh-hans ;
        bdo:script          bdr:ScriptHani ;
        skos:altLabel       "dkar mdzes bod rigs rang skyong khul slob gso'i lo rgyus deb ther/（1991-2005）"@bo-x-ewts ;
        skos:prefLabel      "甘孜藏族自治州教育志(1991-2005)"@zh-hans .
    
    bdr:TT2880137358681E70
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "甘孜藏族自治州教育志(1991-2005)"@zh-hans .
    
    bdr:TTAF92D432F42FA489
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "gan zi zang zu zi zhi zhou jiao yu zhi (1991-2005)"@zh-latn-pinyin .
    
    bdr:TTDB42659E657486AD
        a                   bdo:Title ;
        rdfs:label          "dkar mdzes bod rigs rang skyong khul slob gso'i lo rgyus deb ther/（1991-2005）"@bo-x-ewts .
}
