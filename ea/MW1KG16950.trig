@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG16950 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG3721FCDDC7F1E3E8
        a                   adm:InitialDataCreation ;
        adm:logDate         "2014-04-02T15:26:58.313000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG70560F08EB41A0F1
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-08T23:22:19.539000+00:00"^^xsd:dateTime ;
        adm:logMessage      "changed encoding value extendedWylie to In Tibetan"@en ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG16950  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG16950 ;
        adm:facetIndex      15 ;
        adm:gitPath         "ea/MW1KG16950.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG16950 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG3721FCDDC7F1E3E8 , bda:LG70560F08EB41A0F1 , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVCA71F379AFEC64D6
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1995"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID012B70C29CFED687
        a                   bdr:HarvardShelfId ;
        rdf:value           "011303463-6" .
    
    bdr:ID0F2167CD123D4E16
        a                   bdr:HollisId ;
        rdf:value           "014256435" .
    
    bdr:IDMW1KG16950_0HB6NQAPVH9Z
        a                   bf:Isbn ;
        rdf:value           "7105024828" .
    
    bdr:IDMW1KG16950_VDT7O6DHPQ9V
        a                   bf:Isbn ;
        rdf:value           "9787105024827" .
    
    bdr:MW1KG16950  a       bdo:Instance ;
        bf:identifiedBy     bdr:ID012B70C29CFED687 , bdr:ID0F2167CD123D4E16 , bdr:IDMW1KG16950_0HB6NQAPVH9Z , bdr:IDMW1KG16950_VDT7O6DHPQ9V ;
        bdo:authorshipStatement  "bod rang skyong srid gros lo rgyus rig gnas dpyad gzhi'i rgyu cha zhib 'jug u yon lhan khang gis rtsom sgrig byas pa/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightUndetermined ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "10, 7, 699 p." ;
        bdo:hasTitle        bdr:TT66D400BE36DF15A6 , bdr:TT74FC9193C00B66B6 , bdr:TTAD079617B4D1D161 , bdr:TTF49195E11B2CA7E1 , bdr:TTFE3214DA4792514E ;
        bdo:illustrations   "ill. (col. photos)" ;
        bdo:instanceEvent   bdr:EVCA71F379AFEC64D6 ;
        bdo:instanceHasReproduction  bdr:W1KG16950 ;
        bdo:instanceOf      bdr:WA1KG16950 ;
        bdo:note            bdr:NT3740D704D8366B27 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG16950 ;
        skos:altLabel       "Bod kyi ʼpho ʼgyur rlabs po che :bod-raṅ-skyoṅ-ljoṅs dbu brñes nas lo sum chu ʼkhor bar dran gso byed paʼi ched rtsom phyogs bsgrigs /Bod-raṅ-skyoṅ-loṅs srid gros lo rgyus rig gnas dpyad gźʼi rgyu źib ʼjug u yon lhan khaṅ gis rtsom sgrig byas pa."@en-x-mixed , "西藏巨变"@zh-hans ;
        skos:prefLabel      "bod kyi 'pho 'gyur rlabs po che/"@bo-x-ewts .
    
    bdr:NT3740D704D8366B27
        a                   bdo:Note ;
        bdo:noteText        "in tibetan"@en .
    
    bdr:TT66D400BE36DF15A6
        a                   bdo:Title ;
        rdfs:label          "bod kyi 'pho 'gyur rlabs po che/"@bo-x-ewts .
    
    bdr:TT74FC9193C00B66B6
        a                   bdo:CoverTitle ;
        rdfs:label          "Bod kyi ʼpho ʼgyur rlabs po che :bod-raṅ-skyoṅ-ljoṅs dbu brñes nas lo sum chu ʼkhor bar dran gso byed paʼi ched rtsom phyogs bsgrigs /Bod-raṅ-skyoṅ-loṅs srid gros lo rgyus rig gnas dpyad gźʼi rgyu źib ʼjug u yon lhan khaṅ gis rtsom sgrig byas pa."@en-x-mixed .
    
    bdr:TTAD079617B4D1D161
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "xi zang ju bian"@zh-latn-pinyin-x-ndia .
    
    bdr:TTF49195E11B2CA7E1
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bod rang skyong ljongs dbu brnyes nas lo sum cu 'khor bar dran gso byed pa'i ched rtsom phyogs bsgrigs/ bod kyi 'pho 'gyur rlabs po che/"@bo-x-ewts .
    
    bdr:TTFE3214DA4792514E
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "西藏巨变"@zh-hans .
}
