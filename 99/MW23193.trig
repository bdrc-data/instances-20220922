@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW23193 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG80D2F837C59111F0
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGFD5794595C91D987
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-15T14:04:11.033000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW23193  a          adm:AdminData ;
        adm:adminAbout      bdr:MW23193 ;
        adm:facetIndex      13 ;
        adm:gitPath         "99/MW23193.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW23193 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG80D2F837C59111F0 , bda:LGFD5794595C91D987 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV62D733BE0A632475
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1976"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID0036402850BA946E
        a                   bf:Lccn ;
        rdf:value           "77900728" .
    
    bdr:ID0D6778727D337321
        a                   bdr:HollisId ;
        rdf:value           "014258832" .
    
    bdr:IDCC9261E702D73015
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3748 .V4 1976" .
    
    bdr:MW23193  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID0036402850BA946E , bdr:ID0D6778727D337321 , bdr:IDCC9261E702D73015 ;
        bdo:biblioNote      "reproduced from a copy of an ancient manuscript belonging to khri-byang rin-po-che"@en ;
        bdo:extentStatement  "[93] p." ;
        bdo:hasTitle        bdr:TT1CC9CC0550D158B6 , bdr:TT41A7980BF2F9A989 , bdr:TT6C2127D366727789 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV62D733BE0A632475 ;
        bdo:instanceHasReproduction  bdr:W23193 ;
        bdo:instanceOf      bdr:WA23193 ;
        bdo:note            bdr:NT668AAB83E6DB29BC ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "delhi"@en ;
        bdo:publisherName   "ngawang sopa"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "ro dnos grub can gyi sgrun mkhas pa dan skye bo rnams kyi rna ba'i bcud len phun tshogs bde gter : a manuscript collection of twenty-four tibetan vetala stories"@en-x-mixed , "vetalapancavimsati. tibetan version (ro sgrung)"@sa-alalc97 ;
        skos:prefLabel      "ro dngos grub can gyi sgrung mkhas pa dang skye bo rnams kyi rna ba'i bcud len phun tshogs bde gter/"@bo-x-ewts .
    
    bdr:NT668AAB83E6DB29BC
        a                   bdo:Note ;
        bdo:noteText        "tbrc holds scanned images, tiffs and pdf files.\n0533."@en .
    
    bdr:TT1CC9CC0550D158B6
        a                   bdo:OtherTitle ;
        rdfs:label          "vetalapancavimsati. tibetan version (ro sgrung)"@sa-alalc97 .
    
    bdr:TT41A7980BF2F9A989
        a                   bdo:TitlePageTitle ;
        rdfs:label          "ro dnos grub can gyi sgrun mkhas pa dan skye bo rnams kyi rna ba'i bcud len phun tshogs bde gter : a manuscript collection of twenty-four tibetan vetala stories"@en-x-mixed .
    
    bdr:TT6C2127D366727789
        a                   bdo:Title ;
        rdfs:label          "ro dngos grub can gyi sgrung mkhas pa dang skye bo rnams kyi rna ba'i bcud len phun tshogs bde gter/"@bo-x-ewts .
}
