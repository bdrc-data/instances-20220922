@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG22233 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG344780CD233108C1
        a                   adm:InitialDataCreation ;
        adm:logDate         "2015-07-15T14:16:49.660000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:MW1KG22233  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG22233 ;
        adm:facetIndex      11 ;
        adm:gitPath         "06/MW1KG22233.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG22233 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG344780CD233108C1 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV4F0561A5C5E7D83C
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2001"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID2EECA222D093A715
        a                   bdr:HollisId ;
        rdf:value           "009675297" .
    
    bdr:IDE189C97369AC25AA
        a                   bf:Lccn ;
        rdf:value           "2002288901" .
    
    bdr:IDE1B421E29909D6C6
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ2910.C383 H55 2001" .
    
    bdr:MW1KG22233  a       bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID2EECA222D093A715 , bdr:IDE189C97369AC25AA , bdr:IDE1B421E29909D6C6 ;
        bdo:authorshipStatement  "translated and critically edited by gyaltsen namdrol, acarya; supervisor prof. ram shankar tripathi."@en ;
        bdo:biblioNote      "Sanskrit text with Tibetan version and Hindi translation."@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "1st ed."@en ;
        bdo:extentStatement  "57, 214 p." ;
        bdo:hasTitle        bdr:TT480E0A3569E59A30 , bdr:TT6C5D3BF6609B71FE , bdr:TT88A4860279AE3AC5 , bdr:TTFA8F054299D01A68 ;
        bdo:instanceEvent   bdr:EV4F0561A5C5E7D83C ;
        bdo:instanceHasReproduction  bdr:W1KG22233 ;
        bdo:instanceOf      bdr:WA1KG22233 ;
        bdo:note            bdr:NTA8B422933C715BD7 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "sarnath, varanasi, u.p."@zh-hans ;
        bdo:publisherName   "central institute of higher tibetan studies"@en ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG22205 ;
        bdo:seriesNumber    "50" ;
        skos:prefLabel      "bstod pa rnam pa bzhi/"@bo-x-ewts , "आचार्यनागार्जुनाविरचित:चतु:स्तव:"@sa-Deva , "catuhstavah of acarya nagarjuna"@zh-latn-pinyin-x-ndia .
    
    bdr:NTA8B422933C715BD7
        a                   bdo:Note ;
        bdo:noteText        "copy made available from the library of tibetan works and archives; ltwa acc. no. D29349"@en .
    
    bdr:TT480E0A3569E59A30
        a                   bdo:Title ;
        rdfs:label          "bstod pa rnam pa bzhi/"@bo-x-ewts .
    
    bdr:TT6C5D3BF6609B71FE
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "catuhstavah of acarya nagarjuna"@zh-latn-pinyin-x-ndia .
    
    bdr:TT88A4860279AE3AC5
        a                   bdo:OtherTitle ;
        rdfs:label          "आचार्यनागार्जुनाविरचित:चतु:स्तव:"@sa-Deva .
    
    bdr:TTFA8F054299D01A68
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bdag nyid chen po klu sgrub kyis mdzad pa'i bstod pa rnam pa bzhi/"@bo-x-ewts .
}
