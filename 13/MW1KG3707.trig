@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG3707 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG8FB0CE69E0E8EE47
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-01T12:50:43.782000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGB5FBC6EF62FAB2A0
        a                   adm:InitialDataCreation ;
        adm:logDate         "2009-10-19T15:50:14.625000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged and released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGEBA29785A4E5EFD7
        a                   adm:UpdateData ;
        adm:logDate         "2010-09-14T14:33:21.197000+00:00"^^xsd:dateTime ;
        adm:logMessage      "corrected spelling"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG3707  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1KG3707 ;
        adm:facetIndex      9 ;
        adm:gitPath         "13/MW1KG3707.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG3707 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG8FB0CE69E0E8EE47 , bda:LGB5FBC6EF62FAB2A0 , bda:LGEBA29785A4E5EFD7 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:ID5CC9111AF2A4BDF3
        a                   bdr:HollisId ;
        rdf:value           "014256725" .
    
    bdr:MW1KG3707  a        bdo:Instance ;
        bf:identifiedBy     bdr:ID5CC9111AF2A4BDF3 ;
        bdo:extentStatement  "1 v. (560 p.)" ;
        bdo:hasTitle        bdr:TTE7FD3BAC2BE14CD4 ;
        bdo:instanceHasReproduction  bdr:W1KG3707 ;
        bdo:instanceOf      bdr:WA1KG3707 ;
        bdo:note            bdr:NT661AFB723D4EAC3B ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "ladakh"@en ;
        bdo:publisherName   "*la dwags brag thog dgon/"@bo-x-ewts ;
        bdo:script          bdr:ScriptDbuCan ;
        skos:prefLabel      "bla ma mchod pa'i cho ga nor bu'i phreng ba sogs chos tshan khag cig"@bo-x-ewts .
    
    bdr:NT661AFB723D4EAC3B
        a                   bdo:Note ;
        bdo:noteText        "contains: 1. bla ma mchod pa'i cho ga nor bu'i phreng ba, 31 ff.;\n2. byang gter bag chags stobs sbyong gi bum sgrub dang lag len gsal byed me long, 19 ff.;\n3. byang gter spyan ras gzigs kyi sreg sbyang lag len gsal bar bkod pa snang byed nyi ma, 25 ff. + 1 ff.;\n4. byad 'grol gyi bum sgrub, 4 ff.;\n5. byad grol gser gyi lde mig gi lag len gsal byed nag po 'gro shes su bkod pa bklags pas don 'grub, 24 ff.;\n6. rta mgrin gsang sgrub kyi brgyud gsol dang las byang dkyus gcig tu bdebs pa rin chen mdzes pa'i phra tshom, 11 ff.;\n7. byang gter phur pa'i sri gnon gyis ngag 'don chog khrigs rdo rje'i lhun po, 20 ff.;\n8. phywa 'phrin nor bu mchog rgyal kyi zur 'debs, 5 ff. \n9. las tshogs rin chen khang bu las: phywa 'phrin nor bu mchog rgyal, 18 ff.;\n10. sgrub chen bka' brgyad rang byung rang shar gyi cha gsum gtor ma'i rim pa, 8 ff.;\n11. khrus gsol, 7 ff.;\n12. mkhyen gsol/ gzu dpang/ 'grub khung bskyed pa/ bden bdar/ tshe khrus dmigs pa bcas, 7 ff.;\n13. dpal rdo rje phur pa'i thugs kyi 'phrin las, 12 ff.;\n14. tshe sgrub bdud rtsi 'khyil ba'i cha rkyen las: bsangs yig bar chad kun sel, 10 ff.;\n15. rgyal po sku lnga 'khor bcas kyi gsol kha phrin las don bcu ma, 19 ff. \n16. bye chas, 4 ff.;\n17. slob dpon pad+ma saM b+hA bas mdzad pa'i mi kha dgra sgyur gyi gto, 6 ff.;\n18. shin tu gnyan pa ma mo'i khrag mdos, 5 ff.;\n19. thugs rje chen po 'gro ba kun grol dang 'brel ba'i gnas lung bya tshul gyi zin tho, 11 ff.;\n20. phyi sgrub bsnyen yig, 2 ff.;\n21. rig 'dzin gdung sgrub kyi bsnyen sgrub la mkho ba'i zin bris, 8 ff.;\n22. byang gter thugs sgrub drag po rtsal gyi las byang 'bring po dang 'brel ba'i nyin chog tshe bcu tshogs brgya'i zin bris, 4 ff.;\n23. bka' brgyad zin bris, 6 ff.;\n24. byang gter phur pa khro bo rol pa'i gter zlog dang 'brel ba'i smad las dgra bgegs sgrol ba'i gar 'chams kyi yi ge khrag 'thung rol pa'i dga' ston, 11 ff.;\n25. gnod sbyin rtsi'u dmar po ru 'dren sde bzhi dang bcas 'chams yig gdug pa tshar gcod, 2 ff."@en .
    
    bdr:TTE7FD3BAC2BE14CD4
        a                   bdo:Title ;
        rdfs:label          "bla ma mchod pa'i cho ga nor bu'i phreng ba sogs chos tshan khag cig"@bo-x-ewts .
}
