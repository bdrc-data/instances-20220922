@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW20084 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG164F7CD004507FB9
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-05T12:34:08.347000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added copyright title"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG240F2BA0E6C61424
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW20084  a          adm:AdminData ;
        adm:adminAbout      bdr:MW20084 ;
        adm:facetIndex      16 ;
        adm:gitPath         "13/MW20084.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW20084 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG164F7CD004507FB9 , bda:LG240F2BA0E6C61424 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CR44D3F2B385444576
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P7444 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:CR61C0B50729D3494E
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P7445 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV5DC9E52F452D68ED
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1991"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID3A40D2AD3A34D14B
        a                   bf:Lccn ;
        rdf:value           "95918914" .
    
    bdr:ID7C3D8FDD77C63CCF
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3748.G425 T47 1991" .
    
    bdr:ID92BDA4C5D367D2B8
        a                   bf:Isbn ;
        rdf:value           "7542002694" .
    
    bdr:IDFB5F71D0C8C73C40
        a                   bdr:HollisId ;
        rdf:value           "014258031" .
    
    bdr:MW20084  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID3A40D2AD3A34D14B , bdr:ID7C3D8FDD77C63CCF , bdr:ID92BDA4C5D367D2B8 , bdr:IDFB5F71D0C8C73C40 ;
        bdo:authorshipStatement  "bshad mkhan, tshe ring dbang 'dus/ 'dzin mda'i bu thub dga' dang chos skyong bkra shis gnyis kyis dag sgrig byas/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_TypeSet ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:creator         bdr:CR44D3F2B385444576 , bdr:CR61C0B50729D3494E ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "7, 513 p." ;
        bdo:hasTitle        bdr:TT2E9D125FA4DD86FE , bdr:TT2F61148C32346A70 , bdr:TT40C8F23BA63AD685 , bdr:TT4C095BB922BC0644 , bdr:TTA4AE8EA3679FAE0E ;
        bdo:instanceEvent   bdr:EV5DC9E52F452D68ED ;
        bdo:instanceHasReproduction  bdr:W20084 ;
        bdo:instanceOf      bdr:WA20084 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "Thog gliṅ gʾyul ʾgyed :Gliṅ Ge-sar rgyal poʾi sgruṅ /ʾDzin-mdaʾi-bu Thub-dgaʾ daṅ Chos-skyoṅ-bkra-śis gñis kyis dag sgrig byas ; [bśad mkhan Tshe-riṅ-dbaṅ-ʾdus]."@en-x-mixed , "格萨尔陀岭之战"@zh-hans ;
        skos:prefLabel      "thog gling g.yul 'gyed/"@bo-x-ewts .
    
    bdr:TT2E9D125FA4DD86FE
        a                   bdo:CoverTitle ;
        rdfs:label          "Thog gliṅ gʾyul ʾgyed :Gliṅ Ge-sar rgyal poʾi sgruṅ /ʾDzin-mdaʾi-bu Thub-dgaʾ daṅ Chos-skyoṅ-bkra-śis gñis kyis dag sgrig byas ; [bśad mkhan Tshe-riṅ-dbaṅ-ʾdus]."@en-x-mixed .
    
    bdr:TT2F61148C32346A70
        a                   bdo:Title ;
        rdfs:label          "thog gling g.yul 'gyed/"@bo-x-ewts .
    
    bdr:TT40C8F23BA63AD685
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "ge sha er tuo ling zhi zhan"@zh-latn-pinyin-x-ndia .
    
    bdr:TT4C095BB922BC0644
        a                   bdo:TitlePageTitle ;
        rdfs:label          "gling ge sar rgyal po'i sgrung thog gling rgyal po'i sgrung /"@bo-x-ewts .
    
    bdr:TTA4AE8EA3679FAE0E
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "格萨尔陀岭之战"@zh-hans .
}
