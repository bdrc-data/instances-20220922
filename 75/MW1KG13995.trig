@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG13995 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG66E25B54531412D5
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-07T15:14:34.735000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added loc numbers"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG883FD3CEFF1EE647
        a                   adm:InitialDataCreation ;
        adm:logDate         "2013-04-29T14:39:11.655000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG13995  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG13995 ;
        adm:facetIndex      13 ;
        adm:gitPath         "75/MW1KG13995.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG13995 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG66E25B54531412D5 , bda:LG883FD3CEFF1EE647 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV605519049D447BFC
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2012"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID7E95CE0A82552B13
        a                   bf:ShelfMarkLcc ;
        rdf:value           "MLCSC 2013/00053 (P) PL3748.B555" .
    
    bdr:IDE03DDB2CF5777066
        a                   bf:Lccn ;
        rdf:value           "2013318444" .
    
    bdr:IDE8032849912F19D9
        a                   bdr:HollisId ;
        rdf:value           "014256091" .
    
    bdr:IDMW1KG13995_VEVL7QLP2SDY
        a                   bf:Isbn ;
        rdf:value           "9789380359700" .
    
    bdr:MW1KG13995  a       bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID7E95CE0A82552B13 , bdr:IDE03DDB2CF5777066 , bdr:IDE8032849912F19D9 , bdr:IDMW1KG13995_VEVL7QLP2SDY ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:extentStatement  "vi, 161 p." ;
        bdo:hasTitle        bdr:TT4BC90357BA75C12B , bdr:TT7F32DCE4FDC7ACA8 , bdr:TT87CE482B3AEDC676 ;
        bdo:instanceEvent   bdr:EV605519049D447BFC ;
        bdo:instanceHasReproduction  bdr:W1KG13995 ;
        bdo:instanceOf      bdr:WA1KG13995 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "dharamsala, h.p."@en ;
        bdo:publisherName   "bod kyi dpe mdzod khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG13995 ;
        bdo:seriesNumber    "7" ;
        skos:altLabel       "a wrathful story of a beautiful lady"@en ;
        skos:prefLabel      "bzhin bzang ma gtum drag rol pa'i sgrung gtam/"@bo-x-ewts .
    
    bdr:TT4BC90357BA75C12B
        a                   bdo:Title ;
        rdfs:label          "bzhin bzang ma gtum drag rol pa'i sgrung gtam/"@bo-x-ewts .
    
    bdr:TT7F32DCE4FDC7ACA8
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bzhin bzang ma gtum drag rol pa'i sgrung gtam yi gar 'jo ba'i rol rtsed/"@bo-x-ewts .
    
    bdr:TT87CE482B3AEDC676
        a                   bdo:OtherTitle ;
        rdfs:label          "a wrathful story of a beautiful lady"@en .
}
