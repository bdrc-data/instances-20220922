@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW29562 {
    bda:LG03A0824B39DD1B07
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG166C5667235AAF26
        a                   adm:UpdateData ;
        adm:logDate         "2015-09-30T04:16:51.134000+00:00"^^xsd:dateTime ;
        adm:logMessage      "deleted inProduct PR1COPYRIGHT"@en ;
        adm:logWho          bdu:U00006 .
    
    bda:LGFB98705B0CFC69A5
        a                   adm:UpdateData ;
        adm:logDate         "2008-06-19T11:47:31.452000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00003 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW29562  a          adm:AdminData ;
        adm:adminAbout      bdr:MW29562 ;
        adm:facetIndex      15 ;
        adm:gitPath         "75/MW29562.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW29562 ;
        adm:logEntry        bda:LG03A0824B39DD1B07 , bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG166C5667235AAF26 , bda:LGFB98705B0CFC69A5 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVCBE1E90C136E653D
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1991"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID0264509E828E591E
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ6349.T552 B573 1991" .
    
    bdr:ID7434136438E8D9F4
        a                   bf:Isbn ;
        rdf:value           "7542002546" .
    
    bdr:IDADA4BF5CA471E4BD
        a                   bdr:HollisId ;
        rdf:value           "014260028" .
    
    bdr:IDD561AFA9BF7142B6
        a                   bf:Lccn ;
        rdf:value           "95900725" .
    
    bdr:MW29562  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID0264509E828E591E , bdr:ID7434136438E8D9F4 , bdr:IDADA4BF5CA471E4BD , bdr:IDD561AFA9BF7142B6 ;
        bdo:authorshipStatement  "bis pa 'jam dbyangs grags pa ... [et al.]"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "722 p." ;
        bdo:hasTitle        bdr:TT06C180B7C2AD3BB1 , bdr:TT1032DAB1F85C6B80 , bdr:TT31CDE2D4F5EB110C ;
        bdo:instanceEvent   bdr:EVCBE1E90C136E653D ;
        bdo:instanceHasReproduction  bdr:W29562 ;
        bdo:instanceOf      bdr:WA29562 ;
        bdo:note            bdr:NT0491BDF69C932EF1 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "*zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS10232 ;
        skos:altLabel       "wen bu si zhi"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "bis mdo dgon chen bkra shis thos bsam chos 'khor gling gi gdan rabs dad pa'i chu bo gzhol ba'i 'bab stegs/"@bo-x-ewts .
    
    bdr:NT0491BDF69C932EF1
        a                   bdo:Note ;
        bdo:noteSource      bdr:MW29562 ;
        bdo:noteText        "reference source for this important dge lugs monastery in a mdo; includes biographies of the succesors on the abbatial throne"@en .
    
    bdr:TT06C180B7C2AD3BB1
        a                   bdo:Title ;
        rdfs:label          "bis mdo dgon chen bkra shis thos bsam chos 'khor gling gi gdan rabs dad pa'i chu bo gzhol ba'i 'bab stegs/"@bo-x-ewts .
    
    bdr:TT1032DAB1F85C6B80
        a                   bdo:CoverTitle ;
        rdfs:label          "bis mdo dgon chen gyi gdan rabs/"@bo-x-ewts .
    
    bdr:TT31CDE2D4F5EB110C
        a                   bdo:ColophonTitle ;
        rdfs:label          "wen bu si zhi"@zh-latn-pinyin-x-ndia .
}
