@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW29101 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG4256C64385224CBD
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGC596026FB712243A
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-30T15:41:42.710000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGE55F39A7BA5C927B
        a                   adm:UpdateData ;
        adm:logDate         "2013-09-20T09:18:05.924000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added authorship statement and date of writing"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW29101  a          adm:AdminData ;
        adm:adminAbout      bdr:MW29101 ;
        adm:facetIndex      10 ;
        adm:gitPath         "1a/MW29101.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW29101 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG4256C64385224CBD , bda:LGC596026FB712243A , bda:LGE55F39A7BA5C927B , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV6203993ECC170E18
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "199-"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:IDF0130C6D2C286E73
        a                   bdr:HollisId ;
        rdf:value           "014259906" .
    
    bdr:MW29101  a          bdo:Instance ;
        bf:identifiedBy     bdr:IDF0130C6D2C286E73 ;
        bdo:authorshipStatement  "ཅེས་གཏན་ཚིགས་རིག་པའི་རྣམ་བཞག་གསལ་བར་བཤད་པ་རབ་གསལ་ནོར་བུའི་སྒྲོན་མེ་ཞེས་བྱ་བ་འདི་ནི་ཡང་རྒྱ་བོད་ཀྱི་གཏན་ཚིགས་རིག་པའི་གཞུང་རྣམས་ལ་བརྟགས་ཏེ་ངག་དབང་ཚོགས་གཉིས་རྒྱ་མཚོས་རབ་བྱུང་བཅུ་དྲུག་པའི་ས་ཕོ་སྟག་གི་ཞོར་དགུན་ཟླ་དགུ་པའི་ཡར་ཚེས་འགྲུབ་པའི་སྦྱོར་བ་ལ་རྫོགས་པར་སྦྱར་བའི་ཡི་གེ་པ་སློབ་རིང་པ་ངག་དབང་འཕྲིན་ལས་ཞེས་བྱ་བས་བགྱིས་པའོ༎"@bo , "ngag dbang tshogs gnyis rgya mtsho/"@bo-x-ewts ;
        bdo:extentStatement  "2 v." ;
        bdo:hasTitle        bdr:TT13A896203CC4E28B ;
        bdo:instanceEvent   bdr:EV6203993ECC170E18 ;
        bdo:instanceHasReproduction  bdr:W29101 ;
        bdo:instanceOf      bdr:WA29101 ;
        bdo:note            bdr:NTA282024B27B4E063 ;
        bdo:numberOfVolumes  2 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "*dzam thang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:prefLabel      "gtan tshigs rig pa'i rnam bzhag gsal bar bshad pa rab gsal nor bu'i sgron me/"@bo-x-ewts .
    
    bdr:NTA282024B27B4E063
        a                   bdo:Note ;
        bdo:noteText        "-5112"@en .
    
    bdr:TT13A896203CC4E28B
        a                   bdo:Title ;
        rdfs:label          "gtan tshigs rig pa'i rnam bzhag gsal bar bshad pa rab gsal nor bu'i sgron me/"@bo-x-ewts .
}
