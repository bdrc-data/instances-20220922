@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW10206 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0MW10206_QRQJBF512PUL
        a                   adm:UpdateData ;
        adm:logDate         "2024-08-29T17:31:15.660438Z"^^xsd:dateTime ;
        adm:logMessage      "corrected publisher name"@en ;
        adm:logWho          bdu:U1426146149 .
    
    bda:LG27414E35C3794B65
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG4C3236E1A7CD76BE
        a                   adm:UpdateData ;
        adm:logDate         "2010-07-19T10:20:50.082000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00017 .
    
    bda:LGCED4F9392D4C153E
        a                   adm:UpdateData ;
        adm:logDate         "2013-02-06T12:29:34.237000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added imagegroup of vol. 1, 4, 7"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW10206  a          adm:AdminData ;
        adm:adminAbout      bdr:MW10206 ;
        adm:facetIndex      17 ;
        adm:gitPath         "1a/MW10206.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW10206 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG0MW10206_QRQJBF512PUL , bda:LG27414E35C3794B65 , bda:LG4C3236E1A7CD76BE , bda:LGCED4F9392D4C153E , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV8D571C59336DF18A
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1985/1991"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID34222F6CEF322322
        a                   bdr:HollisId ;
        rdf:value           "014254603" .
    
    bdr:ID6D269D6CC8AA914C
        a                   bf:Lccn ;
        rdf:value           "85904970" .
    
    bdr:ID6E41E06AB95A85A4
        a                   bdr:HarvardShelfId ;
        rdf:value           "007412754-3" .
    
    bdr:IDB41CEEC1DC5664F4
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7550 .B73 1985" .
    
    bdr:MW10206  a          bdo:Instance ;
        skos:altLabel       "the collected works (gsun 'bum) of dge bses brag phug dge 'dun rin chen"@en ;
        skos:prefLabel      "gsung 'bum/_dge 'dun rin chen/"@bo-x-ewts , "The collected works (gsuṅ ʾbum) of Dge-bśes Brag-phug Dge-ʾdun-rin-chen."@en-x-mixed ;
        bf:identifiedBy     bdr:ID34222F6CEF322322 , bdr:ID6D269D6CC8AA914C , bdr:ID6E41E06AB95A85A4 , bdr:IDB41CEEC1DC5664F4 ;
        bdo:biblioNote      "reproduced with the permission of the author from his manuscripts"@en ;
        bdo:binding         bdr:Binding_LooseLeaf_Pecha ;
        bdo:copyrightStatus  bdr:CopyrightWaived ;
        bdo:extentStatement  "v. <1-8, 10>" ;
        bdo:hasTitle        bdr:TT381135820F53ABEE , bdr:TT4FAE148E880DDDB4 , bdr:TT9C42A135BEB49000 , bdr:TTAEF2DF3FD3F5D379 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV8D571C59336DF18A ;
        bdo:instanceHasReproduction  bdr:W10206 ;
        bdo:instanceOf      bdr:WA10206 ;
        bdo:material        bdr:MaterialPaper ;
        bdo:note            bdr:NT8E52744E6D319DEB ;
        bdo:numberOfVolumes  9 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "rewalsar, distt. mandi, h.p."@en ;
        bdo:publisherName   "Sherab Gyaltse Lama and Acharya Shedup Tenzin"@en ;
        bdo:script          bdr:ScriptDbuCan ;
        bdo:serialInstanceOf  bdr:WAS10206 ;
        bdo:seriesNumber    "v. 125-129, 171-174" .
    
    bdr:NT8E52744E6D319DEB
        a                   bdo:Note ;
        bdo:noteText        "tbrc hold digitally texts for 6 volumes"@en .
    
    bdr:TT381135820F53ABEE
        a                   bdo:BibliographicalTitle ;
        rdfs:label          "gsung 'bum/_dge 'dun rin chen/"@bo-x-ewts .
    
    bdr:TT4FAE148E880DDDB4
        a                   bdo:CoverTitle ;
        rdfs:label          "The collected works (sugṅ ʾbum) of Dge-bśes Brag-phug Dge-ʾdun-rin-chen."@en-x-mixed .
    
    bdr:TT9C42A135BEB49000
        a                   bdo:ToCTitle ;
        rdfs:label          "bshes gnyen chen po dge 'dun rin chen mchog gi gsung 'bum/"@bo-x-ewts .
    
    bdr:TTAEF2DF3FD3F5D379
        a                   bdo:TitlePageTitle ;
        rdfs:label          "the collected works (gsun 'bum) of dge bses brag phug dge 'dun rin chen"@en .
}
