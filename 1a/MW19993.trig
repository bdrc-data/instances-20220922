@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW19993 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGC39BCA4A02B8BF15
        a                   adm:UpdateData ;
        adm:logDate         "2007-02-22T14:08:53.735000+00:00"^^xsd:dateTime ;
        adm:logMessage      "ccl dang cln sogs gsar du bcug"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LGEE460D2109EE206B
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW19993  a          adm:AdminData ;
        adm:adminAbout      bdr:MW19993 ;
        adm:facetIndex      17 ;
        adm:gitPath         "1a/MW19993.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW19993 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LGC39BCA4A02B8BF15 , bda:LGEE460D2109EE206B , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV2D34127C803767DB
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1997"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID5D11E8D36A33A5BE
        a                   bf:ShelfMarkLcc ;
        rdf:value           "MLCSA 2000/03802 (R)" .
    
    bdr:IDAA4403E55F720324
        a                   bf:Lccn ;
        rdf:value           "98907510" .
    
    bdr:IDAA5EE04C858C516E
        a                   bdr:HollisId ;
        rdf:value           "014254940" .
    
    bdr:IDB3D546AC4CF8CAFB
        a                   bdr:HarvardShelfId ;
        rdf:value           "008098362-6" .
    
    bdr:IDMW19993_8PT630TFKEO3
        a                   bf:Isbn ;
        rdf:value           "7542105523" .
    
    bdr:IDMW19993_TF4MSI4ZP9G9
        a                   bf:Isbn ;
        rdf:value           "9787542105523" .
    
    bdr:MW19993  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID5D11E8D36A33A5BE , bdr:IDAA4403E55F720324 , bdr:IDAA5EE04C858C516E , bdr:IDB3D546AC4CF8CAFB , bdr:IDMW19993_8PT630TFKEO3 , bdr:IDMW19993_TF4MSI4ZP9G9 ;
        bdo:authorshipStatement  "bse mkhar kun dga' tshe ring /"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:extentStatement  "269 p." ;
        bdo:hasTitle        bdr:TT616A80B9C4FDE681 , bdr:TT7B8B5E3534B95033 , bdr:TTC12698369ADC7E01 , bdr:TTCAD6C8923E86B6FE ;
        bdo:instanceEvent   bdr:EV2D34127C803767DB ;
        bdo:instanceHasReproduction  bdr:W19993 ;
        bdo:instanceOf      bdr:WA19993 ;
        bdo:note            bdr:NT0F6061CCB670CB1C ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "a dictionary of tibetan medicine"@en , "zang yao ming ci ci dian"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "sman ming gi mdzod/"@bo-x-ewts .
    
    bdr:NT0F6061CCB670CB1C
        a                   bdo:Note ;
        bdo:noteText        "tbrc holds inkprint copy and digitally scanned images, tiffs and pdf files;"@en .
    
    bdr:TT616A80B9C4FDE681
        a                   bdo:CoverTitle ;
        rdfs:label          "Sman miṅ gu mdzod =A dictionary of Tibetan medicine /Bse-mkhar Kun-dgaʾ-tshe-riṅ."@en-x-mixed .
    
    bdr:TT7B8B5E3534B95033
        a                   bdo:CoverTitle ;
        rdfs:label          "a dictionary of tibetan medicine"@en .
    
    bdr:TTC12698369ADC7E01
        a                   bdo:Title ;
        rdfs:label          "sman ming gi mdzod/"@bo-x-ewts .
    
    bdr:TTCAD6C8923E86B6FE
        a                   bdo:ColophonTitle ;
        rdfs:label          "zang yao ming ci ci dian"@zh-latn-pinyin-x-ndia .
}
