@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW00EGS1016276 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG6C97EDB598563E20
        a                   adm:UpdateData ;
        adm:logDate         "2021-04-08T16:36:05.035000+00:00"^^xsd:dateTime ;
        adm:logMessage      "Added Author, Language encoding, print type"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:LGD32DB999C25D6504
        a                   adm:InitialDataCreation ;
        adm:logDate         "2006-11-13T18:30:17.343000+00:00"^^xsd:dateTime ;
        adm:logMessage      "first published"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW00EGS1016276
        a                   adm:AdminData ;
        adm:adminAbout      bdr:MW00EGS1016276 ;
        adm:facetIndex      15 ;
        adm:gitPath         "19/MW00EGS1016276.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW00EGS1016276 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG6C97EDB598563E20 , bda:LGD32DB999C25D6504 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV0EE64442404B52C5
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1975"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID0FF3D5F767E02AD1
        a                   bdr:PL480 ;
        rdf:value           "i tib 1397" .
    
    bdr:ID866C816F23799E21
        a                   bdr:HarvardShelfId ;
        rdf:value           "007366418-9" .
    
    bdr:ID86E26EE478E4CC5A
        a                   bf:ShelfMarkLcc ;
        rdf:value           "R603.T5 K37 1975" .
    
    bdr:ID8DB02614566689CB
        a                   bf:Lccn ;
        rdf:value           "75901333" .
    
    bdr:ID976C846DDBCA11D0
        a                   bdr:HollisId ;
        rdf:value           "014253829" .
    
    bdr:MW00EGS1016276
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:ID0FF3D5F767E02AD1 , bdr:ID866C816F23799E21 , bdr:ID86E26EE478E4CC5A , bdr:ID8DB02614566689CB , bdr:ID976C846DDBCA11D0 ;
        bdo:authorshipStatement  "by karma-chos-rgyal"@en ;
        bdo:biblioNote      "reproduced from a manuscript from gser-bzan dri-med."@en ;
        bdo:extentStatement  "611 columns" ;
        bdo:hasTitle        bdr:TT1479EF39BDB3D59A , bdr:TTB56EACB07AED15BE , bdr:TTEF43749B3005907F ;
        bdo:instanceEvent   bdr:EV0EE64442404B52C5 ;
        bdo:instanceHasReproduction  bdr:W00EGS1016276 ;
        bdo:instanceOf      bdr:WA00EGS1016276 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "new delhi"@en ;
        bdo:publisherName   "rta rna bla ma/"@bo-x-ewts ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "a treatise on tibetan medical practice containing numerous instructions and formulae"@en ;
        skos:prefLabel      "gso rig man ngag gces btus rin chen gter mdzod/"@bo-x-ewts .
    
    bdr:TT1479EF39BDB3D59A
        a                   bdo:Title ;
        rdfs:label          "gso rig man ngag gces btus rin chen gter mdzod/"@bo-x-ewts .
    
    bdr:TTB56EACB07AED15BE
        a                   bdo:Subtitle ;
        rdfs:label          "a treatise on tibetan medical practice containing numerous instructions and formulae"@en .
    
    bdr:TTEF43749B3005907F
        a                   bdo:CoverTitle ;
        rdfs:label          "Gso rig man ṅag gces btus Rin chen gter mdzod :a treatise on Tibetan medical practice containing numerous instructions and formulae /by Karma-chos-rgyal."@en-x-mixed .
}
