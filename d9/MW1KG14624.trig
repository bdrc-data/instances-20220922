@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG14624 {
    bda:LG0MW1KG14624_OSF16YTTST3Y
        a                   adm:UpdateData ;
        adm:logDate         "2024-02-05T17:02:13.775867Z"^^xsd:dateTime ;
        adm:logMessage      "added image W8LS19204"@en ;
        adm:logWho          bdu:U1426146149 .
    
    bda:LG0MW1KG14624_VFRA7VMO66ZV
        a                   adm:UpdateData ;
        adm:logDate         "2024-01-30T22:05:16.054705Z"^^xsd:dateTime ;
        adm:logMessage      "saved"@en ;
        adm:logWho          bdu:U2089842660 .
    
    bda:LG5AC5E5357557E06A
        a                   adm:UpdateData ;
        adm:logDate         "2014-01-08T15:27:15.856Z"^^xsd:dateTime ;
        adm:logMessage      "added copyright titles"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG995F36C052FD45A2
        a                   adm:InitialDataCreation ;
        adm:logDate         "2013-10-24T14:22:05.887Z"^^xsd:dateTime ;
        adm:logMessage      "released for scan request to india"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577Z"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356Z"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG14624  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG14624 ;
        adm:facetIndex      12 ;
        adm:gitPath         "d9/MW1KG14624.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG14624 ;
        adm:logEntry        bda:LG0MW1KG14624_OSF16YTTST3Y , bda:LG0MW1KG14624_VFRA7VMO66ZV , bda:LG5AC5E5357557E06A , bda:LG995F36C052FD45A2 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV578FB9712232B8AF
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2007"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:IDA5FD3E8ABF8303BB
        a                   bf:Isbn ;
        rdf:value           "9787105090402" .
    
    bdr:IDBD30D5EFF05DE0AB
        a                   bdr:HollisId ;
        rdf:value           "014256186" .
    
    bdr:MW1KG14624  a       bdo:Instance ;
        skos:altLabel       "利乐百万与精化; 药王宝库"@zh-hans ;
        skos:prefLabel      "gso dpyad rgyal po'i dkor mdzod dang phan bde 'bum phrag dang yang snying /"@bo-x-ewts , "li le bai wan yu jing hua; yao wang bao ku"@zh-latn-pinyin-x-ndia ;
        bf:identifiedBy     bdr:IDA5FD3E8ABF8303BB , bdr:IDBD30D5EFF05DE0AB ;
        bdo:authorshipStatement  "rje btsun grags pa rgyal mtshan/ sman rje bkra shis g.yang 'phel/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par dzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2, 3, 5, 8, 2, 475 p." ;
        bdo:hasTitle        bdr:TT00183B4DA9DB6B47 , bdr:TT69EDAB0CBEF314C6 , bdr:TTDBBC5210B18BA161 ;
        bdo:instanceEvent   bdr:EV578FB9712232B8AF ;
        bdo:instanceHasReproduction  bdr:W1KG14624 , bdr:W8LS19204 ;
        bdo:instanceOf      bdr:WA1KG14624 ;
        bdo:material        bdr:MaterialPaper ;
        bdo:note            bdr:NT4FAEC3C710F428A1 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptDbuCan ;
        bdo:serialInstanceOf  bdr:WAS1AC418 ;
        bdo:seriesNumber    "64" .
    
    bdr:NT4FAEC3C710F428A1
        a                   bdo:Note ;
        bdo:noteText        "in tibetan"@en .
    
    bdr:TT00183B4DA9DB6B47
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "li le bai wan yu jing hua; yao wang bao ku"@zh-latn-pinyin-x-ndia .
    
    bdr:TT69EDAB0CBEF314C6
        a                   bdo:OtherTitle ;
        rdfs:label          "利乐百万与精化; 药王宝库"@zh-hans .
    
    bdr:TTDBBC5210B18BA161
        a                   bdo:TitlePageTitle ;
        rdfs:label          "gso dpyad rgyal po'i dkor mdzod dang phan bde 'bum phrag dang yang snying /"@bo-x-ewts .
}
