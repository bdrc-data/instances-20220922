@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21975 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG8FB5EB9C052F919D
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21975  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21975 ;
        adm:facetIndex      12 ;
        adm:gitPath         "37/MW21975.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21975 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG8FB5EB9C052F919D , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV5C67A24F73220A0F
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1988"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID33C915942D07F628
        a                   bdr:HollisId ;
        rdf:value           "014258532" .
    
    bdr:ID504CAFA51F0E4D3E
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7989 .B577 B75 1988" .
    
    bdr:ID7E1FB9FBAAD4650F
        a                   bf:Isbn ;
        rdf:value           "7540901810" .
    
    bdr:IDFF81807A2872B27B
        a                   bf:Lccn ;
        rdf:value           "95903237" .
    
    bdr:MW21975  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID33C915942D07F628 , bdr:ID504CAFA51F0E4D3E , bdr:ID7E1FB9FBAAD4650F , bdr:IDFF81807A2872B27B ;
        bdo:authorshipStatement  "bskal bzang bstan pa'i rgyal mtshan/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "3, 107 p." ;
        bdo:hasTitle        bdr:TTDAADAF6A9F534240 , bdr:TTE1E2374CB8206144 , bdr:TTE5DDD21C1EA93FE5 ;
        bdo:instanceEvent   bdr:EV5C67A24F73220A0F ;
        bdo:instanceHasReproduction  bdr:W21975 ;
        bdo:instanceOf      bdr:WA21975 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "tsa hsi chi che chuan"@zh-latn-wadegile ;
        skos:prefLabel      "shar rdza ba bkra shis rgyal mtshan gyi rnam thar/"@bo-x-ewts .
    
    bdr:TTDAADAF6A9F534240
        a                   bdo:ColophonTitle ;
        rdfs:label          "tsa hsi chi che chuan"@zh-latn-wadegile .
    
    bdr:TTE1E2374CB8206144
        a                   bdo:ColophonTitle ;
        rdfs:label          "zaxijizhe zhuan"@zh-latn-pinyin-x-ndia .
    
    bdr:TTE5DDD21C1EA93FE5
        a                   bdo:Title ;
        rdfs:label          "shar rdza ba bkra shis rgyal mtshan gyi rnam thar/"@bo-x-ewts .
}
