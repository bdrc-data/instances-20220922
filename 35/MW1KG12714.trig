@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG12714 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG16333230F9860016
        a                   adm:UpdateData ;
        adm:logDate         "2012-11-14T15:13:00.278000+00:00"^^xsd:dateTime ;
        adm:logMessage      "changed accessioned to released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG72A65C03FAC87B81
        a                   adm:InitialDataCreation ;
        adm:logDate         "2012-08-07T14:55:57.753000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG12714  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG12714 ;
        adm:facetIndex      12 ;
        adm:gitPath         "35/MW1KG12714.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG12714 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG16333230F9860016 , bda:LG72A65C03FAC87B81 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV735BA420664EB32B
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1978"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID74B31E808C1B995F
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BF1714.T53 S26 1978" .
    
    bdr:ID79E0A2AE7F854F11
        a                   bf:Lccn ;
        rdf:value           "79901826" .
    
    bdr:ID8573EE1F9CB86F6D
        a                   bdr:HollisId ;
        rdf:value           "014255950" .
    
    bdr:MW1KG12714  a       bdo:Instance ;
        bf:identifiedBy     bdr:ID74B31E808C1B995F , bdr:ID79E0A2AE7F854F11 , bdr:ID8573EE1F9CB86F6D ;
        bdo:biblioNote      "reproduced from a set of prints from the 18th century sde-dge blocks from the library of klu-sding mkhan-po"@en ;
        bdo:extentStatement  "2 v." ;
        bdo:hasSourcePrintery  bdr:G1657 ;
        bdo:hasTitle        bdr:TT4E14D2170E2068E7 , bdr:TTD44D1D2FE0A026E9 , bdr:TTF485B472A2410DFC ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV735BA420664EB32B ;
        bdo:instanceHasReproduction  bdr:W1KG12714 ;
        bdo:instanceOf      bdr:WA1KG12714 ;
        bdo:numberOfVolumes  2 ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:publisherLocation  "dehradun, u.p."@en ;
        bdo:publisherName   "sakya centre"@en ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "phug lugs rtsis kyi legs bshad mkhas pa'i mgul rgyan baiDUr dkar po'i do sh al dpyod ldan snying nor : the basic tibetan treatise on the principles of astrology, astronomy, and calendrical calculations"@en ;
        skos:prefLabel      "bai DUrya dkar po/"@bo-x-ewts .
    
    bdr:TT4E14D2170E2068E7
        a                   bdo:Title ;
        rdfs:label          "bai DUrya dkar po/"@bo-x-ewts .
    
    bdr:TTD44D1D2FE0A026E9
        a                   bdo:TitlePageTitle ;
        rdfs:label          "phug lugs rtsis kyi legs bshad mkhas pa'i mgul rgyan baiDUr dkar po'i do sh al dpyod ldan snying nor : the basic tibetan treatise on the principles of astrology, astronomy, and calendrical calculations"@en .
    
    bdr:TTF485B472A2410DFC
        a                   bdo:FullTitle ;
        rdfs:label          "phug lugs rtsis kyi legs bshad mkhas pa'i mgul rgyan baiDUr dkar po'i do shal dpyod ldan snying nor/"@bo-x-ewts .
}
