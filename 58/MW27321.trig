@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW27321 {
    bda:LG0A193BEC7ABDA521
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGDC87AD9FE725C1D9
        a                   adm:UpdateData ;
        adm:logDate         "2021-03-09T18:26:42.144000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added about person and modify catalog"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW27321  a          adm:AdminData ;
        adm:adminAbout      bdr:MW27321 ;
        adm:facetIndex      13 ;
        adm:gitPath         "58/MW27321.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW27321 ;
        adm:logEntry        bda:LG0A193BEC7ABDA521 , bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LGDC87AD9FE725C1D9 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVA1C721FB7E4DA29F
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2004"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID7B1E46BBFD6C5AB3
        a                   bdr:HollisId ;
        rdf:value           "014259616" .
    
    bdr:IDC305D37D2AC2A7FD
        a                   bdr:HarvardShelfId ;
        rdf:value           "010022853-4" .
    
    bdr:IDMW27321_EUE4AG8SX6ZB
        a                   bf:Isbn ;
        rdf:value           "9787105061204" .
    
    bdr:IDMW27321_K6V91N94TURO
        a                   bf:Isbn ;
        rdf:value           "7105061200" .
    
    bdr:MW27321  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID7B1E46BBFD6C5AB3 , bdr:IDC305D37D2AC2A7FD , bdr:IDMW27321_EUE4AG8SX6ZB , bdr:IDMW27321_K6V91N94TURO ;
        bdo:authorshipStatement  "byang sems rgyal ba ye shes kyis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/ par thengs dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2, 2, 209 p." ;
        bdo:hasTitle        bdr:TT077DA76DF3B0B45A , bdr:TTE0C77909331C6467 , bdr:TTE7286F9CEA6D7045 ;
        bdo:illustrations   "ports." ;
        bdo:instanceEvent   bdr:EVA1C721FB7E4DA29F ;
        bdo:instanceHasReproduction  bdr:W27321 ;
        bdo:instanceOf      bdr:WA27321 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "Dpal ldan dus kyi ʾkhor lo Jo-naṅ-paʾi lugs kyi bla ma rgyud paʾi rnam thar /Byaṅ-sems-rgyal-ba-ye-śes kyi brtsams."@en-x-mixed ;
        skos:prefLabel      "dus 'khor jo nang lugs kyi bla ma brgyud pa'i rnam thar/"@bo-x-ewts .
    
    bdr:TT077DA76DF3B0B45A
        a                   bdo:CoverTitle ;
        rdfs:label          "Dpal ldan dus kyi ʾkhor lo Jo-naṅ-paʾi lugs kyi bla ma rgyud paʾi rnam thar /Byaṅ-sems-rgyal-ba-ye-śes kyi brtsams."@en-x-mixed .
    
    bdr:TTE0C77909331C6467
        a                   bdo:Title ;
        rdfs:label          "dus 'khor jo nang lugs kyi bla ma brgyud pa'i rnam thar/"@bo-x-ewts .
    
    bdr:TTE7286F9CEA6D7045
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dpal ldan dus kyi 'khor lo jo nang pa'i lugs kyi bla ma brgyud pa'i rnam thar/"@bo-x-ewts .
}
