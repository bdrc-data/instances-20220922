@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW12034 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGD5F21BFA960CC730
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW12034  a          adm:AdminData ;
        adm:adminAbout      bdr:MW12034 ;
        adm:facetIndex      15 ;
        adm:gitPath         "7c/MW12034.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW12034 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LGD5F21BFA960CC730 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVD92D38FA2575CC4A
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1996"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID3E027C01C6DAE75B
        a                   bf:Lccn ;
        rdf:value           "97915907" .
    
    bdr:ID814607FE9AF29F45
        a                   bdr:HollisId ;
        rdf:value           "014254659" .
    
    bdr:IDC7D04BE57D0B87D0
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ942.L57 B56 1996" .
    
    bdr:IDMW12034_6XY1ULK7T07D
        a                   bf:Isbn ;
        rdf:value           "7542005421" .
    
    bdr:IDMW12034_AFR6SXS3W67J
        a                   bf:Isbn ;
        rdf:value           "9787542005427" .
    
    bdr:MW12034  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID3E027C01C6DAE75B , bdr:ID814607FE9AF29F45 , bdr:IDC7D04BE57D0B87D0 , bdr:IDMW12034_6XY1ULK7T07D , bdr:IDMW12034_AFR6SXS3W67J ;
        bdo:authorshipStatement  "rnye blo bzang bstan pa'i rgyal mtshan and blo bzang bstan pa rgya mtshos brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "3, 250 p." ;
        bdo:hasTitle        bdr:TT0A273B1FFC6A05BE , bdr:TT46629E8C6F5B7BD9 , bdr:TTA648F6B093650360 , bdr:TTB2087CE331E338E9 , bdr:TTC7912F40010BA209 ;
        bdo:instanceEvent   bdr:EVD92D38FA2575CC4A ;
        bdo:instanceHasReproduction  bdr:W12034 ;
        bdo:instanceOf      bdr:WA12034 ;
        bdo:note            bdr:NT0641F717114AAA9A ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "sai k'ang wa lo sang tan tseng chia ts'o chuan"@zh-latn-wadegile ;
        skos:prefLabel      "blo bzang bstan 'dzin rgya mtsho'i rnam thar/"@bo-x-ewts .
    
    bdr:NT0641F717114AAA9A
        a                   bdo:Note ;
        bdo:noteText        "Reprinted from the Sku 'bum print.\nWritten at the behest of Blon-chos Ri-khrod-pa Bstan-'dzin-rgya-mtsho,  A-mdo Zhwa-dmar, A-lags Glang-rgya, Byang-btang-pa Dkon-mchog-blo-gros, Dge-slong Ngag-dbang-rgyal-mtshan, etc."@en .
    
    bdr:TT0A273B1FFC6A05BE
        a                   bdo:ColophonTitle ;
        rdfs:label          "sai k'ang wa lo sang tan tseng chia ts'o chuan"@zh-latn-wadegile .
    
    bdr:TT46629E8C6F5B7BD9
        a                   bdo:ColophonTitle ;
        rdfs:label          "saikangwa luosangdanzengjiacuo zhuan"@zh-latn-pinyin-x-ndia .
    
    bdr:TTA648F6B093650360
        a                   bdo:Title ;
        rdfs:label          "blo bzang bstan 'dzin rgya mtsho'i rnam thar/"@bo-x-ewts .
    
    bdr:TTB2087CE331E338E9
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rigs dang dkyil 'khor rgya mtsho'i mnga' bdag rje gser khang pa blo bzang bstan 'dzin rgya mtsho'i zhal snga nas kyi rnam par thar pa mdo tsam brjod pa thams cad mkhyen par bgrod pa'i them skas/"@bo-x-ewts .
    
    bdr:TTC7912F40010BA209
        a                   bdo:CoverTitle ;
        rdfs:label          "rje gser khang pa blo bzang bstan 'dzin rgya mtsho'i rnam thar/"@bo-x-ewts .
}
