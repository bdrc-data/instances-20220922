@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG3960 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BE7E9F667E62B4A
        a                   adm:InitialDataCreation ;
        adm:logDate         "2009-11-24T12:00:08.837000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG37C1AE8A6E5988AD
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-29T16:28:10.975000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG3960  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1KG3960 ;
        adm:facetIndex      14 ;
        adm:gitPath         "1d/MW1KG3960.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG3960 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG0BE7E9F667E62B4A , bda:LG37C1AE8A6E5988AD , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV61BBD836FB3084F5
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1973"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID180DB61FEEA4839B
        a                   bdr:HollisId ;
        rdf:value           "014256776" .
    
    bdr:ID6ADE1650A038B76A
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7982.3 .K46 1973" .
    
    bdr:IDA35CFBC635719692
        a                   bf:Lccn ;
        rdf:value           "73901943" .
    
    bdr:IDA8F7E8B6B4D8271C
        a                   bdr:HarvardShelfId ;
        rdf:value           "007366404-9" .
    
    bdr:MW1KG3960  a        bdo:Instance ;
        bf:identifiedBy     bdr:ID180DB61FEEA4839B , bdr:ID6ADE1650A038B76A , bdr:IDA35CFBC635719692 , bdr:IDA8F7E8B6B4D8271C ;
        bdo:biblioNote      "reproduced from a manuscript preserved at bsam-gliing monastery in dolpo (northwestern nepal) by tashi dorji"@en ;
        bdo:extentStatement  "1 v. (614 p.)" ;
        bdo:hasTitle        bdr:TT0D9057F4EDFA06E9 , bdr:TT32FED32BAB2D41AA , bdr:TT81795DDA9E77690C ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV61BBD836FB3084F5 ;
        bdo:instanceHasReproduction  bdr:W1KG3960 ;
        bdo:instanceOf      bdr:WA1KG3960 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "dolanji, h.p."@en ;
        bdo:publisherName   "tashi dorji"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "Zab lam mkhaʼ ʼgro gsaṅ baʼi gcod kyi gdams pa;a collection of Bonpo Gcod tantras (rgyud) and related texts,largely from the oral transmission received by Sprul-sku Khro-gñan-rgyal-mtshan. Reproduced from a MS. preserved at Bsam-gliṅ Monastery in Dol-po (northwestern Nepal) by Tashi Dorji."@en-x-mixed ;
        skos:prefLabel      "zab lam mkha' 'gro gsang ba'i gcod kyi gdams pa/"@bo-x-ewts .
    
    bdr:TT0D9057F4EDFA06E9
        a                   bdo:CoverTitle ;
        rdfs:label          "Zab lam mkhaʼ ʼgro gsaṅ baʼi gcod kyi gdams pa;a collection of Bonpo Gcod tantras (rgyud) and related texts,largely from the oral transmission received by Sprul-sku Khro-gñan-rgyal-mtshan. Reproduced from a MS. preserved at Bsam-gliṅ Monastery in Dol-po (northwestern Nepal) by Tashi Dorji."@en-x-mixed .
    
    bdr:TT32FED32BAB2D41AA
        a                   bdo:TitlePageTitle ;
        rdfs:label          "zab lam mkha' 'gro gsang ba'i gcod rgyud kyi gdams pa/"@bo-x-ewts .
    
    bdr:TT81795DDA9E77690C
        a                   bdo:Title ;
        rdfs:label          "zab lam mkha' 'gro gsang ba'i gcod kyi gdams pa/"@bo-x-ewts .
}
