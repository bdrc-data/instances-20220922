@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW30149 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGAC6A6A3CE5B0221D
        a                   adm:UpdateData ;
        adm:logDate         "2015-02-05T12:31:30.122000+00:00"^^xsd:dateTime ;
        adm:logMessage      "changed language"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGC184A969AC5530CC
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGDB5D48AB19BCA4C7
        a                   adm:UpdateData ;
        adm:logDate         "2013-08-05T09:47:15.098000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW30149  a          adm:AdminData ;
        adm:adminAbout      bdr:MW30149 ;
        adm:facetIndex      15 ;
        adm:gitPath         "30/MW30149.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW30149 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LGAC6A6A3CE5B0221D , bda:LGC184A969AC5530CC , bda:LGDB5D48AB19BCA4C7 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVCAE87AD3C5FD3563
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1985"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID09FE22F42DF9D04D
        a                   bf:Lccn ;
        rdf:value           "86900160" .
    
    bdr:IDBDD27E2F8C76EFDD
        a                   bdr:PL480 ;
        rdf:value           "i-tib-2865" .
    
    bdr:IDC595B68DC4571316
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7672.4 .D63 1985" .
    
    bdr:IDEB421804C2E829EE
        a                   bdr:HollisId ;
        rdf:value           "014260658" .
    
    bdr:MW30149  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID09FE22F42DF9D04D , bdr:IDBDD27E2F8C76EFDD , bdr:IDC595B68DC4571316 , bdr:IDEB421804C2E829EE ;
        bdo:authorshipStatement  "written by dmar-ston chos-rgyal on the basis of the teachings of his master, sa-skya pandita kun-dga'-rgyal-mtshan"@en ;
        bdo:biblioNote      "reproduced from a manuscript from the library of h.h. the sakya tridzin"@en ;
        bdo:extentStatement  "455 p." ;
        bdo:hasTitle        bdr:TTB4D803B9C0697516 , bdr:TTB7DCC3A33B4AC998 , bdr:TTEB9D3A0178CE4E05 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EVCAE87AD3C5FD3563 ;
        bdo:instanceHasReproduction  bdr:W30149 ;
        bdo:instanceOf      bdr:WA30149 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "dehradun, u.p."@en ;
        bdo:publisherName   "sakya centre"@en ;
        bdo:script          bdr:ScriptDbuCan ;
        skos:altLabel       "an exegesis of lam 'bras and its root text, the rdo rje'i tshig rkan"@en-x-mixed ;
        skos:prefLabel      "lam 'bras po ti dmar ma/"@bo-x-ewts .
    
    bdr:TTB4D803B9C0697516
        a                   bdo:CoverTitle ;
        rdfs:label          "lam 'bras pod dmar ma/"@bo-x-ewts .
    
    bdr:TTB7DCC3A33B4AC998
        a                   bdo:Subtitle ;
        rdfs:label          "an exegesis of lam 'bras and its root text, the rdo rje'i tshig rkan"@en-x-mixed .
    
    bdr:TTEB9D3A0178CE4E05
        a                   bdo:Title ;
        rdfs:label          "lam 'bras po ti dmar ma/"@bo-x-ewts .
}
