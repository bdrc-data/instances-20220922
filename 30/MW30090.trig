@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW30090 {
    bda:LG0MW30090_ZGSKR8FP6RVB
        a                   adm:UpdateData ;
        adm:logDate         "2024-01-30T15:13:03.171582Z"^^xsd:dateTime ;
        adm:logMessage      "saved"@en ;
        adm:logWho          bdu:U2089842660 .
    
    bda:LGA4E1F1713759F08B
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001Z"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577Z"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334Z"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356Z"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW30090  a          adm:AdminData ;
        adm:adminAbout      bdr:MW30090 ;
        adm:facetIndex      17 ;
        adm:gitPath         "30/MW30090.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW30090 ;
        adm:logEntry        bda:LG0MW30090_ZGSKR8FP6RVB , bda:LGA4E1F1713759F08B , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:replaceWith     bdr:W19816 ;
        adm:status          bda:StatusWithdrawn .
    
    bdr:EVED1666D84C0FE0FD
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1992"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID21BE9A8A0E779A94
        a                   bdr:HarvardShelfId ;
        rdf:value           "008638088-5" .
    
    bdr:ID95DA6A6435C792C2
        a                   bdr:HollisId ;
        rdf:value           "014260609" .
    
    bdr:IDAFB11815E94D4661
        a                   bf:Lccn ;
        rdf:value           "94926641" .
    
    bdr:IDCE39D255BA28E12B
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7674.2 .N35 1992" .
    
    bdr:IDF2712FB3B4CC84EB
        a                   bf:Isbn ;
        rdf:value           "7800570754" .
    
    bdr:MW30090  a          bdo:Instance ;
        skos:altLabel       "Jo-naṅ chos ʾbyuṅ zla baʾi sgron me /[Ṅag-dbaṅ-blo-gros-grags-pas brtsams]."@en-x-mixed , "juexiangpai jiao fa shi"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "jo nang chos 'byung zla ba'i sgron me/"@bo-x-ewts ;
        bf:identifiedBy     bdr:ID21BE9A8A0E779A94 , bdr:ID95DA6A6435C792C2 , bdr:IDAFB11815E94D4661 , bdr:IDCE39D255BA28E12B , bdr:IDF2712FB3B4CC84EB ;
        bdo:authorshipStatement  "*ngag dbang blo gros grags pas brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/ par thengs dang po/"@bo-x-ewts ;
        bdo:extentStatement  "3, 3, 591 p." ;
        bdo:hasTitle        bdr:TT53A7CEE6A275B9BC , bdr:TTA0A5DCCD469E98AD , bdr:TTFF40BC323E59D58E ;
        bdo:instanceEvent   bdr:EVED1666D84C0FE0FD ;
        bdo:instanceHasReproduction  bdr:W30090 ;
        bdo:instanceOf      bdr:WA19816 ;
        bdo:material        bdr:MaterialPaper ;
        bdo:note            bdr:NT8001411248BB81DE ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "krung go'i bod kyi shes rig dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptDbuCan .
    
    bdr:NT8001411248BB81DE
        a                   bdo:Note ;
        bdo:noteText        "only;"@en .
    
    bdr:TT53A7CEE6A275B9BC
        a                   bdo:TitlePageTitle ;
        rdfs:label          "jo nang chos 'byung zla ba'i sgron me/"@bo-x-ewts .
    
    bdr:TTA0A5DCCD469E98AD
        a                   bdo:FullTitle ;
        rdfs:label          "dpal ldan jo nang pa'i chos 'byung rgyal ba'i chos tshul gsal byed zla ba'i sgron me/"@bo-x-ewts .
    
    bdr:TTFF40BC323E59D58E
        a                   bdo:OtherTitle ;
        rdfs:label          "juexiangpai jiao fa shi"@zh-latn-pinyin-x-ndia .
}
