@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG13810 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG3F26BF04F5438705
        a                   adm:UpdateData ;
        adm:logDate         "2013-10-28T16:03:42.077000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG6DEF0445EAD4B962
        a                   adm:UpdateData ;
        adm:logDate         "2013-10-31T12:00:16.907000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added serial number"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG8587DB10FFE161FD
        a                   adm:InitialDataCreation ;
        adm:logDate         "2013-04-05T12:36:30.619000+00:00"^^xsd:dateTime ;
        adm:logMessage      "accessioned"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGCF737F9D64060CF6
        a                   adm:UpdateData ;
        adm:logDate         "2013-10-28T16:07:02.426000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added authorship statement"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG13810  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG13810 ;
        adm:facetIndex      15 ;
        adm:gitPath         "8a/MW1KG13810.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG13810 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG3F26BF04F5438705 , bda:LG6DEF0445EAD4B962 , bda:LG8587DB10FFE161FD , bda:LGCF737F9D64060CF6 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV156C406318A66D88
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2004,2007"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID9240F59517FAE8F4
        a                   bf:Lccn ;
        rdf:value           "2008308203" .
    
    bdr:IDA0F0323CAF166E49
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ286 .T48 2004" .
    
    bdr:IDAF2CCF2229EA3AD7
        a                   bdr:HollisId ;
        rdf:value           "014256067" .
    
    bdr:MW1KG13810  a       bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID9240F59517FAE8F4 , bdr:IDA0F0323CAF166E49 , bdr:IDAF2CCF2229EA3AD7 ;
        bdo:authorshipStatement  "rtsom pa po/ rnam grwa thub bstan yar 'phel/"@bo-x-ewts ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "1st ed."@en ;
        bdo:extentStatement  "a-e, 292 p." ;
        bdo:hasTitle        bdr:TT466C745F6FA529A2 , bdr:TTAC7A605CC6D5A8BF ;
        bdo:illustrations   "ill." ;
        bdo:instanceEvent   bdr:EV156C406318A66D88 ;
        bdo:instanceHasReproduction  bdr:W1KG13810 ;
        bdo:instanceOf      bdr:WA1KG13810 ;
        bdo:note            bdr:NT55D088898D296A07 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "dharamsala, h.p."@en ;
        bdo:publisherName   "rnam grwa shes yon lhan tshogs/"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS00KG03603 ;
        bdo:seriesNumber    "deb phreng 3" ;
        skos:prefLabel      "chos 'byung dris lan brgya pa legs bshad snying bsdus/"@bo-x-ewts .
    
    bdr:NT55D088898D296A07
        a                   bdo:Note ;
        bdo:noteText        "in tibetan"@en .
    
    bdr:TT466C745F6FA529A2
        a                   bdo:TitlePageTitle ;
        rdfs:label          "chos 'byung legs bshad snying bsdus blo gsar dga' bskyed/"@bo-x-ewts .
    
    bdr:TTAC7A605CC6D5A8BF
        a                   bdo:Title ;
        rdfs:label          "chos 'byung dris lan brgya pa legs bshad snying bsdus/"@bo-x-ewts .
}
