@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG10003 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG55171515B69150AE
        a                   adm:UpdateData ;
        adm:logDate         "2013-06-24T14:32:27.319000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGB12D3156F0D2F3CA
        a                   adm:InitialDataCreation ;
        adm:logDate         "2011-06-17T10:23:39.909000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG10003  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG10003 ;
        adm:facetIndex      19 ;
        adm:gitPath         "7d/MW1KG10003.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG10003 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG55171515B69150AE , bda:LGB12D3156F0D2F3CA , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV317F58A9586D5289
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1976"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID3D63AB3FD983ABFF
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ6115 .T96" .
    
    bdr:ID43B691BDF28E2C0A
        a                   bdr:HarvardShelfId ;
        rdf:value           "001872791-3" .
    
    bdr:ID533B145460E3C11E
        a                   bf:Lccn ;
        rdf:value           "77903404" .
    
    bdr:IDEEBC013018289C42
        a                   bdr:HollisId ;
        rdf:value           "014255559" .
    
    bdr:MW1KG10003  a       bdo:Instance ;
        bf:identifiedBy     bdr:ID3D63AB3FD983ABFF , bdr:ID43B691BDF28E2C0A , bdr:ID533B145460E3C11E , bdr:IDEEBC013018289C42 ;
        bdo:biblioNote      "reproduced form a manuscripts preserved at the library of the ven. rtogs-ldan rin-po-che of sgan-snon"@en ;
        bdo:extentStatement  "400 p." ;
        bdo:hasTitle        bdr:TT49945477AD2929C9 , bdr:TT5995120279C788E0 , bdr:TT825F5EB3073BDE01 , bdr:TT83423C14FCF79ED6 , bdr:TT86B7B6CEA17D45BC , bdr:TT9B229E6DF7084D35 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV317F58A9586D5289 ;
        bdo:instanceHasReproduction  bdr:W1KG10003 ;
        bdo:instanceOf      bdr:WA1KG10003 ;
        bdo:note            bdr:NT219343CC3C15C711 , bdr:NT53D0F63ACF9D8F9F ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "bir, h.p."@en ;
        bdo:publisherName   "d. tsondu senghe"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "two bka'-brgyud-pa works on buddhist disciplines"@en-x-mixed ;
        skos:prefLabel      "gzhi gsum gsal byed gur kum chun po dang / sdom gsum nyer mkho'i bum bzang bcas/"@bo-x-ewts .
    
    bdr:NT219343CC3C15C711
        a                   bdo:Note ;
        bdo:noteText        "the first text was written by an unknown abbot of 'bri-gung bka' brgyud"@en .
    
    bdr:NT53D0F63ACF9D8F9F
        a                   bdo:Note ;
        bdo:noteText        "in tibetan; preface in english"@en .
    
    bdr:TT49945477AD2929C9
        a                   bdo:OtherTitle ;
        rdfs:label          "two bka'-brgyud-pa works on buddhist disciplines"@en-x-mixed .
    
    bdr:TT5995120279C788E0
        a                   bdo:OtherTitle ;
        rdfs:label          "sdom gsum rnam par bstan pa nyer mkho'i bum bzang zhes bya ba/ rang 'dra'i rgyud la 'tshams par 'brel pa'i bstan bcos/"@bo-x-ewts .
    
    bdr:TT825F5EB3073BDE01
        a                   bdo:OtherTitle ;
        rdfs:label          "nyams su len bya 'jug pa tshul khrims kyi gzhi gsum gsal bar byed pa gur kum gyi chun po/"@bo-x-ewts .
    
    bdr:TT83423C14FCF79ED6
        a                   bdo:CoverTitle ;
        rdfs:label          "Two Bkaʾ-brgyud-pa works on Buddhist disciplines from the library of the Ven. Rtogs-ldan Rin-po-che of Sgan-snon."@en-x-mixed .
    
    bdr:TT86B7B6CEA17D45BC
        a                   bdo:Title ;
        rdfs:label          "gzhi gsum gsal byed gur kum chun po dang / sdom gsum nyer mkho'i bum bzang bcas/"@bo-x-ewts .
    
    bdr:TT9B229E6DF7084D35
        a                   bdo:OtherTitle ;
        rdfs:label          "rje rat+na karma bha dras mdzad pa'i me bsur gyi cho ga mdor bsdus/"@bo-x-ewts .
}
