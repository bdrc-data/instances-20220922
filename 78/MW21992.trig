@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21992 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG8A464D651C06E1B9
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-11T16:07:30.573000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LG9B9E3F02C61EB68D
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21992  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21992 ;
        adm:facetIndex      17 ;
        adm:gitPath         "78/MW21992.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21992 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG8A464D651C06E1B9 , bda:LG9B9E3F02C61EB68D , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV716A93BB40A827B7
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1972"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID26296A45511A0977
        a                   bf:Lccn ;
        rdf:value           "72908270" .
    
    bdr:ID60F387A721DE90A4
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7935.N344 G8 1972 ORIEN TIB" .
    
    bdr:IDA99D4E02FFB87ED8
        a                   bdr:HarvardShelfId ;
        rdf:value           "001835716-4" .
    
    bdr:IDB050857C719FA3DF
        a                   bdr:HollisId ;
        rdf:value           "014258540" .
    
    bdr:MW21992  a          bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID26296A45511A0977 , bdr:ID60F387A721DE90A4 , bdr:IDA99D4E02FFB87ED8 , bdr:IDB050857C719FA3DF ;
        bdo:biblioNote      "reproduced from a rare manuscript belonging to the stag tshang ras pa bla brang of hemis"@en ;
        bdo:extentStatement  "533 columns" ;
        bdo:hasTitle        bdr:TT0AE8050D36F10C43 , bdr:TT4C1EF729B92F03EB , bdr:TT670A7AFA4DF28E65 , bdr:TT8B49A47A0F08AEF6 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV716A93BB40A827B7 ;
        bdo:instanceHasReproduction  bdr:W21992 ;
        bdo:instanceOf      bdr:WA21992 ;
        bdo:note            bdr:NTE620F00C5D03BA7A ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "leh"@en ;
        bdo:publisherName   "s.w. tashigangpa"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        bdo:serialInstanceOf  bdr:WAS00EGS1016202 ;
        bdo:seriesNumber    "v. 42" ;
        skos:altLabel       "a record of the visionary experiences of the fifth dalai lama nag -dban-blo-bzan-rgya-mtsho"@en-x-mixed ;
        skos:prefLabel      "gsang ba'i rnam thar rgya can ma/"@bo-x-ewts .
    
    bdr:NTE620F00C5D03BA7A
        a                   bdo:Note ;
        bdo:noteText        "tbrc has a set of digital scanned images, tiffs and pdf files.\n0601."@en .
    
    bdr:TT0AE8050D36F10C43
        a                   bdo:CoverTitle ;
        rdfs:label          "Gsaṅ baʾi rnam thar rgya can ma :a record of the visionary experiences of the Fifth Dalai Lama Ṅag-dbaṅ-blo-bzaṅ-rgya-mtsho."@en-x-mixed .
    
    bdr:TT4C1EF729B92F03EB
        a                   bdo:Title ;
        rdfs:label          "gsang ba'i rnam thar rgya can ma/"@bo-x-ewts .
    
    bdr:TT670A7AFA4DF28E65
        a                   bdo:TitlePageTitle ;
        rdfs:label          "gsang ba'i rnam thar rgya can ma"@bo-alalc97 .
    
    bdr:TT8B49A47A0F08AEF6
        a                   bdo:Subtitle ;
        rdfs:label          "a record of the visionary experiences of the fifth dalai lama nag -dban-blo-bzan-rgya-mtsho"@en-x-mixed .
}
