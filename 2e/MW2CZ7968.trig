@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW2CZ7968 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG34787F779733E183
        a                   adm:UpdateData ;
        adm:logDate         "2009-10-07T11:33:08.748000+00:00"^^xsd:dateTime ;
        adm:logMessage      "corrected spelling and updated"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG8E529290A8077EA0
        a                   adm:InitialDataCreation ;
        adm:logDate         "2009-10-05T15:34:21.794000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:LG90DB88369FE9C490
        a                   adm:UpdateData ;
        adm:logDate         "2013-04-25T12:34:41.985000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type, source printery and biblio note"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW2CZ7968  a        adm:AdminData ;
        adm:adminAbout      bdr:MW2CZ7968 ;
        adm:facetIndex      16 ;
        adm:gitPath         "2e/MW2CZ7968.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW2CZ7968 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG34787F779733E183 , bda:LG8E529290A8077EA0 , bda:LG90DB88369FE9C490 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVC1FCF3B269A07EFF
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1977"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID7916392B25E398A7
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7982.4 .K55 1977" .
    
    bdr:ID7972DDF0377507E4
        a                   bdr:HollisId ;
        rdf:value           "014260281" .
    
    bdr:ID7CEC827C03E006C1
        a                   bf:Lccn ;
        rdf:value           "77905318" .
    
    bdr:ID82BFEB210EAE5C5E
        a                   bdr:HarvardShelfId ;
        rdf:value           "001570593-5" .
    
    bdr:MW2CZ7968  a        bdo:Instance ;
        bf:identifiedBy     bdr:ID7916392B25E398A7 , bdr:ID7972DDF0377507E4 , bdr:ID7CEC827C03E006C1 , bdr:ID82BFEB210EAE5C5E ;
        bdo:biblioNote      "The mos famous redaction was the large klu 'bum gtsang ma which was carved on blocks and preserved at the monastery of rtan brtan phun tshogs gling in gtsang during the time of the great jo nang kun mkhyen ta ra na tha. On this redaction the nest most famous edition, that of sde dge, is based."@en ;
        bdo:extentStatement  "5 v. ; 28 x 37 cm." ;
        bdo:hasSourcePrintery  bdr:G2CN10906 ;
        bdo:hasTitle        bdr:TT40F7C35AABCA9500 , bdr:TT9C5DF698AA7CCBE2 , bdr:TTA65A227C0142D884 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EVC1FCF3B269A07EFF ;
        bdo:instanceHasReproduction  bdr:W2CZ7968 ;
        bdo:instanceOf      bdr:WA2CZ7968 ;
        bdo:note            bdr:NT6C182E519AEA1130 ;
        bdo:numberOfVolumes  5 ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:publisherLocation  "dolanji, h.p."@en ;
        bdo:publisherName   "tibetan bonpo monastic centre"@en ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "Gtsaṅ ma Klu ʾbum chen mo :a reproduction of a manuscript copy based upon the Tāranātha tradition of the famed Bonpo recitational classic."@en-x-mixed ;
        skos:prefLabel      "gtsang ma klu 'bum chen mo/"@bo-x-ewts .
    
    bdr:NT6C182E519AEA1130
        a                   bdo:Note ;
        bdo:noteText        "contents:\n1) v.1. klu 'bum dkar po\n2) v.2-3. klu 'bum khra bo1) \n3.) v.4-5. klu 'bum nag po"@en .
    
    bdr:TT40F7C35AABCA9500
        a                   bdo:Title ;
        rdfs:label          "gtsang ma klu 'bum chen mo/"@bo-x-ewts .
    
    bdr:TT9C5DF698AA7CCBE2
        a                   bdo:OtherTitle ;
        rdfs:label          "klu 'bum dkar nag khra gsum/"@bo-x-ewts .
    
    bdr:TTA65A227C0142D884
        a                   bdo:CoverTitle ;
        rdfs:label          "Gtsaṅ ma Klu ʾbum chen mo :a reproduction of a manuscript copy based upon the Tāranātha tradition of the famed Bonpo recitational classic."@en-x-mixed .
}
