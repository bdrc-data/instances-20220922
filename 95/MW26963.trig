@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW26963 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG3003C531A5C6D77A
        a                   adm:UpdateData ;
        adm:logDate         "2010-02-17T16:26:03.094000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:LG77C5DC87CEBC05B2
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG97A7C3EAE1B61E22
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-06T15:45:24.478000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold and completed cataloging"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:MW26963  a          adm:AdminData ;
        adm:adminAbout      bdr:MW26963 ;
        adm:facetIndex      11 ;
        adm:gitPath         "95/MW26963.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW26963 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG3003C531A5C6D77A , bda:LG77C5DC87CEBC05B2 , bda:LG97A7C3EAE1B61E22 , bda:LGIMA45794A9D2 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV9CE5C20CE2428CE9
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2001"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:IDMW26963_71WC25COZPZF
        a                   bf:Isbn ;
        rdf:value           "9787223013222" .
    
    bdr:IDMW26963_SS90U5YAJ0Z4
        a                   bf:Isbn ;
        rdf:value           "7223013222" .
    
    bdr:MW26963  a          bdo:Instance ;
        bf:identifiedBy     bdr:IDMW26963_71WC25COZPZF , bdr:IDMW26963_SS90U5YAJ0Z4 ;
        bdo:authorshipStatement  "gzhis rtse sa khul gyi dmangs rtsom rnam gsum legs btus/ spyi khyab rtsom sgrig u yon lhan khang gis bsgrigs/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightUndetermined ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "4, 4, 184 p." ;
        bdo:hasTitle        bdr:TT3B96D0A6F8102118 , bdr:TT46A74888D1AE0E4B , bdr:TT8D123BB36737E6AC , bdr:TT8F734CF0AC86D3AA ;
        bdo:illustrations   "ill (col.)" ;
        bdo:instanceEvent   bdr:EV9CE5C20CE2428CE9 ;
        bdo:instanceHasReproduction  bdr:W26963 ;
        bdo:instanceOf      bdr:WA26963 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "lha sa/"@bo-x-ewts ;
        bdo:publisherName   "bod ljongs mi dmangs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS26963 ;
        skos:altLabel       "Gtsaṅ khul gyi yul glu legs btus /Gźis-rtse Sa-khul gyi dmaṅs rtsom rnam gsum legs btus spyi khyab rtsom sgrig u yon lhan khaṅ gis bsgrigs."@en-x-mixed , "日喀则民间故事集"@zh-hans ;
        skos:prefLabel      "gtsang khul gyi yul glu legs btus/"@bo-x-ewts .
    
    bdr:TT3B96D0A6F8102118
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "ri ka ze min jian gu shi ji"@zh-latn-pinyin-x-ndia .
    
    bdr:TT46A74888D1AE0E4B
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "日喀则民间故事集"@zh-hans .
    
    bdr:TT8D123BB36737E6AC
        a                   bdo:Title ;
        rdfs:label          "gtsang khul gyi yul glu legs btus/"@bo-x-ewts .
    
    bdr:TT8F734CF0AC86D3AA
        a                   bdo:CoverTitle ;
        rdfs:label          "Gtsaṅ khul gyi yul glu legs btus /Gźis-rtse Sa-khul gyi dmaṅs rtsom rnam gsum legs btus spyi khyab rtsom sgrig u yon lhan khaṅ gis bsgrigs."@en-x-mixed .
}
