@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW00EGS1016682 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG5C4B6434B9913A9C
        a                   adm:InitialDataCreation ;
        adm:logDate         "2006-11-30T00:41:31.718000+00:00"^^xsd:dateTime ;
        adm:logMessage      "published for creation of scan request"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGA2F349F4042C4A64
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-04T16:16:37.726000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added lccn 2004309846"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW00EGS1016682
        a                   adm:AdminData ;
        adm:adminAbout      bdr:MW00EGS1016682 ;
        adm:facetIndex      14 ;
        adm:gitPath         "6d/MW00EGS1016682.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW00EGS1016682 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG5C4B6434B9913A9C , bda:LGA2F349F4042C4A64 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV156F8988228C8E4E
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1989"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID295A8C17DEF8D125
        a                   bdr:HarvardShelfId ;
        rdf:value           "010889817-2" .
    
    bdr:ID57E0611604C956AA
        a                   bdr:HollisId ;
        rdf:value           "014253849" .
    
    bdr:IDCFEBD21623B3BF94
        a                   bf:Lccn ;
        rdf:value           "2004309846" .
    
    bdr:MW00EGS1016682
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:ID295A8C17DEF8D125 , bdr:ID57E0611604C956AA , bdr:IDCFEBD21623B3BF94 ;
        bdo:authorshipStatement  "mdo smad pa yon tan rgya mtsho zhes 'bod pas sbyar/"@bo-x-ewts ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:extentStatement  "7, 352 p." ;
        bdo:hasTitle        bdr:TT754D0D4DF374DEC7 , bdr:TT7AA0BA473CB6F31F , bdr:TTBF3D7B7A93BF9F2F ;
        bdo:illustrations   "ill., map" ;
        bdo:instanceEvent   bdr:EV156F8988228C8E4E ;
        bdo:instanceHasReproduction  bdr:W00EGS1016682 ;
        bdo:instanceOf      bdr:WA00EGS1016682 ;
        bdo:note            bdr:NT6FCDC614760E7F29 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "tibetan colony, distt. n. kanara"@en ;
        bdo:publisherName   "*mdo smad pa yon tan rgya mtsho/"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:workPrintery    "drepung losaling printing press/"@bo-x-ewts ;
        skos:altLabel       "Goṅ sa rgyal mchog bcu bźi pa chen poʼi skuʼi gcen po sku ʼbum khri zur Stag-mtsher mchog sprul Thub-bstan-ʼjigs-med-nor-buʼi thun moṅ mdzad rim bsdus don dpyod ldan yoṅs la gtam du bya ba sȯn med legs bśad ṅes don sprin gyi pho ña /Mdo-smad-pa Yon-tan-rgya-mtsho źes ʼbod pas sbyar."@en-x-mixed ;
        skos:prefLabel      "sku 'bum khri zur stag mtsher mchog sprul thub bstan 'jigs med nor bu'i thun mong mdzad rim bsdus don dpyod ldan yongs la gtam du bya ba sngon med legs bshad nges don sprin gyi pho nya/"@bo-x-ewts .
    
    bdr:NT6FCDC614760E7F29
        a                   bdo:Note ;
        bdo:noteText        "biography of the eldest brother of h.h. the 14th dalai lama"@en .
    
    bdr:TT754D0D4DF374DEC7
        a                   bdo:CoverTitle ;
        rdfs:label          "Goṅ sa rgyal mchog bcu bźi pa chen poʼi skuʼi gcen po sku ʼbum khri zur Stag-mtsher mchog sprul Thub-bstan-ʼjigs-med-nor-buʼi thun moṅ mdzad rim bsdus don dpyod ldan yoṅs la gtam du bya ba sȯn med legs bśad ṅes don sprin gyi pho ña /Mdo-smad-pa Yon-tan-rgya-mtsho źes ʼbod pas sbyar."@en-x-mixed .
    
    bdr:TT7AA0BA473CB6F31F
        a                   bdo:TitlePageTitle ;
        rdfs:label          "gong sa rgyal mchog bcu bzhi pa chen po'i sku'i gcen po sku 'bum khri zur stag mtsher mchog sprul thub bstan 'jigs med nor bu'i thun mong mdzad rim bsdus don dpyod ldan yongs la gtam du bya ba sngon med legs bshad nges don sprin gyi pho nya/"@bo-x-ewts .
    
    bdr:TTBF3D7B7A93BF9F2F
        a                   bdo:Title ;
        rdfs:label          "sku 'bum khri zur stag mtsher mchog sprul thub bstan 'jigs med nor bu'i thun mong mdzad rim bsdus don dpyod ldan yongs la gtam du bya ba sngon med legs bshad nges don sprin gyi pho nya/"@bo-x-ewts .
}
