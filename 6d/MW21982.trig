@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21982 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0E6D7B6C59E1966D
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG744220614FEEBEF3
        a                   adm:UpdateData ;
        adm:logDate         "2008-04-16T15:12:45.395000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21982  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21982 ;
        adm:facetIndex      15 ;
        adm:gitPath         "6d/MW21982.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21982 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG0E6D7B6C59E1966D , bda:LG744220614FEEBEF3 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV75B8FAC3A489B2D3
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1994"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID2E2F52BC3370DECD
        a                   bf:Isbn ;
        rdf:value           "7540908254" .
    
    bdr:ID4F766048B2A4737A
        a                   bf:Lccn ;
        rdf:value           "97915829" .
    
    bdr:ID5A5D834EC4B7C3BF
        a                   bdr:HollisId ;
        rdf:value           "014258535" .
    
    bdr:ID67F1E8B38622BFDB
        a                   bdr:HarvardShelfId ;
        rdf:value           "008079321-5" .
    
    bdr:ID8F732D8A8F55E4A5
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ5592.A45 B43 1994" .
    
    bdr:MW21982  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID2E2F52BC3370DECD , bdr:ID4F766048B2A4737A , bdr:ID5A5D834EC4B7C3BF , bdr:ID67F1E8B38622BFDB , bdr:ID8F732D8A8F55E4A5 ;
        bdo:authorshipStatement  "karma chags med sogs kyis mdzad/"@bo-x-ewts ;
        bdo:biblioNote      "v. 1. rtsa gzhung skor v. 2. 'grel ba'i skor"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:extentStatement  "2 v." ;
        bdo:hasTitle        bdr:TT2A821A5FC9A98928 , bdr:TT54F7CDFE0D4E9CA7 , bdr:TTD16A55A8C3EDFAAC ;
        bdo:illustrations   "ill." ;
        bdo:instanceEvent   bdr:EV75B8FAC3A489B2D3 ;
        bdo:instanceHasReproduction  bdr:W21982 ;
        bdo:instanceOf      bdr:WA1PD83963 ;
        bdo:numberOfVolumes  2 ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "Bde smon phyogs bsgrigs /[Karma-chags-med sogs kyis mdzad]."@en-x-mixed , "chu tz'u chi"@zh-latn-wadegile ;
        skos:prefLabel      "bde smon phyogs bsgrigs/"@bo-x-ewts .
    
    bdr:TT2A821A5FC9A98928
        a                   bdo:CoverTitle ;
        rdfs:label          "Bde smon phyogs bsgrigs /[Karma-chags-med sogs kyis mdzad]."@en-x-mixed .
    
    bdr:TT54F7CDFE0D4E9CA7
        a                   bdo:Title ;
        rdfs:label          "bde smon phyogs bsgrigs/"@bo-x-ewts .
    
    bdr:TTD16A55A8C3EDFAAC
        a                   bdo:ColophonTitle ;
        rdfs:label          "chu tz'u chi"@zh-latn-wadegile .
}
