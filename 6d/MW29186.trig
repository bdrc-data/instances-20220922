@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW29186 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG8795DDFB98538C6E
        a                   adm:UpdateData ;
        adm:logDate         "2007-01-11T06:52:19.718000+00:00"^^xsd:dateTime ;
        adm:logMessage      "published for scan request"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGCD5876DFCF63AD51
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW29186  a          adm:AdminData ;
        adm:adminAbout      bdr:MW29186 ;
        adm:facetIndex      17 ;
        adm:gitPath         "6d/MW29186.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW29186 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDS5ZNKT5O6YISF , bda:LG8795DDFB98538C6E , bda:LGCD5876DFCF63AD51 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVCA3EFD531C123B4F
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2002"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID32D4E2072FE01004
        a                   bdr:HarvardShelfId ;
        rdf:value           "010199587-3" .
    
    bdr:ID5F2AF255E16F50A0
        a                   bf:Lccn ;
        rdf:value           "2005323769" .
    
    bdr:IDA53E534F36072723
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7550 .Y42 2002" .
    
    bdr:IDBD651FFD9345FA7E
        a                   bdr:HollisId ;
        rdf:value           "014259921" .
    
    bdr:IDMW29186_9U6YXMNSLQXW
        a                   bf:Isbn ;
        rdf:value           "9789624501087" .
    
    bdr:IDMW29186_SMIDN0QW7GWK
        a                   bf:Isbn ;
        rdf:value           "9624501084" .
    
    bdr:MW29186  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID32D4E2072FE01004 , bdr:ID5F2AF255E16F50A0 , bdr:IDA53E534F36072723 , bdr:IDBD651FFD9345FA7E , bdr:IDMW29186_9U6YXMNSLQXW , bdr:IDMW29186_SMIDN0QW7GWK ;
        bdo:authorshipStatement  "mtsho lho khul gung ho rdzong bod skad yig gzhung las khang gis bsgrigs/"@bo-x-ewts ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "5 v." ;
        bdo:hasTitle        bdr:TT06E20788E41025F9 , bdr:TT1AE9FB6E35722748 , bdr:TT567A28B84F0CC320 , bdr:TTE61ABE73FF053AA7 ;
        bdo:illustrations   "port." ;
        bdo:instanceEvent   bdr:EVCA3EFD531C123B4F ;
        bdo:instanceHasReproduction  bdr:W29186 ;
        bdo:instanceOf      bdr:WA1KG89110 ;
        bdo:numberOfVolumes  5 ;
        bdo:publisherLocation  "zhang kang /"@bo-x-ewts ;
        bdo:publisherName   "zhang kAng than ma dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "Rje-btsun Ye-śes-rgya-mtshoʼi gsuṅ ʼbum /[Mtsho Lho Khul Guṅ-ho Rdzoṅ Bod Skad-yig Gźuṅ Las-khaṅ gis bsgrigs]."@en-x-mixed ;
        skos:prefLabel      "gsung 'bum/_ye shes rgya mtsho/"@bo-x-ewts .
    
    bdr:TT06E20788E41025F9
        a                   bdo:Title ;
        rdfs:label          "gsung 'bum/_ye shes rgya mtsho/"@bo-x-ewts .
    
    bdr:TT1AE9FB6E35722748
        a                   bdo:CoverTitle ;
        rdfs:label          "rje ye shes rgya mtsho'i gsung 'bum/"@bo-x-ewts .
    
    bdr:TT567A28B84F0CC320
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rje btsun ye shes rgya mtsho'i gsung 'bum/"@bo-x-ewts .
    
    bdr:TTE61ABE73FF053AA7
        a                   bdo:CoverTitle ;
        rdfs:label          "Rje-btsun Ye-śes-rgya-mtshoʼi gsuṅ ʼbum /[Mtsho Lho Khul Guṅ-ho Rdzoṅ Bod Skad-yig Gźuṅ Las-khaṅ gis bsgrigs]."@en-x-mixed .
}
