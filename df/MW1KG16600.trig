@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG16600 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGBC5301BF1131DC02
        a                   adm:InitialDataCreation ;
        adm:logDate         "2014-01-29T13:40:04.810000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG16600  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG16600 ;
        adm:facetIndex      11 ;
        adm:gitPath         "df/MW1KG16600.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG16600 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LGBC5301BF1131DC02 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV44E8310116CB314D
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1987,1997"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID63ADF87F6A07B2BF
        a                   bdr:HollisId ;
        rdf:value           "014256368" .
    
    bdr:IDMW1KG16600_334KM9QUB8IM
        a                   bf:Isbn ;
        rdf:value           "9787105027941" .
    
    bdr:IDMW1KG16600_BCVPEY8HWZS2
        a                   bf:Isbn ;
        rdf:value           "7105027940" .
    
    bdr:MW1KG16600  a       bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID63ADF87F6A07B2BF , bdr:IDMW1KG16600_334KM9QUB8IM , bdr:IDMW1KG16600_BCVPEY8HWZS2 ;
        bdo:authorshipStatement  "krung go bod brgyud mtho rim nang bstan slob gling gi slob gzhi rtsom sgrig tsho chung gis bsgrigs/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_TypeSet ;
        bdo:copyrightStatus  bdr:CopyrightUndetermined ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2, 2, 87, 4 p." ;
        bdo:hasTitle        bdr:TT58EFB52449526C81 , bdr:TT6ED601DE2F8E1E2C , bdr:TTF6E0D84AFE26BE20 ;
        bdo:instanceEvent   bdr:EV44E8310116CB314D ;
        bdo:instanceHasReproduction  bdr:W1KG16600 ;
        bdo:instanceOf      bdr:WA1KG16599 , bdr:WA1KG16600 ;
        bdo:note            bdr:NT8385B5176A21270E ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG11702 ;
        bdo:seriesNumber    "deb 2" ;
        skos:altLabel       "藏文文选（二）"@zh-hans ;
        skos:prefLabel      "gangs can rig brgya'i sgo 'byed lde mig (deb gnyis pa/)"@bo-x-ewts .
    
    bdr:NT8385B5176A21270E
        a                   bdo:Note ;
        bdo:noteText        "in tibetan"@en .
    
    bdr:TT58EFB52449526C81
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "藏文文选（二）"@zh-hans .
    
    bdr:TT6ED601DE2F8E1E2C
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "zang wen wen xuan (er)"@zh-latn-pinyin-x-ndia .
    
    bdr:TTF6E0D84AFE26BE20
        a                   bdo:Title ;
        rdfs:label          "gangs can rig brgya'i sgo 'byed lde mig (deb gnyis pa/)"@bo-x-ewts .
}
