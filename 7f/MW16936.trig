@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW16936 {
    bda:LG05E1F795854912CD
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW16936  a          adm:AdminData ;
        adm:adminAbout      bdr:MW16936 ;
        adm:facetIndex      15 ;
        adm:gitPath         "7f/MW16936.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW16936 ;
        adm:logEntry        bda:LG05E1F795854912CD , bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV6FCC7A10D1344216
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1998/1999"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID5AEF66B4B41161F4
        a                   bf:Lccn ;
        rdf:value           "99950284" .
    
    bdr:IDB10E5634DAE82BA5
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7550 .S28 1998" .
    
    bdr:IDFC461B4B0D9EC2A8
        a                   bdr:HollisId ;
        rdf:value           "014254770" .
    
    bdr:MW16936  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID5AEF66B4B41161F4 , bdr:IDB10E5634DAE82BA5 , bdr:IDFC461B4B0D9EC2A8 ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "6 v." ;
        bdo:hasTitle        bdr:TT07ADB24B607E548E , bdr:TT1D52544A1DFA34FD , bdr:TT706DEC33DBC24BD5 , bdr:TTF51695101DF053CA ;
        bdo:illustrations   "port." ;
        bdo:instanceEvent   bdr:EV6FCC7A10D1344216 ;
        bdo:instanceHasReproduction  bdr:IE16936 , bdr:W16936 ;
        bdo:instanceOf      bdr:WA16936 ;
        bdo:note            bdr:NT9D9AEA4F98F97631 ;
        bdo:numberOfVolumes  6 ;
        bdo:publisherLocation  "zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "hsia jih tung wen chi"@zh-latn-wadegile ;
        skos:prefLabel      "gsung 'bum/_blo bzang bshad sgrub rgya mtsho/"@bo-x-ewts .
    
    bdr:NT9D9AEA4F98F97631
        a                   bdo:Note ;
        bdo:noteText        "-4910\n\n\nisbn of other volumes:  7-5420-0685-1 (v. 2); 7-5420-0688-6 (v. 3); 7-5420-0708-4 (v. 4); 7-5420-0699-1 (v. 5); 7-5420-0840-4 (v. 6)"@en .
    
    bdr:TT07ADB24B607E548E
        a                   bdo:ColophonTitle ;
        rdfs:label          "hsia jih tung wen chi"@zh-latn-wadegile .
    
    bdr:TT1D52544A1DFA34FD
        a                   bdo:ColophonTitle ;
        rdfs:label          "xiaridong wen ji"@zh-latn-pinyin-x-ndia .
    
    bdr:TT706DEC33DBC24BD5
        a                   bdo:Title ;
        rdfs:label          "gsung 'bum/_blo bzang bshad sgrub rgya mtsho/"@bo-x-ewts .
    
    bdr:TTF51695101DF053CA
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rje shar gdong blo bzang bshad sgrub rgya mtsho'i gsung 'bum/"@bo-x-ewts .
}
