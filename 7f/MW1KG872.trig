@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG872 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0MW1KG872_BNYB96PCKT8G
        a                   adm:UpdateData ;
        adm:logDate         "2024-03-02T13:08:47.609244Z"^^xsd:dateTime ;
        adm:logMessage      "added serial"@en ;
        adm:logWho          bdu:U1426146149 .
    
    bda:LG0MW1KG872_IB8XA1SJBGLW
        a                   adm:UpdateData ;
        adm:logDate         "2024-01-31T16:37:13.210806Z"^^xsd:dateTime ;
        adm:logMessage      "corrected info"@bo ;
        adm:logWho          bdu:U0ES1788534075 .
    
    bda:LGE4B02A8CEC9217B9
        a                   adm:UpdateData ;
        adm:logDate         "2020-05-28T16:09:41.690000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added isbn"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGE98FED55438B596E
        a                   adm:InitialDataCreation ;
        adm:logDate         "2008-10-27T16:34:43.983000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged and released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG872  a         adm:AdminData ;
        adm:adminAbout      bdr:MW1KG872 ;
        adm:facetIndex      11 ;
        adm:gitPath         "7f/MW1KG872.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG872 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0MW1KG872_BNYB96PCKT8G , bda:LG0MW1KG872_IB8XA1SJBGLW , bda:LGE4B02A8CEC9217B9 , bda:LGE98FED55438B596E , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVFFBE7EDA8A72DB58
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2005"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID2839497B00DB5C51
        a                   bdr:HollisId ;
        rdf:value           "014257427" .
    
    bdr:IDMW1KG872_EV6F4FU964EE
        a                   bf:Isbn ;
        rdf:value           "7540932295" .
    
    bdr:IDMW1KG872_Y2ZBCKSNAY86
        a                   bf:Isbn ;
        rdf:value           "9787540932299" .
    
    bdr:MW1KG872  a         bdo:Instance ;
        skos:altLabel       "龙王与大比丘的问答"@zh-hans ;
        skos:prefLabel      "klu rgyal 'jog po dang dge slong chen po'i zhus lan/"@bo-x-ewts , "long wang yu da bi qiu de wen da"@zh-latn-pinyin-x-ndia ;
        bf:identifiedBy     bdr:ID2839497B00DB5C51 , bdr:IDMW1KG872_EV6F4FU964EE , bdr:IDMW1KG872_Y2ZBCKSNAY86 ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/ par thengs dang po/"@bo-x-ewts ;
        bdo:extentStatement  "6, 374 p." ;
        bdo:hasTitle        bdr:TT10CCF822E0F570A8 , bdr:TT4D5C3E704EC42ABC , bdr:TT6A45B9F5A50125D3 ;
        bdo:instanceEvent   bdr:EVFFBE7EDA8A72DB58 ;
        bdo:instanceHasReproduction  bdr:W1KG872 ;
        bdo:instanceOf      bdr:WA1KG872 ;
        bdo:material        bdr:MaterialPaper ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron dpe skrun tshogs pa/ si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptDbuCan ;
        bdo:serialInstanceOf  bdr:WAS1GS60779 ;
        bdo:seriesNumber    "13" .
    
    bdr:TT10CCF822E0F570A8
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "long wang yu da bi qiu de wen da"@zh-latn-pinyin-x-ndia .
    
    bdr:TT4D5C3E704EC42ABC
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "龙王与大比丘的问答"@zh-hans .
    
    bdr:TT6A45B9F5A50125D3
        a                   bdo:FullTitle ;
        rdfs:label          "klu rgyal 'jog po dang dge slong chen po'i zhus lan/"@bo-x-ewts .
}
