@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW30101 {
    bda:LG07BD1EC930DD908D
        a                   adm:UpdateData ;
        adm:logDate         "2016-04-09T01:36:05.139000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated catalog info"@en ;
        adm:logWho          bdu:U00006 .
    
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG396BEC5E720FB7DD
        a                   adm:UpdateData ;
        adm:logDate         "2013-03-19T12:50:13.944000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type and source printery"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LG7917A26E998E9777
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW30101  a          adm:AdminData ;
        adm:adminAbout      bdr:MW30101 ;
        adm:facetIndex      17 ;
        adm:gitPath         "34/MW30101.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW30101 ;
        adm:logEntry        bda:LG07BD1EC930DD908D , bda:LG0BDS7T15T6YORU18 , bda:LG396BEC5E720FB7DD , bda:LG7917A26E998E9777 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:CRB508ABC9E92A6A10
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P10134 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EVA3373BE5CBAB8AE8
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1969/1971"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID0771E7FA6018A6BD
        a                   bdr:HollisId ;
        rdf:value           "014260616" .
    
    bdr:ID5EF3D738E8C56D1F
        a                   bf:Lccn ;
        rdf:value           "74908873" .
    
    bdr:ID5F0A44EFA2C9A10F
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7684 .B75 1969" .
    
    bdr:ID9595CF2C4D9E8A66
        a                   bdr:HarvardShelfId ;
        rdf:value           "001350398-7" .
    
    bdr:MW30101  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID0771E7FA6018A6BD , bdr:ID5EF3D738E8C56D1F , bdr:ID5F0A44EFA2C9A10F , bdr:ID9595CF2C4D9E8A66 ;
        bdo:authorshipStatement  "reproduced photographically from the 'bri-gung yang-ri-sgar xylographic edition by khangsar tulku/"@bo-x-ewts ;
        bdo:creator         bdr:CRB508ABC9E92A6A10 ;
        bdo:extentStatement  "5 v." ;
        bdo:hasSourcePrintery  bdr:G2CN10876 ;
        bdo:hasTitle        bdr:TT406702E4F4191A18 , bdr:TT98FB6DB6382FEAF2 , bdr:TTE138BC5057F415F8 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EVA3373BE5CBAB8AE8 ;
        bdo:instanceHasReproduction  bdr:W30101 ;
        bdo:instanceOf      bdr:WA1KG10019 ;
        bdo:note            bdr:NT609DCFBC5B33145F ;
        bdo:numberOfVolumes  5 ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:publisherLocation  "new delhi/"@bo-x-ewts ;
        bdo:publisherName   "khangsar tulku/"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "the collected writings (gsung-'bum) of 'bri-gung chos-rje 'jig-rten-mgon-po"@en ;
        skos:prefLabel      "gsung 'bum/_'jig rten mgon po/"@bo-x-ewts .
    
    bdr:NT609DCFBC5B33145F
        a                   bdo:Note ;
        bdo:noteText        "-6109"@en .
    
    bdr:TT406702E4F4191A18
        a                   bdo:TitlePageTitle ;
        rdfs:label          "the collected writings (gsung-'bum) of 'bri-gung chos-rje 'jig-rten-mgon-po"@en .
    
    bdr:TT98FB6DB6382FEAF2
        a                   bdo:Title ;
        rdfs:label          "gsung 'bum/_'jig rten mgon po/"@bo-x-ewts .
    
    bdr:TTE138BC5057F415F8
        a                   bdo:CoverTitle ;
        rdfs:label          "The collected writings (Gsung-ʾbum) of ʾBri-gung Chos-rje ʾJig-rten-mgon-po Rin-chen-dpal.Reproduced photographically from the ʾBri-gung Yang-re-sgar xylographic ed. by Khangsar Talku [sic]."@en-x-mixed .
}
