@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW23747 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG5C8729B7CAD70067
        a                   adm:UpdateData ;
        adm:logDate         "2013-03-06T12:17:40.190000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type and source printery"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGC18A402F9AA99D3C
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW23747  a          adm:AdminData ;
        adm:adminAbout      bdr:MW23747 ;
        adm:facetIndex      13 ;
        adm:gitPath         "59/MW23747.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW23747 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG5C8729B7CAD70067 , bda:LGC18A402F9AA99D3C , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CR70941BDB338DBBE0
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P280 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EVF29EA34964F56A16
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1985"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID18CE82A8976CBB86
        a                   bf:Lccn ;
        rdf:value           "85902823" .
    
    bdr:ID25215FD5F9FAF8C0
        a                   bdr:HollisId ;
        rdf:value           "014259089" .
    
    bdr:IDA3EBC5269718F7AA
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7670 .L44 1985" .
    
    bdr:MW23747  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID18CE82A8976CBB86 , bdr:ID25215FD5F9FAF8C0 , bdr:IDA3EBC5269718F7AA ;
        bdo:authorshipStatement  "edited by don-grub-rgyal-mtshan, alias ye-ses-don-grub-bstan-pa'i-rgyal-mtshan"@en ;
        bdo:biblioNote      "reproduced from a print from the lhasa bzi-sde blocks"@en ;
        bdo:creator         bdr:CR70941BDB338DBBE0 ;
        bdo:extentStatement  "593 p." ;
        bdo:hasSourcePrintery  bdr:G3JT12562 ;
        bdo:hasTitle        bdr:TT06626E4C02C38FD0 , bdr:TT1F05FD5A19C8AB20 , bdr:TT9C80000CF0662CC8 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EVF29EA34964F56A16 ;
        bdo:instanceHasReproduction  bdr:W23747 ;
        bdo:instanceOf      bdr:WA1KG89134 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:publisherLocation  "bir, distt. kangra, h.p."@en ;
        bdo:publisherName   "d. tsondu senghe"@en ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "a collection of treasured teachings of the bka'-gdams-pa tradition"@en ;
        skos:prefLabel      "legs par bshad pa bka' gdams rin po che'i gsung gi gces btus nor bu'i bang mdzod/"@bo-x-ewts .
    
    bdr:TT06626E4C02C38FD0
        a                   bdo:TitlePageTitle ;
        rdfs:label          "legs par bsad pa bka' gdams rin po che'i gsun gi gces btus nor bu'i ban mdzod"@bo-alalc97 .
    
    bdr:TT1F05FD5A19C8AB20
        a                   bdo:Title ;
        rdfs:label          "legs par bshad pa bka' gdams rin po che'i gsung gi gces btus nor bu'i bang mdzod/"@bo-x-ewts .
    
    bdr:TT9C80000CF0662CC8
        a                   bdo:Subtitle ;
        rdfs:label          "a collection of treasured teachings of the bka'-gdams-pa tradition"@en .
}
