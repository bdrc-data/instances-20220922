@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW15973 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG2F039A6F38D391B7
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW15973  a          adm:AdminData ;
        adm:adminAbout      bdr:MW15973 ;
        adm:facetIndex      13 ;
        adm:gitPath         "15/MW15973.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW15973 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG2F039A6F38D391B7 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV648E3063F5F3DAC2
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1985,1999printing"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID34C654B52E379B14
        a                   bf:Lccn ;
        rdf:value           "99949078" .
    
    bdr:ID4D1D035FF96FCCFE
        a                   bf:ShelfMarkLcc ;
        rdf:value           "MLCSA 2000/01730 PL3633" .
    
    bdr:IDA6EEB8D396F1914E
        a                   bdr:HollisId ;
        rdf:value           "014254753" .
    
    bdr:IDMW15973_0OC78PHTJI0T
        a                   bf:Isbn ;
        rdf:value           "9787105033775" .
    
    bdr:IDMW15973_6KRNVP8HC226
        a                   bf:Isbn ;
        rdf:value           "7105033770" .
    
    bdr:MW15973  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID34C654B52E379B14 , bdr:ID4D1D035FF96FCCFE , bdr:IDA6EEB8D396F1914E , bdr:IDMW15973_0OC78PHTJI0T , bdr:IDMW15973_6KRNVP8HC226 ;
        bdo:authorshipStatement  "rin spungs ngag dbang 'jigs grags kyis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "10, 199 p." ;
        bdo:hasTitle        bdr:TT03B211E9B78BD99F , bdr:TTA9B4250689C6245C , bdr:TTB1BDD3473A40009A ;
        bdo:instanceEvent   bdr:EV648E3063F5F3DAC2 ;
        bdo:instanceHasReproduction  bdr:IE15973 , bdr:W15973 ;
        bdo:instanceOf      bdr:WA15973 ;
        bdo:note            bdr:NT9F78B6202D4257C5 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "zao shi ci lun--zhi zhe er shi"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "mngon brjod mkhas pa'i rna rgyan/"@bo-x-ewts .
    
    bdr:NT9F78B6202D4257C5
        a                   bdo:Note ;
        bdo:noteText        "tbrc also has:\nmngon brjod kyi bstan bcos mkhas pa'i rna rgyan zhes bya ba bzhugs so.\n82 ff.\nimpressions from the blocks carved about 1945 under the patronage of the regent stag brag paNDi ta and preserved at the zhol par khang chen mo.\nwritten at rin spungs pho brang  in a khyu mchog year.\nmargin title: mngon brjod rna rgyan.\ncopy: tbrc -- 2 copies"@en .
    
    bdr:TT03B211E9B78BD99F
        a                   bdo:Title ;
        rdfs:label          "mngon brjod mkhas pa'i rna rgyan/"@bo-x-ewts .
    
    bdr:TTA9B4250689C6245C
        a                   bdo:TitlePageTitle ;
        rdfs:label          "mngon brjod kyi bstan bcos mkhas pa'i rna rgyan/"@bo-x-ewts .
    
    bdr:TTB1BDD3473A40009A
        a                   bdo:ColophonTitle ;
        rdfs:label          "zao shi ci lun--zhi zhe er shi"@zh-latn-pinyin-x-ndia .
}
