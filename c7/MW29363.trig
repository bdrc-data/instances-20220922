@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW29363 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGF6154CB38A68A2BE
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW29363  a          adm:AdminData ;
        adm:adminAbout      bdr:MW29363 ;
        adm:facetIndex      14 ;
        adm:gitPath         "c7/MW29363.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW29363 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LGF6154CB38A68A2BE , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CR9068D246A15BD034
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P9714 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV142AF296A3FCA628
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2005"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID16580D31E8295FC2
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ3235 .B55 2005" .
    
    bdr:ID2B68F26B80571928
        a                   bdr:HollisId ;
        rdf:value           "014259994" .
    
    bdr:ID3D2BA63CD10A3CCC
        a                   bf:Lccn ;
        rdf:value           "2005322251" .
    
    bdr:IDMW29363_3FRZXERWWCZ6
        a                   bf:Isbn ;
        rdf:value           "7105071079" .
    
    bdr:IDMW29363_YZMJTI41YAH9
        a                   bf:Isbn ;
        rdf:value           "9787105071074" .
    
    bdr:MW29363  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID16580D31E8295FC2 , bdr:ID2B68F26B80571928 , bdr:ID3D2BA63CD10A3CCC , bdr:IDMW29363_3FRZXERWWCZ6 , bdr:IDMW29363_YZMJTI41YAH9 ;
        bdo:authorshipStatement  "dpal ldan rgya mtshos bsgrigs/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:creator         bdr:CR9068D246A15BD034 ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "6, 434 p." ;
        bdo:hasTitle        bdr:TT2EC4140510BB2759 , bdr:TTF64E46951B9F3FBD , bdr:TTFC542647D20FDCDD ;
        bdo:illustrations   "col. ill." ;
        bdo:instanceEvent   bdr:EV142AF296A3FCA628 ;
        bdo:instanceHasReproduction  bdr:W29363 ;
        bdo:instanceOf      bdr:WA29363 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "zang ban zhi da jie zun luo sang dan ba jia cuo bai sang bu wen ji"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "gsung 'bum/_blo bzang bstan pa rgya mtsho/"@bo-x-ewts .
    
    bdr:TT2EC4140510BB2759
        a                   bdo:TitlePageTitle ;
        rdfs:label          "gtsang paNDi ta'i gsung 'bum/"@bo-x-ewts .
    
    bdr:TTF64E46951B9F3FBD
        a                   bdo:ColophonTitle ;
        rdfs:label          "zang ban zhi da jie zun luo sang dan ba jia cuo bai sang bu wen ji"@zh-latn-pinyin-x-ndia .
    
    bdr:TTFC542647D20FDCDD
        a                   bdo:Title ;
        rdfs:label          "gsung 'bum/_blo bzang bstan pa rgya mtsho/"@bo-x-ewts .
}
