@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21909 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG897CB98319D0518D
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGBAA256A4E2E3B935
        a                   adm:UpdateData ;
        adm:logDate         "2013-09-05T14:14:59.996000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21909  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21909 ;
        adm:facetIndex      13 ;
        adm:gitPath         "0e/MW21909.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21909 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG897CB98319D0518D , bda:LGBAA256A4E2E3B935 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVC49D46418F935F9B
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1999"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID47D39BE23E8EC05A
        a                   bdr:HollisId ;
        rdf:value           "014258495" .
    
    bdr:ID814A7670C9AD4232
        a                   bf:Lccn ;
        rdf:value           "99938218" .
    
    bdr:IDE49BB15B5B2BE193
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PN6519.C5 H283 1999" .
    
    bdr:IDMW21909_GDS8APLN7RIM
        a                   bf:Isbn ;
        rdf:value           "9787105031900" .
    
    bdr:IDMW21909_JA6EHCYDTNKO
        a                   bf:Isbn ;
        rdf:value           "7105031905" .
    
    bdr:MW21909  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID47D39BE23E8EC05A , bdr:ID814A7670C9AD4232 , bdr:IDE49BB15B5B2BE193 , bdr:IDMW21909_GDS8APLN7RIM , bdr:IDMW21909_JA6EHCYDTNKO ;
        bdo:authorshipStatement  "'jang shes rab rnam dag dang tho skar 'od gsal gyis sgrig sgyur byas/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "7, 5, 460 p" ;
        bdo:hasTitle        bdr:TT03D1482F5B610986 , bdr:TT960C0C982D09BE0B , bdr:TTA41184D5AF346C9F ;
        bdo:instanceEvent   bdr:EVC49D46418F935F9B ;
        bdo:instanceHasReproduction  bdr:W21909 ;
        bdo:instanceOf      bdr:WA21909 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "汉藏谚语手册"@zh-hans ;
        skos:prefLabel      "rgya bod gtam dpe ngo mtshar phreng ba/"@bo-x-ewts .
    
    bdr:TT03D1482F5B610986
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "han zang yan yu shou ce"@zh-latn-pinyin-x-ndia .
    
    bdr:TT960C0C982D09BE0B
        a                   bdo:Title ;
        rdfs:label          "rgya bod gtam dpe ngo mtshar phreng ba/"@bo-x-ewts .
    
    bdr:TTA41184D5AF346C9F
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "汉藏谚语手册"@zh-hans .
}
