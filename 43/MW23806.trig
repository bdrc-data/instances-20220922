@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW23806 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGCDE4B2136E74CA8E
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGDEE8036875913375
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-25T11:08:47.675000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW23806  a          adm:AdminData ;
        adm:adminAbout      bdr:MW23806 ;
        adm:facetIndex      15 ;
        adm:gitPath         "43/MW23806.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW23806 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LGCDE4B2136E74CA8E , bda:LGDEE8036875913375 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EVAF09B9044CCBF8FB
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1984"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID481876E5AB5BAB0F
        a                   bdr:HollisId ;
        rdf:value           "014259143" .
    
    bdr:ID98A1517F8F75E8B8
        a                   bdr:PL480 ;
        rdf:value           "i-tib-2590" .
    
    bdr:IDD40861121901A457
        a                   bf:Lccn ;
        rdf:value           "85902188" .
    
    bdr:IDE1B4D5048821FE18
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3748.G4 J36 1984" .
    
    bdr:MW23806  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID481876E5AB5BAB0F , bdr:ID98A1517F8F75E8B8 , bdr:IDD40861121901A457 , bdr:IDE1B4D5048821FE18 ;
        bdo:authorshipStatement  "by the 3rd 'jam-sprul rin-po-che 'jam-dbyans-ses-rab-'od-zer"@en ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:extentStatement  "14, 281 p." ;
        bdo:hasTitle        bdr:TT148C8AD54582AE7B , bdr:TT56DEDC365552D8A9 , bdr:TT595B4713981A54D6 , bdr:TTBF51A0A1AC3A71BB ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EVAF09B9044CCBF8FB ;
        bdo:instanceHasReproduction  bdr:W23806 ;
        bdo:instanceOf      bdr:WA23806 ;
        bdo:note            bdr:NTBA3E2E4076E27D10 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "dharamsala"@en ;
        bdo:publisherName   "library of tibetan works and archives"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "'dzam glin skyes mchog glin rje ge sar nor bu dgra 'dul gyi rtogs brjod 'chi med lha yi rnga sgra : an episode from the gesar epic recounting gesar's birth, his conquest of rma-khog, and how he won the throne of glin through horse-racing"@en-x-mixed ;
        skos:prefLabel      "'dzam gling skyes mchog gling rje ge sar nor bu dgra 'dul gyi rtogs brjod 'chi med lha yi rnga sgra/"@bo-x-ewts .
    
    bdr:NTBA3E2E4076E27D10
        a                   bdo:Note ;
        bdo:noteText        "tbrc holds scanned images, tiffs and pdf files"@en .
    
    bdr:TT148C8AD54582AE7B
        a                   bdo:TitlePageTitle ;
        rdfs:label          "'dzam glin skyes mchog glin rje ge sar nor bu dgra 'dul gyi rtogs brjod 'chi med lha yi rnga sgra : an episode from the gesar epic recounting gesar's birth, his conquest of rma-khog, and how he won the throne of glin through horse-racing"@en-x-mixed .
    
    bdr:TT56DEDC365552D8A9
        a                   bdo:OtherTitle ;
        rdfs:label          "'dzam gling skyes mchog gling rje ge sar nor bu dgra 'dul gyi rtogs brjod 'chi med lha yi rnga sgra/"@bo-x-ewts .
    
    bdr:TT595B4713981A54D6
        a                   bdo:OtherTitle ;
        rdfs:label          "'chi med lha yi rnga sgra/"@bo-x-ewts .
    
    bdr:TTBF51A0A1AC3A71BB
        a                   bdo:Title ;
        rdfs:label          "gling rje ge sar rgyal po'i rtogs brjod 'chi med lha yi rnga sgra/"@bo-x-ewts .
}
