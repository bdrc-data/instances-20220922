@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1CZ2486 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG75FE6C2F11173FD1
        a                   adm:UpdateData ;
        adm:logDate         "2021-03-22T16:04:05.509000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added author, cat, pub info"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:LG999C59F4BE3AA2E9
        a                   adm:InitialDataCreation ;
        adm:logDate         "2007-11-15T13:44:21.876000+00:00"^^xsd:dateTime ;
        adm:logMessage      "new"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1CZ2486  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1CZ2486 ;
        adm:facetIndex      12 ;
        adm:gitPath         "5c/MW1CZ2486.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1CZ2486 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG75FE6C2F11173FD1 , bda:LG999C59F4BE3AA2E9 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVA932BEDE29E30249
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2004"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID7296253EC2164CA4
        a                   bdr:HollisId ;
        rdf:value           "014255356" .
    
    bdr:IDMW1CZ2486_CS9C1Q9VAWLM
        a                   bf:Isbn ;
        rdf:value           "2457782" .
    
    bdr:MW1CZ2486  a        bdo:Instance ;
        bf:identifiedBy     bdr:ID7296253EC2164CA4 , bdr:IDMW1CZ2486_CS9C1Q9VAWLM ;
        bdo:authorshipStatement  "dpal yul snga 'gyur mtho slob mdo sngags thos bsam dar rgyas gling gi slob dpon tshe dbang bkra shis kyis phyogs bsgrigs byas/"@bo-x-ewts ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightUndetermined ;
        bdo:editionStatement  "par thengs 1"@bo-x-ewts ;
        bdo:extentStatement  "108 p." ;
        bdo:hasTitle        bdr:TT1558AE36A0DF0CA1 , bdr:TT7C907969CB3EEF14 ;
        bdo:illustrations   "Illustrated" ;
        bdo:instanceEvent   bdr:EVA932BEDE29E30249 ;
        bdo:instanceHasReproduction  bdr:IE1CZ2486 , bdr:W1CZ2486 ;
        bdo:instanceOf      bdr:WA1CZ2486 ;
        bdo:note            bdr:NT8B74727657D19BD5 , bdr:NTD51D0CBDA4FFFCEF ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "[s.l.]"@en ;
        bdo:publisherName   "[s.n.]"@en ;
        bdo:script          bdr:ScriptTibt ;
        skos:prefLabel      "gnas chen rdzong shod bde gshegs 'dus pa'i pho brang chen mo'i rten dang brten pa'i byung ba rags bsdus skal bzang dad pa'i gsos sman/"@bo-x-ewts .
    
    bdr:NT8B74727657D19BD5
        a                   bdo:Note ;
        bdo:noteText        "unknown author"@en .
    
    bdr:NTD51D0CBDA4FFFCEF
        a                   bdo:Note ;
        bdo:noteText        "Text mentions that it was written by slob dpon tshe dbang bkra shis, Identity of this author is uncertain"@en .
    
    bdr:TT1558AE36A0DF0CA1
        a                   bdo:Title ;
        rdfs:label          "gnas chen rdzong shod bde gshegs 'dus pa'i pho brang chen mo'i rten dang brten pa'i byung ba rags bsdus skal bzang dad pa'i gsos sman/"@bo-x-ewts .
    
    bdr:TT7C907969CB3EEF14
        a                   bdo:TitlePageTitle ;
        rdfs:label          "shar phyogs 'gro 'dul sprul pa'i zhing mchog 'og min zab bu lung gnyis pa sbas gnas chen po rdzong shod bde gshegs 'dus pa'i pho brang chen mo'i rten dang brten pa'i byung ba rags bsdus skal bzang dad pa'i gsos sman/"@bo-x-ewts .
}
