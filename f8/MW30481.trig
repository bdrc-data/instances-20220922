@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW30481 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG660B71FBBBA37242
        a                   adm:UpdateData ;
        adm:logDate         "2014-03-10T15:22:20.450000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold and completed cataloging"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGED24720001A74E9D
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW30481  a          adm:AdminData ;
        adm:adminAbout      bdr:MW30481 ;
        adm:facetIndex      13 ;
        adm:gitPath         "f8/MW30481.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW30481 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG660B71FBBBA37242 , bda:LGED24720001A74E9D , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV44247D5B1D26446F
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2004"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:IDE5F92DB8D47459B2
        a                   bdr:HollisId ;
        rdf:value           "014260816" .
    
    bdr:MW30481  a          bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:IDE5F92DB8D47459B2 ;
        bdo:authorshipStatement  "krung go spyi tshogs tshan rig khang grangs nyung mi rigs rtsom rig so'o dang bod rang skyong ljongs spyi tshogs tshan rig khang gis bsgrigs/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2 v." ;
        bdo:hasTitle        bdr:TT1283894DCA78CDE6 , bdr:TT96A5C18B2202204C , bdr:TTBEB2C55BCDA2432A , bdr:TTF621ADC89DB132C5 ;
        bdo:instanceEvent   bdr:EV44247D5B1D26446F ;
        bdo:instanceHasReproduction  bdr:W30481 ;
        bdo:instanceOf      bdr:WA30481 ;
        bdo:note            bdr:NT2F4DBBFB1B9380D4 ;
        bdo:numberOfVolumes  2 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "lha sa/"@bo-x-ewts ;
        bdo:publisherName   "bod ljongs bod yig dpe rnying dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG16786 ;
        bdo:seriesNumber    "26 (stod cha, smad cha)" ;
        skos:altLabel       "其日珊瑚宗"@zh-hans ;
        skos:prefLabel      "bye ri byur rdzong /"@bo-x-ewts .
    
    bdr:NT2F4DBBFB1B9380D4
        a                   bdo:Note ;
        bdo:noteText        "in tibetan"@en .
    
    bdr:TT1283894DCA78CDE6
        a                   bdo:TitlePageTitle ;
        rdfs:label          "sgrung mkhan bsam grub kyis phab pa'i gling rje ge sar rgyal po'i sgrung / bye ri byur rdzong /"@bo-x-ewts .
    
    bdr:TT96A5C18B2202204C
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "其日珊瑚宗"@zh-hans .
    
    bdr:TTBEB2C55BCDA2432A
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "qi ri shan hu zong"@zh-latn-pinyin-x-ndia .
    
    bdr:TTF621ADC89DB132C5
        a                   bdo:Title ;
        rdfs:label          "bye ri byur rdzong /"@bo-x-ewts .
}
