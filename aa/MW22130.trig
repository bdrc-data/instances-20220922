@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW22130 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG1D90B793923402B6
        a                   adm:UpdateData ;
        adm:logDate         "2013-02-28T09:21:28.672000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type and source printery"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LG715BFB091B468A53
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW22130  a          adm:AdminData ;
        adm:adminAbout      bdr:MW22130 ;
        adm:facetIndex      16 ;
        adm:gitPath         "aa/MW22130.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW22130 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG1D90B793923402B6 , bda:LG715BFB091B468A53 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV0F71187459B8D4C4
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1983/1985"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID0B8B1C40C94E338D
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7662 .B94 1983" .
    
    bdr:ID27ECDC11F81AC5C7
        a                   bdr:HollisId ;
        rdf:value           "014258646" .
    
    bdr:ID2EABF637CF1A6816
        a                   bdr:HarvardShelfId ;
        rdf:value           "007593411-6" .
    
    bdr:ID6B61FCC5BF18CEF8
        a                   bf:Lccn ;
        rdf:value           "83907132" .
    
    bdr:MW22130  a          bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID0B8B1C40C94E338D , bdr:ID27ECDC11F81AC5C7 , bdr:ID2EABF637CF1A6816 , bdr:ID6B61FCC5BF18CEF8 ;
        bdo:biblioNote      "reproduced from prints from the 'ol-dga' blocks from the library of he-mi rgod-tshan sgrub-sde by o-rgyan-rnam-rgyal"@en ;
        bdo:extentStatement  "13 v." ;
        bdo:hasSourcePrintery  bdr:G2CN10881 ;
        bdo:hasTitle        bdr:TT6BAEC6DDE853B4ED , bdr:TT98ACA64632526F6D , bdr:TTB48253F7F4F4F63E , bdr:TTF52314A2A43C3F63 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV0F71187459B8D4C4 ;
        bdo:instanceHasReproduction  bdr:W22130 ;
        bdo:instanceOf      bdr:WA1CZ2744 , bdr:WA22130 ;
        bdo:note            bdr:NTD0404A5507AEE7B5 ;
        bdo:numberOfVolumes  13 ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:publisherLocation  "leh"@en ;
        bdo:publisherName   "t. sonam & d.l. tashigang"@en ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS00EGS1016202 ;
        bdo:seriesNumber    "115-127" ;
        skos:altLabel       "the collected works (gsun 'bum) of sle-lun rje-drun bzad-pa'i-rdo-rje"@en-x-mixed ;
        skos:prefLabel      "gsung 'bum/_bzhad pa'i rdo rje/"@bo-x-ewts .
    
    bdr:NTD0404A5507AEE7B5
        a                   bdo:Note ;
        bdo:noteText        "-5900"@en .
    
    bdr:TT6BAEC6DDE853B4ED
        a                   bdo:Title ;
        rdfs:label          "gsung 'bum/_bzhad pa'i rdo rje/"@bo-x-ewts .
    
    bdr:TT98ACA64632526F6D
        a                   bdo:TitlePageTitle ;
        rdfs:label          "the collected works (gsun 'bum) of sle-lun rje-drun bzad-pa'i-rdo-rje"@en-x-mixed .
    
    bdr:TTB48253F7F4F4F63E
        a                   bdo:Title ;
        rdfs:label          "sle lung rje drung bzhad pa'i rdo rje'i gsung 'bum/"@bo-x-ewts .
    
    bdr:TTF52314A2A43C3F63
        a                   bdo:CoverTitle ;
        rdfs:label          "The collected works (gsuṅ ʼbum) of Sle-luṅ Rje-druṅ Bźad-pa'i-rdo-rje."@en-x-mixed .
}
