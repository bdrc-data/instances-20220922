@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW22013 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG176C97E2C95B6D54
        a                   adm:UpdateData ;
        adm:logDate         "2013-01-22T13:55:37.969000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold and added copyrighttitle"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGDF424119FB5283F4
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGF879B18702672E4D
        a                   adm:UpdateData ;
        adm:logDate         "2013-01-17T11:52:08.860000+00:00"^^xsd:dateTime ;
        adm:logMessage      "reviewed"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW22013  a          adm:AdminData ;
        adm:adminAbout      bdr:MW22013 ;
        adm:facetIndex      15 ;
        adm:gitPath         "02/MW22013.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW22013 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG176C97E2C95B6D54 , bda:LGDF424119FB5283F4 , bda:LGF879B18702672E4D , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVA943D95AFE2D1838
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1987/1988"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID0443D066D4AE5F2C
        a                   bdr:HollisId ;
        rdf:value           "014258555" .
    
    bdr:ID1E3DA1B141F1B819
        a                   bf:Isbn ;
        rdf:value           "7542000470" .
    
    bdr:ID86CD851C5F30F433
        a                   bf:Lccn ;
        rdf:value           "95903234" .
    
    bdr:IDA14DB5885F87384B
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ5650 .Z33 1987" .
    
    bdr:MW22013  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID0443D066D4AE5F2C , bdr:ID1E3DA1B141F1B819 , bdr:ID86CD851C5F30F433 , bdr:IDA14DB5885F87384B ;
        bdo:biblioNote      "vol. 2 has isbn: 7-5420-0052-7"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2 v." ;
        bdo:hasTitle        bdr:TT0A8008A7DE799C2E , bdr:TT1C6609FC870F8D0D , bdr:TTC26D91C2EECAD410 , bdr:TTCF09B1DD4A59CE8F ;
        bdo:instanceEvent   bdr:EVA943D95AFE2D1838 ;
        bdo:instanceHasReproduction  bdr:W22013 ;
        bdo:instanceOf      bdr:WA22013 ;
        bdo:numberOfVolumes  2 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "夏嘎巴道歌"@zh-hans ;
        skos:prefLabel      "zhabs dkar ba'i mgur 'bum/"@bo-x-ewts .
    
    bdr:TT0A8008A7DE799C2E
        a                   bdo:Title ;
        rdfs:label          "zhabs dkar ba'i mgur 'bum/"@bo-x-ewts .
    
    bdr:TT1C6609FC870F8D0D
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bya btang tshogs drug rang grol gyis rang dang skal ldan gdul bya la mgrin pa gdams pa'i bang mdzod nas glu dbyangs dga' ston 'gyed pa rnams bzhugs/"@bo-x-ewts .
    
    bdr:TTC26D91C2EECAD410
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "xia ga ba dao ge"@zh-latn-pinyin-x-ndia .
    
    bdr:TTCF09B1DD4A59CE8F
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "夏嘎巴道歌"@zh-hans .
}
