@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW00KG09730 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0MW00KG09730_XEAIU8VBVPBZ
        a                   adm:UpdateData ;
        adm:logDate         "2024-01-30T15:26:05.496064Z"^^xsd:dateTime ;
        adm:logMessage      "saved"@en ;
        adm:logWho          bdu:U2089842660 .
    
    bda:LGDCF8AE287D1EAC1C
        a                   adm:InitialDataCreation ;
        adm:logDate         "2008-06-17T10:42:16.312000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged and released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW00KG09730  a      adm:AdminData ;
        adm:adminAbout      bdr:MW00KG09730 ;
        adm:facetIndex      12 ;
        adm:gitPath         "c5/MW00KG09730.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW00KG09730 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0MW00KG09730_XEAIU8VBVPBZ , bda:LGDCF8AE287D1EAC1C , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV176E161B0AAE8934
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2002"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID46BF8CAE065A9281
        a                   bdr:HarvardShelfId ;
        rdf:value           "008174759-4" .
    
    bdr:ID5CBBB3906319AE92
        a                   bdr:HollisId ;
        rdf:value           "014254550" .
    
    bdr:IDMW00KG09730_7WPZMYVXC7U5
        a                   bf:Isbn ;
        rdf:value           "7105025034" .
    
    bdr:IDMW00KG09730_Z1LT5C21M76F
        a                   bf:Isbn ;
        rdf:value           "9787105025039" .
    
    bdr:MW00KG09730  a      bdo:Instance ;
        skos:altLabel       "Rgyal rabs gsal baʼi me loṅ /Sa-skya Bsod-nams-rgyal-mtshan gyis brtsams."@en-x-mixed , "xi zang wang tong ji"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "rgyal rabs gsal ba'i me long /"@bo-x-ewts ;
        bf:identifiedBy     bdr:ID46BF8CAE065A9281 , bdr:ID5CBBB3906319AE92 , bdr:IDMW00KG09730_7WPZMYVXC7U5 , bdr:IDMW00KG09730_Z1LT5C21M76F ;
        bdo:authorshipStatement  "sa skya bsod nams rgyal mtshan gyis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Sewn ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/ par thengs lnga pa/"@bo-x-ewts ;
        bdo:extentStatement  "2, 3, 250 p." ;
        bdo:hasTitle        bdr:TT4385FF3B9C0C14F0 , bdr:TT64785B666A22C117 ;
        bdo:instanceEvent   bdr:EV176E161B0AAE8934 ;
        bdo:instanceHasReproduction  bdr:IE00KG09730 , bdr:W00KG09730 , bdr:W1KG24814 ;
        bdo:instanceOf      bdr:WA00CHZ0103341 ;
        bdo:material        bdr:MaterialPaper ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptDbuCan .
    
    bdr:TT4385FF3B9C0C14F0
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rgyal rabs gsal ba'i me long /"@bo-x-ewts .
    
    bdr:TT64785B666A22C117
        a                   bdo:OtherTitle ;
        rdfs:label          "xi zang wang tong ji"@zh-latn-pinyin-x-ndia .
}
