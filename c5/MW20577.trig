@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW20577 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG5CD0179BC4F8C265
        a                   adm:UpdateData ;
        adm:logDate         "2011-07-06T17:08:47.837000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added shad and chinese title"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG8859B6ADDAC906F6
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW20577  a          adm:AdminData ;
        adm:adminAbout      bdr:MW20577 ;
        adm:facetIndex      11 ;
        adm:gitPath         "c5/MW20577.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW20577 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG5CD0179BC4F8C265 , bda:LG8859B6ADDAC906F6 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV8EDA9347C5D327BD
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2000"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID970ED9FE30A18C7A
        a                   bdr:HollisId ;
        rdf:value           "014258216" .
    
    bdr:IDMW20577_D9YU6BXMV6MY
        a                   bf:Isbn ;
        rdf:value           "7540923784" .
    
    bdr:IDMW20577_SVQJBG0N0DP1
        a                   bf:Isbn ;
        rdf:value           "9787540923785" .
    
    bdr:MW20577  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID970ED9FE30A18C7A , bdr:IDMW20577_D9YU6BXMV6MY , bdr:IDMW20577_SVQJBG0N0DP1 ;
        bdo:authorshipStatement  "'bum skyabs kyis brtsams/"@bo-x-ewts ;
        bdo:biblioNote      "printed from computerized input"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "4, 4, 246 p." ;
        bdo:hasTitle        bdr:TTAFBF6266CBC797CA , bdr:TTB018EEB77253BBBD , bdr:TTE92D46A2A21658F1 ;
        bdo:instanceEvent   bdr:EV8EDA9347C5D327BD ;
        bdo:instanceHasReproduction  bdr:W20577 ;
        bdo:instanceOf      bdr:WA20577 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "zang chuan bian li luo ji xue"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "bod kyi gtan tshigs rig pa'i spyi don go bder brjod pa/"@bo-x-ewts .
    
    bdr:TTAFBF6266CBC797CA
        a                   bdo:ColophonTitle ;
        rdfs:label          "zang chuan bian li luo ji xue"@zh-latn-pinyin-x-ndia .
    
    bdr:TTB018EEB77253BBBD
        a                   bdo:Title ;
        rdfs:label          "bod kyi gtan tshigs rig pa'i spyi don go bder brjod pa/"@bo-x-ewts .
    
    bdr:TTE92D46A2A21658F1
        a                   bdo:ColophonTitle ;
        rdfs:label          "藏传辩理逻辑学"@zh-hans .
}
