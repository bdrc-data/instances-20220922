@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG5375 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG9C2AB9937EFD2F3B
        a                   adm:InitialDataCreation ;
        adm:logDate         "2010-06-11T14:14:31.072000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG5375  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1KG5375 ;
        adm:facetIndex      11 ;
        adm:gitPath         "cc/MW1KG5375.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG5375 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG9C2AB9937EFD2F3B , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV5118386F572B215E
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2003"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID0BD5EA5D09963BBF
        a                   bf:ShelfMarkLcc ;
        rdf:value           "RS131.75.T55 Z54 2003" .
    
    bdr:ID1013827B96EF2855
        a                   bf:Lccn ;
        rdf:value           "2003320018" .
    
    bdr:ID3E81BE282BE1C147
        a                   bdr:HollisId ;
        rdf:value           "014257126" .
    
    bdr:IDMW1KG5375_3JWP6L99P369
        a                   bf:Ean ;
        rdf:value           "0614141000012" .
    
    bdr:IDMW1KG5375_KIAEPPERBK9Z
        a                   bf:Isbn ;
        rdf:value           "8186230416" .
    
    bdr:MW1KG5375  a        bdo:Instance ;
        bf:identifiedBy     bdr:ID0BD5EA5D09963BBF , bdr:ID1013827B96EF2855 , bdr:ID3E81BE282BE1C147 , bdr:IDMW1KG5375_3JWP6L99P369 , bdr:IDMW1KG5375_KIAEPPERBK9Z ;
        bdo:authorshipStatement  "rtsom sgrig pa sman pa zla ba/"@bo-x-ewts ;
        bdo:biblioNote      "printed from computerized input"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:extentStatement  "xxxix, 512 p." ;
        bdo:hasTitle        bdr:TT9CB3D6B52451067B ;
        bdo:instanceEvent   bdr:EV5118386F572B215E ;
        bdo:instanceHasReproduction  bdr:W1KG5375 ;
        bdo:instanceOf      bdr:WA1KG5375 ;
        bdo:note            bdr:NT7902FFC5A306333D ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "[dharamsala, h.p.]"@en ;
        bdo:publisherName   "ri brag dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:prefLabel      "bod kyi gso ba rig pa las sman rdzas sbyor bzo'i lag len gsang sgo 'byed pa'i lde mig"@bo-x-ewts .
    
    bdr:NT7902FFC5A306333D
        a                   bdo:Note ;
        bdo:noteText        "copy made available from the library of tibetan works and archives; ltwa acc. no. 7893"@en .
    
    bdr:TT9CB3D6B52451067B
        a                   bdo:Title ;
        rdfs:label          "bod kyi gso ba rig pa las sman rdzas sbyor bzo'i lag len gsang sgo 'byed pa'i lde mig"@bo-x-ewts .
}
