@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW23936 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG78277E711DDB975E
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGA46F5AE77856EA9A
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-26T10:22:45.313000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW23936  a          adm:AdminData ;
        adm:adminAbout      bdr:MW23936 ;
        adm:facetIndex      16 ;
        adm:gitPath         "cc/MW23936.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW23936 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG78277E711DDB975E , bda:LGA46F5AE77856EA9A , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV0F9A24DCC3A9A96F
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1985"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID3437898E99AA85E5
        a                   bdr:HollisId ;
        rdf:value           "014259245" .
    
    bdr:IDA833606254D124AC
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ968.U58 T74 1985" .
    
    bdr:IDB82061C799C00FE9
        a                   bdr:PL480 ;
        rdf:value           "bhu-tib-444" .
    
    bdr:IDD12BC9BB08077809
        a                   bf:Lccn ;
        rdf:value           "85902623" .
    
    bdr:MW23936  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID3437898E99AA85E5 , bdr:IDA833606254D124AC , bdr:IDB82061C799C00FE9 , bdr:IDD12BC9BB08077809 ;
        bdo:authorshipStatement  "by tshar-chen blo-gsal-rgya-mtsho (1502-1566)"@en ;
        bdo:biblioNote      "reproduced from a rare manuscript that had once belonged to the eighth pad-glin gsun-sprul of lha-lun (1843-1891) from the gon-'phel dpe-mdzod"@en ;
        bdo:extentStatement  "154 p." ;
        bdo:hasTitle        bdr:TT243491646F7C1371 , bdr:TT459263700C2D878F , bdr:TT5F4145B8F8C188EA , bdr:TT6D0D332FF2F9BBA4 , bdr:TT95A7DC9ED70D35BF ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV0F9A24DCC3A9A96F ;
        bdo:instanceHasReproduction  bdr:W23936 ;
        bdo:instanceOf      bdr:WA23936 ;
        bdo:note            bdr:NT6F389EBCC3E7C8BB ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "thimphu, bhutan"@en ;
        bdo:publisherName   "national library of bhutan"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "the biography of the sa-skya-pa lam-'bras master, rdo-rin-pa kun-spans chen-po kun-bzan-chos-kyi-ni-ma (1449-1524)"@en-x-mixed ;
        skos:prefLabel      "rje btsun rdo rje 'chang chen po kun spangs chos rje'i rnam thar ngo mtshar dad pa'i spu long g.yo ba/"@bo-x-ewts .
    
    bdr:NT6F389EBCC3E7C8BB
        a                   bdo:Note ;
        bdo:noteText        "original manuscript in dbu med"@en .
    
    bdr:TT243491646F7C1371
        a                   bdo:Title ;
        rdfs:label          "rdo ring pa kun bzang chos kyi nyi ma'i rnam thar/"@bo-x-ewts .
    
    bdr:TT459263700C2D878F
        a                   bdo:Subtitle ;
        rdfs:label          "the biography of the sa-skya-pa lam-'bras master, rdo-rin-pa kun-spans chen-po kun-bzan-chos-kyi-ni-ma (1449-1524)"@en-x-mixed .
    
    bdr:TT5F4145B8F8C188EA
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rje btsun rdo rje 'chan chen po kun spans chos rje'i rnam thar no mtshar dad pa'i spu lon g.yo ba"@bo-alalc97 .
    
    bdr:TT6D0D332FF2F9BBA4
        a                   bdo:OtherTitle ;
        rdfs:label          "rje btsun rdo rje 'chang chen po kun spangs chos rje'i rnam thar ngo mtshar dad pa'i spu long g.yo ba/"@bo-x-ewts .
    
    bdr:TT95A7DC9ED70D35BF
        a                   bdo:CoverTitle ;
        rdfs:label          "rje btsun kun spangs chos rje'i rnam thar/"@bo-x-ewts .
}
