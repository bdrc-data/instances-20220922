@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG4186 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG271EDFDAD3E68353
        a                   adm:UpdateData ;
        adm:logDate         "2010-07-22T14:44:24.345000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00020 .
    
    bda:LG6696A0ADB5B52C5C
        a                   adm:UpdateData ;
        adm:logDate         "2012-02-24T10:26:28.189000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added shad"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG9CCD25D0DF6C26E7
        a                   adm:InitialDataCreation ;
        adm:logDate         "2010-01-14T12:49:21.493000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged and released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGE887270DA9223247
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-03T11:48:17.690000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG4186  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1KG4186 ;
        adm:facetIndex      15 ;
        adm:gitPath         "f3/MW1KG4186.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG4186 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG271EDFDAD3E68353 , bda:LG6696A0ADB5B52C5C , bda:LG9CCD25D0DF6C26E7 , bda:LGE887270DA9223247 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV17AEEF77C942E46A
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1978"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID2E89DAF0FE9F7161
        a                   bf:Lccn ;
        rdf:value           "78908560" .
    
    bdr:ID467786FE59DDE9B5
        a                   bdr:HollisId ;
        rdf:value           "014256812" .
    
    bdr:ID679412C59F87DAC2
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7602 .N33 1978" .
    
    bdr:MW1KG4186  a        bdo:Instance ;
        bf:identifiedBy     bdr:ID2E89DAF0FE9F7161 , bdr:ID467786FE59DDE9B5 , bdr:ID679412C59F87DAC2 ;
        bdo:biblioNote      "compiled from the surviving manuscripts in India, Nepal, and abroad by t. g. dhongthog rimpoche"@en ;
        bdo:extentStatement  "230 p." ;
        bdo:hasTitle        bdr:TT2A0DC74D086A77B5 , bdr:TTBDD568F57F5C19ED , bdr:TTC7164C0A57ECBBE5 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV17AEEF77C942E46A ;
        bdo:instanceHasReproduction  bdr:W1KG4186 ;
        bdo:instanceOf      bdr:WA1KG4186 ;
        bdo:note            bdr:NT17FB2AE2AB8FD91B ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "new delhi"@en ;
        bdo:publisherName   "t. g. dhongthog rimpoche"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "collected minor works (gsung thor bu) of sga-ston ngag-dbang legs-pa"@en ;
        skos:prefLabel      "gsung thor bu/_ngag dbang legs pa/"@bo-x-ewts .
    
    bdr:NT17FB2AE2AB8FD91B
        a                   bdo:Note ;
        bdo:noteText        "copy made available from the library of tibetan works and archives"@en .
    
    bdr:TT2A0DC74D086A77B5
        a                   bdo:OtherTitle ;
        rdfs:label          "collected minor works (gsung thor bu) of sga-ston ngag-dbang legs-pa"@en .
    
    bdr:TTBDD568F57F5C19ED
        a                   bdo:TitlePageTitle ;
        rdfs:label          "sga ston rdo rje 'chang 'jam mgon ngag dbang legs pa'i gsung thor bu/"@bo-x-ewts .
    
    bdr:TTC7164C0A57ECBBE5
        a                   bdo:Title ;
        rdfs:label          "gsung thor bu/_ngag dbang legs pa/"@bo-x-ewts .
}
