@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1CZ2423 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0D02B1C9CD88D9D3
        a                   adm:InitialDataCreation ;
        adm:logDate         "2007-11-12T14:25:39.093000+00:00"^^xsd:dateTime ;
        adm:logMessage      "new"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:LG3F83E67599FDC094
        a                   adm:UpdateData ;
        adm:logDate         "2007-12-27T14:26:11.991000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00017 .
    
    bda:LG51F7BA0A65A983F1
        a                   adm:UpdateData ;
        adm:logDate         "2007-12-12T16:11:40.440000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00017 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1CZ2423  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1CZ2423 ;
        adm:facetIndex      16 ;
        adm:gitPath         "94/MW1CZ2423.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1CZ2423 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG0D02B1C9CD88D9D3 , bda:LG3F83E67599FDC094 , bda:LG51F7BA0A65A983F1 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV2A2B41DF32175318
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2006"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID5A8466BCDC79F047
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7920 .C465 2006" .
    
    bdr:ID6EB4FD37E3931D88
        a                   bf:Isbn ;
        rdf:value           "7542111272" .
    
    bdr:IDB69788F5047B9C22
        a                   bf:Lccn ;
        rdf:value           "2006448397" .
    
    bdr:IDBF8280B7D8426E91
        a                   bdr:HollisId ;
        rdf:value           "014255349" .
    
    bdr:MW1CZ2423  a        bdo:Instance ;
        bf:identifiedBy     bdr:ID5A8466BCDC79F047 , bdr:ID6EB4FD37E3931D88 , bdr:IDB69788F5047B9C22 , bdr:IDBF8280B7D8426E91 ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "sangs rgyas mkhar gyis legs sgrig byas/"@bo-x-ewts ;
        bdo:extentStatement  "3, 6, 100 p. ; 22 cm." ;
        bdo:hasTitle        bdr:TT2BA215F8C4604858 , bdr:TT822F919C6B1C65EF , bdr:TTC28C4D8B0A89AB4E ;
        bdo:instanceEvent   bdr:EV2A2B41DF32175318 ;
        bdo:instanceHasReproduction  bdr:W1CZ2423 ;
        bdo:instanceOf      bdr:WA1CZ2423 ;
        bdo:note            bdr:NT1F9DDD1BD4F8FB5B ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "lan chou"@en ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "xiang xiong qu wang zhi hua zhan ci ji"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "zhang zhung chos dbang grags pa'i bstod tshogs/"@bo-x-ewts .
    
    bdr:NT1F9DDD1BD4F8FB5B
        a                   bdo:Note ;
        bdo:noteText        "tbrc holds ink print and digitally scanned images, tiffs and pdf files;"@en .
    
    bdr:TT2BA215F8C4604858
        a                   bdo:Title ;
        rdfs:label          "zhang zhung chos dbang grags pa'i bstod tshogs/"@bo-x-ewts .
    
    bdr:TT822F919C6B1C65EF
        a                   bdo:OtherTitle ;
        rdfs:label          "xiang xiong qu wang zhi hua zhan ci ji"@zh-latn-pinyin-x-ndia .
    
    bdr:TTC28C4D8B0A89AB4E
        a                   bdo:TitlePageTitle ;
        rdfs:label          "yul gangs ljongs kyi snyan dngags pa chen po zhang zhung chos dbang grags pa'i bstod tshogs/"@bo-x-ewts .
}
