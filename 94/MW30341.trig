@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW30341 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG413C1ECD669E0E79
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGE5E0DF850C84A85C
        a                   adm:UpdateData ;
        adm:logDate         "2011-09-02T13:10:48.439000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added shad"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW30341  a          adm:AdminData ;
        adm:adminAbout      bdr:MW30341 ;
        adm:facetIndex      12 ;
        adm:gitPath         "94/MW30341.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW30341 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG413C1ECD669E0E79 , bda:LGE5E0DF850C84A85C , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV1FF490CDCD9B4620
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2004"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID2C3E6AA2CB7C96C0
        a                   bdr:HollisId ;
        rdf:value           "014260742" .
    
    bdr:IDMW30341_79O3CZ9FS32A
        a                   bf:Isbn ;
        rdf:value           "9624509182" .
    
    bdr:IDMW30341_9QJEREVUGIL1
        a                   bf:Isbn ;
        rdf:value           "9789624502107" .
    
    bdr:MW30341  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID2C3E6AA2CB7C96C0 , bdr:IDMW30341_79O3CZ9FS32A , bdr:IDMW30341_9QJEREVUGIL1 ;
        bdo:biblioNote      "printed from computerized input"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "10, 4, 200 p." ;
        bdo:hasTitle        bdr:TT1334CA13DFD1CBAC , bdr:TT55A1732984B7633C , bdr:TT564B1BE051D6C5F0 ;
        bdo:illustrations   "col. ill." ;
        bdo:instanceEvent   bdr:EV1FF490CDCD9B4620 ;
        bdo:instanceHasReproduction  bdr:W30341 ;
        bdo:instanceOf      bdr:WA30341 ;
        bdo:note            bdr:NTD1395A7014782957 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "zhang kang /"@bo-x-ewts ;
        bdo:publisherName   "zhang kang gyi ling dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptHani , bdr:ScriptTibt ;
        skos:prefLabel      "sems sgron/"@bo-x-ewts , "xin ling ming deng"@zh-latn-pinyin-x-ndia .
    
    bdr:NTD1395A7014782957
        a                   bdo:Note ;
        bdo:noteText        "in tibetan and chinese language"@en .
    
    bdr:TT1334CA13DFD1CBAC
        a                   bdo:OtherTitle ;
        rdfs:label          "xin ling ming deng"@zh-latn-pinyin-x-ndia .
    
    bdr:TT55A1732984B7633C
        a                   bdo:ColophonTitle ;
        rdfs:label          "佛化人生"@zh-hans .
    
    bdr:TT564B1BE051D6C5F0
        a                   bdo:Title ;
        rdfs:label          "sems sgron/"@bo-x-ewts .
}
