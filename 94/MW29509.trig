@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW29509 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG8170DDDE99C61A2A
        a                   adm:UpdateData ;
        adm:logDate         "2008-06-19T09:18:05.499000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added pub info"@en ;
        adm:logWho          bdu:U00003 .
    
    bda:LGD43FC60FD8D2C0DE
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW29509  a          adm:AdminData ;
        adm:adminAbout      bdr:MW29509 ;
        adm:facetIndex      14 ;
        adm:gitPath         "94/MW29509.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW29509 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG8170DDDE99C61A2A , bda:LGD43FC60FD8D2C0DE , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV1AAFFB8AFFA3AC1D
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2005"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID03025EA647CADDA0
        a                   bdr:HollisId ;
        rdf:value           "014260013" .
    
    bdr:ID20EEEFE650048F0E
        a                   bf:Lccn ;
        rdf:value           "2005389075" .
    
    bdr:IDA601CBF56E0D1885
        a                   bf:ShelfMarkLcc ;
        rdf:value           "ND1046.T5 K66 2005" .
    
    bdr:IDMW29509_808E54C4UTOS
        a                   bf:Isbn ;
        rdf:value           "9788174721280" .
    
    bdr:IDMW29509_H5840EO3QQCD
        a                   bf:Isbn ;
        rdf:value           "8174721282" .
    
    bdr:MW29509  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID03025EA647CADDA0 , bdr:ID20EEEFE650048F0E , bdr:IDA601CBF56E0D1885 , bdr:IDMW29509_808E54C4UTOS , bdr:IDMW29509_H5840EO3QQCD ;
        bdo:authorshipStatement  "konchog lhadrepa/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:extentStatement  "91 leaves" ;
        bdo:hasTitle        bdr:TT52B42AE5359D5834 , bdr:TT93A14478A363322B , bdr:TTD5EEE9A782878271 ;
        bdo:illustrations   "all ill." ;
        bdo:instanceEvent   bdr:EV1AAFFB8AFFA3AC1D ;
        bdo:instanceHasReproduction  bdr:W29509 ;
        bdo:instanceOf      bdr:WA29509 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "new delhi"@en ;
        bdo:publisherName   "shechen publications ; tsering art school"@en ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "the sun : tsering art school manual for the basic drawing"@en ;
        skos:prefLabel      "bod kyi ri mo sngon 'gro'i ma dpe kun khyab nyi 'bum/"@bo-x-ewts .
    
    bdr:TT52B42AE5359D5834
        a                   bdo:CoverTitle ;
        rdfs:label          "tshe ring bzo rigs slob grwa'i bod kyi ri mo sngon 'gro'i ma dpe kun khyab nyi 'bum/"@bo-x-ewts .
    
    bdr:TT93A14478A363322B
        a                   bdo:OtherTitle ;
        rdfs:label          "the sun : tsering art school manual for the basic drawing"@en .
    
    bdr:TTD5EEE9A782878271
        a                   bdo:Title ;
        rdfs:label          "bod kyi ri mo sngon 'gro'i ma dpe kun khyab nyi 'bum/"@bo-x-ewts .
}
