@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW29329 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG53FAF560FE0A120D
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG6495E61B000692F1
        a                   adm:UpdateData ;
        adm:logDate         "2021-09-15T00:21:37.391000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalized infos"@en ;
        adm:logWho          bdu:U00007 .
    
    bda:LG7E3D4D4B9B26E053
        a                   adm:UpdateData ;
        adm:logDate         "2013-09-09T12:13:58.533000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG877697EA2110B948
        a                   adm:UpdateData ;
        adm:logDate         "2021-05-10T09:13:16.780000+00:00"^^xsd:dateTime ;
        adm:logMessage      "reviewed"@en ;
        adm:logWho          bdu:U00019 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW29329  a          adm:AdminData ;
        adm:adminAbout      bdr:MW29329 ;
        adm:facetIndex      19 ;
        adm:gitPath         "cb/MW29329.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW29329 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG53FAF560FE0A120D , bda:LG6495E61B000692F1 , bda:LG7E3D4D4B9B26E053 , bda:LG877697EA2110B948 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:CRF02D91F258862EEB
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P9710 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV14B3594598DFF573
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1985"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID4344F0960AE20663
        a                   bdr:HollisId ;
        rdf:value           "014259985" .
    
    bdr:ID85C514FEFD024B87
        a                   bf:Lccn ;
        rdf:value           "86145878" .
    
    bdr:ID9DC2444DD96D51C4
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3637.C5 B63 1985" .
    
    bdr:ID9DF7EBBC682DBDC6
        a                   bdr:HarvardShelfId ;
        rdf:value           "004487435-9" .
    
    bdr:MW29329  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID4344F0960AE20663 , bdr:ID85C514FEFD024B87 , bdr:ID9DC2444DD96D51C4 , bdr:ID9DF7EBBC682DBDC6 ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_TypeSet ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:creator         bdr:CRF02D91F258862EEB ;
        bdo:editionStatement  "di 1 ban"@zh-latn-pinyin ;
        bdo:extentStatement  "3 v. (19, 3294 p., [16] p. of plates))" ;
        bdo:hasTitle        bdr:TT006C07325FA37967 , bdr:TT671E341BD06FD9F3 , bdr:TTA6E099B5EE72062B , bdr:TTDBD5F1704E691540 ;
        bdo:illustrations   "col. ill." ;
        bdo:instanceEvent   bdr:EV14B3594598DFF573 ;
        bdo:instanceHasReproduction  bdr:W29329 ;
        bdo:instanceOf      bdr:WA29329 ;
        bdo:note            bdr:NT43B98243318611B3 ;
        bdo:numberOfVolumes  3 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptHani , bdr:ScriptTibt ;
        skos:prefLabel      "bod rgya tshig mdzod chen mo/"@bo-x-ewts , "880-01Bod rgya tshig mdzod chen mo =Zang Han da ci dian /Zhang Yisun zhu bian."@en-x-mixed , "藏漢大辭典"@zh-hant .
    
    bdr:NT43B98243318611B3
        a                   bdo:Note ;
        bdo:noteText        "in tibetan and chinese"@en .
    
    bdr:TT006C07325FA37967
        a                   bdo:Title ;
        rdfs:label          "bod rgya tshig mdzod chen mo/"@bo-x-ewts .
    
    bdr:TT671E341BD06FD9F3
        a                   bdo:CoverTitle ;
        rdfs:label          "880-01Bod rgya tshig mdzod chen mo =Zang Han da ci dian /Zhang Yisun zhu bian."@en-x-mixed .
    
    bdr:TTA6E099B5EE72062B
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "藏漢大辭典"@zh-hant .
    
    bdr:TTDBD5F1704E691540
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "zang han da ci dian"@zh-latn-pinyin-x-ndia .
}
