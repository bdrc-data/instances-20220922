@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW25159 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG421AA9842AAA6085
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGF7C42E1A586D3B36
        a                   adm:UpdateData ;
        adm:logDate         "2012-03-23T15:06:23.874000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added phyogs bsgrigs as genre"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW25159  a          adm:AdminData ;
        adm:adminAbout      bdr:MW25159 ;
        adm:facetIndex      14 ;
        adm:gitPath         "92/MW25159.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW25159 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG421AA9842AAA6085 , bda:LGF7C42E1A586D3B36 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CRAE0884E4DCA61E4B
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P8096 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV2589E2C218024500
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1998,2002printing"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID79DC547B960459B0
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3617 .D33 1998" .
    
    bdr:ID8F5DB4BA10FFFA0F
        a                   bf:Lccn ;
        rdf:value           "99933433" .
    
    bdr:IDF6FDF1D4A9D53897
        a                   bdr:HollisId ;
        rdf:value           "014259418" .
    
    bdr:IDMW25159_0S01POIR6HT0
        a                   bf:Isbn ;
        rdf:value           "9787542006936" .
    
    bdr:IDMW25159_ISYALZAHN9UO
        a                   bf:Isbn ;
        rdf:value           "7542006932" .
    
    bdr:MW25159  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID79DC547B960459B0 , bdr:ID8F5DB4BA10FFFA0F , bdr:IDF6FDF1D4A9D53897 , bdr:IDMW25159_0S01POIR6HT0 , bdr:IDMW25159_ISYALZAHN9UO ;
        bdo:authorshipStatement  "rtsom sgrig 'gan 'khur pa mkha' 'gro tshe ring /"@bo-x-ewts ;
        bdo:biblioNote      "printed from computerized input"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:creator         bdr:CRAE0884E4DCA61E4B ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "1, 492 p." ;
        bdo:hasTitle        bdr:TT0A0ABC08F76146DB , bdr:TTB786A0984795C302 ;
        bdo:instanceEvent   bdr:EV2589E2C218024500 ;
        bdo:instanceHasReproduction  bdr:IE25159 , bdr:W25159 ;
        bdo:instanceOf      bdr:WA25159 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:prefLabel      "dag yig phyogs bsgrigs mu tig tshom bu/"@bo-x-ewts .
    
    bdr:TT0A0ABC08F76146DB
        a                   bdo:OtherTitle ;
        rdfs:label          "dag yig skor gyi dpe rgyun dkon po 'ga' phyogs gcig tu bsgrigs pa mu tig tshom bu/"@bo-x-ewts .
    
    bdr:TTB786A0984795C302
        a                   bdo:Title ;
        rdfs:label          "dag yig phyogs bsgrigs mu tig tshom bu/"@bo-x-ewts .
}
