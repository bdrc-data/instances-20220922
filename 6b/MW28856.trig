@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW28856 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG3EE16E379E403CEC
        a                   adm:UpdateData ;
        adm:logDate         "2008-01-08T13:42:12.966000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added note"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LG55DC14E9D09E931F
        a                   adm:UpdateData ;
        adm:logDate         "2008-01-09T09:58:23.734000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00003 .
    
    bda:LGCD605BF456D0867D
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW28856  a          adm:AdminData ;
        adm:adminAbout      bdr:MW28856 ;
        adm:facetIndex      18 ;
        adm:gitPath         "6b/MW28856.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW28856 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG3EE16E379E403CEC , bda:LG55DC14E9D09E931F , bda:LGCD605BF456D0867D , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CRE70C833B2C2F66C7
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P280 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EVBE85C90105527EEC
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1996"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID119E56A85F8FF3DB
        a                   bdr:HollisId ;
        rdf:value           "014259808" .
    
    bdr:ID17CAF63AFA86D0AB
        a                   bdr:HarvardShelfId ;
        rdf:value           "008084394-8" .
    
    bdr:ID825078F98B410670
        a                   bf:Lccn ;
        rdf:value           "98900309" .
    
    bdr:IDEA62B639B984E555
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7670.4 .L44 1996" .
    
    bdr:IDMW28856_LKVH5N715177
        a                   bf:Isbn ;
        rdf:value           "9787542005380" .
    
    bdr:IDMW28856_W1LPD4RWFHCO
        a                   bf:Isbn ;
        rdf:value           "7542005383" .
    
    bdr:MW28856  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID119E56A85F8FF3DB , bdr:ID17CAF63AFA86D0AB , bdr:ID825078F98B410670 , bdr:IDEA62B639B984E555 , bdr:IDMW28856_LKVH5N715177 , bdr:IDMW28856_W1LPD4RWFHCO ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:creator         bdr:CRE70C833B2C2F66C7 ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2, 616 p." ;
        bdo:hasTitle        bdr:TT37C76116B49CFF29 , bdr:TT8F0135F0F524F397 , bdr:TTAA140F3A898DD917 ;
        bdo:instanceEvent   bdr:EVBE85C90105527EEC ;
        bdo:instanceHasReproduction  bdr:W28856 ;
        bdo:instanceOf      bdr:WA1KG89134 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "Legs par bśad pa Bkaʾ-gdams rin po cheʾi gsuṅ gi gces btus nor buʾi baṅ mdzod /[ʾgan ʾkhriʾi dpe sgrig pa Mkhaʾ-ʾgro-tshe-riṅ]."@en-x-mixed , "ga dang pai da shi jian yan ji"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "legs par bshad pa bka' gdams rin po che'i gsung gi gces btus nor bu'i bang mdzod/"@bo-x-ewts .
    
    bdr:TT37C76116B49CFF29
        a                   bdo:CoverTitle ;
        rdfs:label          "Legs par bśad pa Bkaʾ-gdams rin po cheʾi gsuṅ gi gces btus nor buʾi baṅ mdzod /[ʾgan ʾkhriʾi dpe sgrig pa Mkhaʾ-ʾgro-tshe-riṅ]."@en-x-mixed .
    
    bdr:TT8F0135F0F524F397
        a                   bdo:Title ;
        rdfs:label          "legs par bshad pa bka' gdams rin po che'i gsung gi gces btus nor bu'i bang mdzod/"@bo-x-ewts .
    
    bdr:TTAA140F3A898DD917
        a                   bdo:ColophonTitle ;
        rdfs:label          "ga dang pai da shi jian yan ji"@zh-latn-pinyin-x-ndia .
}
