@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG12792 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG36558424CF930EB1
        a                   adm:InitialDataCreation ;
        adm:logDate         "2012-08-30T15:45:58.043000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG12792  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG12792 ;
        adm:facetIndex      13 ;
        adm:gitPath         "14/MW1KG12792.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG12792 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG36558424CF930EB1 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVABC2FC95DFD262C7
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1980"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID4087882AFA94E54F
        a                   bdr:HollisId ;
        rdf:value           "014255972" .
    
    bdr:ID4385E7F1F5B8555A
        a                   bdr:HarvardShelfId ;
        rdf:value           "011202569-2" .
    
    bdr:IDC2306A23A5E919DC
        a                   bf:Lccn ;
        rdf:value           "80900958" .
    
    bdr:IDEFEF82BE0E676341
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7662.4 .N3415 1980" .
    
    bdr:MW1KG12792  a       bdo:Instance ;
        bf:identifiedBy     bdr:ID4087882AFA94E54F , bdr:ID4385E7F1F5B8555A , bdr:IDC2306A23A5E919DC , bdr:IDEFEF82BE0E676341 ;
        bdo:biblioNote      "reproduced from rare manuscripts from the library of the dowager gyalmo of sikkim"@en ;
        bdo:extentStatement  "2 v." ;
        bdo:hasTitle        bdr:TT428AEA9305093ECA , bdr:TT4BF7F4653BED2116 , bdr:TTC16303F3DA46075C ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EVABC2FC95DFD262C7 ;
        bdo:instanceHasReproduction  bdr:W1KG12792 ;
        bdo:instanceOf      bdr:WA1KG12792 ;
        bdo:numberOfVolumes  2 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "gangtok, sikkim"@en ;
        bdo:publisherName   "dzongsar chhentse labrang"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "rig 'dzin srog sgrub kyi rgyab chos : supportive texts explaining the fundamentals and ritual practices of the rediscovered teaching of the rig 'dzin srog sgrub cycle including the musical notation texts"@en ;
        skos:prefLabel      "rig 'dzin srog sgrub kyi rgyab chos/"@bo-x-ewts .
    
    bdr:TT428AEA9305093ECA
        a                   bdo:CoverTitle ;
        rdfs:label          "Rig ʼdzin srog sgrub kyi rgyab chos :supportive texts explaining the fundamentals and ritual practices of the rediscovered teachings of the Rig ʼdzin srog sgrub cycle including the musical notation texts /by Lha-btsun Nam-mkhaʼ-ʼJigsmed."@en-x-mixed .
    
    bdr:TT4BF7F4653BED2116
        a                   bdo:Title ;
        rdfs:label          "rig 'dzin srog sgrub kyi rgyab chos/"@bo-x-ewts .
    
    bdr:TTC16303F3DA46075C
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rig 'dzin srog sgrub kyi rgyab chos : supportive texts explaining the fundamentals and ritual practices of the rediscovered teaching of the rig 'dzin srog sgrub cycle including the musical notation texts"@en .
}
