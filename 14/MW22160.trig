@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW22160 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG4C19A53D486BDA7A
        a                   adm:UpdateData ;
        adm:logDate         "2014-07-10T12:32:11.724000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold and completed cataloging"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGC58209F51428E498
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW22160  a          adm:AdminData ;
        adm:adminAbout      bdr:MW22160 ;
        adm:facetIndex      13 ;
        adm:gitPath         "14/MW22160.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW22160 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG4C19A53D486BDA7A , bda:LGC58209F51428E498 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:CR439032B343D4F379
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P7436 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV47960531501CA946
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1988/1989"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID6F7A0740443CDBAD
        a                   bf:Isbn ;
        rdf:value           "7223002312" .
    
    bdr:ID7D1EEB8913B7B8EF
        a                   bdr:HollisId ;
        rdf:value           "014258669" .
    
    bdr:MW22160  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID6F7A0740443CDBAD , bdr:ID7D1EEB8913B7B8EF ;
        bdo:authorshipStatement  "chab mdo'i lag bris ma gzhir bzung phyogs sgrig byas pa/ rtsom sgrig 'gan 'khur ba thang dpon tshe stobs/"@bo-x-ewts ;
        bdo:biblioNote      "isbn: v. 1: 7223002312 v. 2: 7223002892 v. 3: 7223031406"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_TypeSet ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:creator         bdr:CR439032B343D4F379 ;
        bdo:editionStatement  "par thengs dang po/"@bo-x-ewts ;
        bdo:extentStatement  "3 v." ;
        bdo:hasTitle        bdr:TT0B16009105B54F39 , bdr:TT3767258060EF15BA , bdr:TTCB28567FEA9F3015 , bdr:TTCEFC38C85F5FC28D , bdr:TTE4798357F4CBEAB9 ;
        bdo:instanceEvent   bdr:EV47960531501CA946 ;
        bdo:instanceHasReproduction  bdr:W22160 ;
        bdo:instanceOf      bdr:WA1KG17182 ;
        bdo:numberOfVolumes  3 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "lha sa/"@bo-x-ewts ;
        bdo:publisherName   "bod ljongs mi dmangs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "格萨尔王传 --朱古兵器宗"@zh-hans ;
        skos:prefLabel      "gru gu go rdzong /"@bo-x-ewts .
    
    bdr:TT0B16009105B54F39
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "ge sa er wang zhuan-- zhu gu bing qi zong"@zh-latn-pinyin-x-ndia .
    
    bdr:TT3767258060EF15BA
        a                   bdo:CoverTitle ;
        rdfs:label          "gling ge sar rgyal po'i sgrung gru gu go rdzong /"@bo-x-ewts .
    
    bdr:TTCB28567FEA9F3015
        a                   bdo:TitlePageTitle ;
        rdfs:label          "'dzam gling skyong ba'i pho lha ge sar dmag gi rgyal po'i rtogs brjod las byang bdud gru gu g.yul rgyal stobs chen thog rgod rgyal po mnga' 'bangs dang bcas dbang du 'dus shing go mtshon g.yang du blangs pa'i rnam thar yid 'phrog snying gi dga' ston/"@bo-x-ewts .
    
    bdr:TTCEFC38C85F5FC28D
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "格萨尔王传 --朱古兵器宗"@zh-hans .
    
    bdr:TTE4798357F4CBEAB9
        a                   bdo:Title ;
        rdfs:label          "gru gu go rdzong /"@bo-x-ewts .
}
