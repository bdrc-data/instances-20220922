@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW20522 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG1129003E5D84F5CB
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-09T14:18:01.148000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LG73688BB4CAD14321
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW20522  a          adm:AdminData ;
        adm:adminAbout      bdr:MW20522 ;
        adm:facetIndex      15 ;
        adm:gitPath         "14/MW20522.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW20522 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG1129003E5D84F5CB , bda:LG73688BB4CAD14321 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV794D1883795261DE
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1972"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID52FBAAF0E969698F
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7679.4 .N33 1973" .
    
    bdr:ID8D2DC63B1EB612CE
        a                   bdr:HarvardShelfId ;
        rdf:value           "007366334-4" .
    
    bdr:ID96AA3B8D6EBBBB8A
        a                   bf:Lccn ;
        rdf:value           "73901855" .
    
    bdr:IDBE80BFD75E1EF48D
        a                   bdr:HollisId ;
        rdf:value           "014258191" .
    
    bdr:MW20522  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID52FBAAF0E969698F , bdr:ID8D2DC63B1EB612CE , bdr:ID96AA3B8D6EBBBB8A , bdr:IDBE80BFD75E1EF48D ;
        bdo:biblioNote      "reproduced from manuscripts from the library of ri-bo-che rje-drun rin-po-che of padma-bkod by tseten dorji"@en ;
        bdo:extentStatement  "572 columns" ;
        bdo:hasTitle        bdr:TT93CD62920AD6E9C4 , bdr:TTA589F3E3BB42FBEF , bdr:TTD3AB06583F5B4129 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV794D1883795261DE ;
        bdo:instanceHasReproduction  bdr:W20522 ;
        bdo:instanceOf      bdr:WA20522 ;
        bdo:note            bdr:NT9895B234E5B6D042 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "tezu, dist. lohit"@en ;
        bdo:publisherName   "tibetan nyingmapa monastery"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "phyag chen khrid yig of nag-dban-bstan-pa'i-ni-ma and other texts on the mahamudra and na ro chos drug precepts of stag-lung dkar-brgyud-pa tradition"@en-x-mixed ;
        skos:prefLabel      "phyag chen dang nA ro chos drug gi khrid skor stag lung lugs/"@bo-x-ewts .
    
    bdr:NT9895B234E5B6D042
        a                   bdo:Note ;
        bdo:noteText        "tbrc holds scanned images, tiffs and pdf files\n0599."@en .
    
    bdr:TT93CD62920AD6E9C4
        a                   bdo:Title ;
        rdfs:label          "phyag chen dang nA ro chos drug gi khrid skor stag lung lugs/"@bo-x-ewts .
    
    bdr:TTA589F3E3BB42FBEF
        a                   bdo:TitlePageTitle ;
        rdfs:label          "phyag chen khrid yig of nag-dban-bstan-pa'i-ni-ma and other texts on the mahamudra and na ro chos drug precepts of stag-lung dkar-brgyud-pa tradition"@en-x-mixed .
    
    bdr:TTD3AB06583F5B4129
        a                   bdo:CoverTitle ;
        rdfs:label          "Phyag chen khrid yig of Ṅag-dbaṅ-bstan-paʼi-ñi-ma and other texts on the Mahamudra and Na ro chos drug precepts of Stag-luṅ-pa Dkar-brgyud-pa tradition.Reproduced from MSS. from the libiary of Ri-bo-che Rje-druṅ Rin-po-che of Padma-bkod by Tseten Dorji."@en-x-mixed .
}
