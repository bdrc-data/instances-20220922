@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW20541 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG1901E657958908E8
        a                   adm:UpdateData ;
        adm:logDate         "2011-07-29T14:26:53.787000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added shad"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG435AD5F6C6A3C47F
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-09T14:18:08.872000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LG47684DAA5D92D7DF
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW20541  a          adm:AdminData ;
        adm:adminAbout      bdr:MW20541 ;
        adm:facetIndex      14 ;
        adm:gitPath         "bd/MW20541.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW20541 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG1901E657958908E8 , bda:LG435AD5F6C6A3C47F , bda:LG47684DAA5D92D7DF , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV8BAF32F6A4A8BAAF
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1978"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID215329BC03682ACC
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ4860.C354 T55734 1978" .
    
    bdr:ID4720F9AA339D62C5
        a                   bdr:HollisId ;
        rdf:value           "014258200" .
    
    bdr:IDF10971E9E859C3B4
        a                   bf:Lccn ;
        rdf:value           "78906167" .
    
    bdr:MW20541  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID215329BC03682ACC , bdr:ID4720F9AA339D62C5 , bdr:IDF10971E9E859C3B4 ;
        bdo:authorshipStatement  "by the fourth bde-chen-chos-'khor yons-'dzin 'jam-dpal-dpa'-bo"@en ;
        bdo:biblioNote      "reproduced from a manuscript from the he-mi stag-tshan-ras-pa bla-bran"@en ;
        bdo:extentStatement  "2 v." ;
        bdo:hasTitle        bdr:TT0A0DD0494182BAB6 , bdr:TT4671CA15136DED32 , bdr:TTB5C61BAE050EC9B7 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV8BAF32F6A4A8BAAF ;
        bdo:instanceHasReproduction  bdr:W20541 ;
        bdo:instanceOf      bdr:WA20541 ;
        bdo:note            bdr:NT31CE72F520D55986 ;
        bdo:numberOfVolumes  2 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "darjeeling, w.b."@en ;
        bdo:publisherName   "kargyud sungrab nyamso khang"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "snan rgyud yid bzin nor bu'i rnam bsad yan gsal gyi zin bris bde mchog 'khor lo mtha' yas pa'i nes gnas rab tu gsal ba : a detailed commentary on padma-dkar-po's study of the snan brgyud precepts of cakrasamvara practice known as the snan rgyud yid bzin nor bu legs par bsad pa'i rgyal mtshan gyi rtser bton pa dnos grub kyi char 'bebs"@en-x-mixed ;
        skos:prefLabel      "snyan rgyud yid bzhin nor bu'i rnam bshad yang gsal gyi zin bris/"@bo-x-ewts .
    
    bdr:NT31CE72F520D55986
        a                   bdo:Note ;
        bdo:noteText        "in tibetan; preface in english"@en .
    
    bdr:TT0A0DD0494182BAB6
        a                   bdo:TitlePageTitle ;
        rdfs:label          "snyan rgyud yid bzhin nor bu'i rnam bshad yang gsal gyi zin bris bde mchog 'khor lo mtha' yas pa'i nges gnas rab tu gsal ba/"@bo-x-ewts .
    
    bdr:TT4671CA15136DED32
        a                   bdo:TitlePageTitle ;
        rdfs:label          "snan rgyud yid bzin nor bu'i rnam bsad yan gsal gyi zin bris bde mchog 'khor lo mtha' yas pa'i nes gnas rab tu gsal ba : a detailed commentary on padma-dkar-po's study of the snan brgyud precepts of cakrasamvara practice known as the snan rgyud yid bzin nor bu legs par bsad pa'i rgyal mtshan gyi rtser bton pa dnos grub kyi char 'bebs"@en-x-mixed .
    
    bdr:TTB5C61BAE050EC9B7
        a                   bdo:Title ;
        rdfs:label          "snyan rgyud yid bzhin nor bu'i rnam bshad yang gsal gyi zin bris/"@bo-x-ewts .
}
