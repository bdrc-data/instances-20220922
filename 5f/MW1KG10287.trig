@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG10287 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG615273FD2119CE8A
        a                   adm:UpdateData ;
        adm:logDate         "2013-06-25T09:36:43.362000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGBE350B6A2148E570
        a                   adm:InitialDataCreation ;
        adm:logDate         "2011-08-22T10:49:01.343000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG10287  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG10287 ;
        adm:facetIndex      13 ;
        adm:gitPath         "5f/MW1KG10287.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG10287 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG615273FD2119CE8A , bda:LGBE350B6A2148E570 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV8FCF17F58333C804
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1978"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID117B27FCAF597771
        a                   bf:Lccn ;
        rdf:value           "78903297" .
    
    bdr:ID14ECBA631C323D1E
        a                   bdr:HollisId ;
        rdf:value           "014255616" .
    
    bdr:ID773D1D4D6B8D2E95
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3748 .R49 1978" .
    
    bdr:MW1KG10287  a       bdo:Instance ;
        bf:identifiedBy     bdr:ID117B27FCAF597771 , bdr:ID14ECBA631C323D1E , bdr:ID773D1D4D6B8D2E95 ;
        bdo:biblioNote      "reproduced from a manuscript from tibet by sprul-sku kun-bzang-rgya-mtsho"@en ;
        bdo:extentStatement  "268 columns" ;
        bdo:hasTitle        bdr:TT30DF02BF2A13D463 , bdr:TT3B1735F3904A03E8 , bdr:TT612B34E177C8C878 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV8FCF17F58333C804 ;
        bdo:instanceHasReproduction  bdr:W1KG10287 ;
        bdo:instanceOf      bdr:WA1KG10287 ;
        bdo:note            bdr:NT7E48142AA925260B ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "delhi"@en ;
        bdo:publisherName   "tulku kunsang gyatso"@en ;
        bdo:script          bdr:ScriptDbuCan ;
        skos:altLabel       "an account of the life of king sron-btsan-sgam-po and his two wives the chinese and nepalese princesses"@en-x-mixed ;
        skos:prefLabel      "rgya gza' bal bza'i rnam thar/ (dbu can bris ma/)"@bo-x-ewts .
    
    bdr:NT7E48142AA925260B
        a                   bdo:Note ;
        bdo:noteText        "in tibetan; preface in english"@en .
    
    bdr:TT30DF02BF2A13D463
        a                   bdo:OtherTitle ;
        rdfs:label          "an account of the life of king sron-btsan-sgam-po and his two wives the chinese and nepalese princesses"@en-x-mixed .
    
    bdr:TT3B1735F3904A03E8
        a                   bdo:Title ;
        rdfs:label          "rgya gza' bal bza'i rnam thar/ (dbu can bris ma/)"@bo-x-ewts .
    
    bdr:TT612B34E177C8C878
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rgyal po srong btsan sgam po'i 'khrungs rabs dang / rgya bza' bal bza'i rnam thar/"@bo-x-ewts .
}
