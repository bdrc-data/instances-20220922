@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW27913 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG38FF3F4DC2EF8CE5
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG5B062482E4CF7782
        a                   adm:UpdateData ;
        adm:logDate         "2013-04-22T12:29:56.531000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added source printery"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGF88543C1AF8485F2
        a                   adm:UpdateData ;
        adm:logDate         "2013-04-15T10:26:48.212000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW27913  a          adm:AdminData ;
        adm:adminAbout      bdr:MW27913 ;
        adm:facetIndex      15 ;
        adm:gitPath         "1b/MW27913.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW27913 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG38FF3F4DC2EF8CE5 , bda:LG5B062482E4CF7782 , bda:LGF88543C1AF8485F2 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV9CBCAA12C1EE6C62
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1985"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID639686A2680D30F4
        a                   bdr:HollisId ;
        rdf:value           "014259731" .
    
    bdr:ID8AC61D2D2B845BE7
        a                   bf:Lccn ;
        rdf:value           "85903868" .
    
    bdr:IDC9AE43F57DA4B056
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7576 .T48 1985" .
    
    bdr:IDCCA8FB834D46859E
        a                   bdr:PL480 ;
        rdf:value           "i-tib-2804" .
    
    bdr:MW27913  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID639686A2680D30F4 , bdr:ID8AC61D2D2B845BE7 , bdr:IDC9AE43F57DA4B056 , bdr:IDCCA8FB834D46859E ;
        bdo:authorshipStatement  "by rag-mgo mchog-sprul thub-bstan-bsad-sgrub-rgya-mtsho (1890-1973) alias padma-kun-bzan-ran-grol"@en ;
        bdo:biblioNote      "reproduced from a clear print from the eastern tibetan blocks"@en ;
        bdo:extentStatement  "157 p." ;
        bdo:hasSourcePrintery  bdr:G1326 ;
        bdo:hasTitle        bdr:TT421AB952F6D4176A , bdr:TT6735362F67693D4F , bdr:TT8C1E0276155D468E , bdr:TTCCC0976B26BEDC9F ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV9CBCAA12C1EE6C62 ;
        bdo:instanceHasReproduction  bdr:W27913 ;
        bdo:instanceOf      bdr:WA27913 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:publisherLocation  "sonada, darjeeling, w.b."@en ;
        bdo:publisherName   "konchhog lhadrepa"@en ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "a brief history of the various religious traditions of tibet which have contributed to the making of the ris-med movement"@en-x-mixed ;
        skos:prefLabel      "ris med chos 'byung /"@bo-x-ewts .
    
    bdr:TT421AB952F6D4176A
        a                   bdo:Title ;
        rdfs:label          "ris med chos 'byung /"@bo-x-ewts .
    
    bdr:TT6735362F67693D4F
        a                   bdo:Subtitle ;
        rdfs:label          "a brief history of the various religious traditions of tibet which have contributed to the making of the ris-med movement"@en-x-mixed .
    
    bdr:TT8C1E0276155D468E
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rgyal bstan grub mtha' ris med kyi chos 'byun mdor bsdus yid ches dad pa'i sin rta"@bo-alalc97 .
    
    bdr:TTCCC0976B26BEDC9F
        a                   bdo:OtherTitle ;
        rdfs:label          "rgyal bstan grub mtha' ris med kyi chos 'byung mdor bsdus yid ches dad pa'i shing rta/"@bo-x-ewts .
}
