@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW27610 {
    bda:LG07D9762ABDDD264A
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGBEB29E77558037B7
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-30T12:55:43.652000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW27610  a          adm:AdminData ;
        adm:adminAbout      bdr:MW27610 ;
        adm:facetIndex      13 ;
        adm:gitPath         "d5/MW27610.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW27610 ;
        adm:logEntry        bda:LG07D9762ABDDD264A , bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDS7T15T6YORU18 , bda:LGBEB29E77558037B7 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVAF03534C6AA531AD
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1977"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID5238E09F7A317866
        a                   bdr:HollisId ;
        rdf:value           "014259707" .
    
    bdr:ID79CCBB5951BC112E
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7662.4 .S47" .
    
    bdr:IDE266C1EA307B597B
        a                   bf:Lccn ;
        rdf:value           "78903286" .
    
    bdr:MW27610  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID5238E09F7A317866 , bdr:ID79CCBB5951BC112E , bdr:IDE266C1EA307B597B ;
        bdo:authorshipStatement  "by rog bande ses-rab-'od"@en ;
        bdo:biblioNote      "edited from a tibetan blockprint by the ven. 'khor-gdon gter-sprul 'chi-med-rig-'dzin"@en ;
        bdo:extentStatement  "234 p." ;
        bdo:hasTitle        bdr:TTA02D4649AFEE8DBC , bdr:TTD16DD3128C97E1EA , bdr:TTF47EC3C8239DE585 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EVAF03534C6AA531AD ;
        bdo:instanceHasReproduction  bdr:W27610 ;
        bdo:instanceOf      bdr:WA27610 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "nemo, ladakh"@en ;
        bdo:publisherName   "tshul khrims 'jam dbyangs/"@bo-x-ewts ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "a detailed survey of comparative siddhanta in the context of mahayana buddhism and in particular the rnin-ma-pa tradition of tibet"@en-x-mixed ;
        skos:prefLabel      "grub mtha' so so'i bzhed tshul gzhung gsal bar ston pa chos 'byung grub mtha' chen po bstan pa'i sgron me/"@bo-x-ewts .
    
    bdr:TTA02D4649AFEE8DBC
        a                   bdo:Title ;
        rdfs:label          "grub mtha' so so'i bzhed tshul gzhung gsal bar ston pa chos 'byung grub mtha' chen po bstan pa'i sgron me/"@bo-x-ewts .
    
    bdr:TTD16DD3128C97E1EA
        a                   bdo:TitlePageTitle ;
        rdfs:label          "grub mtha' so so'i bzed tshul gzun gsal bar ston pa chos 'byung grub mtha' chen po bstan pa'i sgron me"@bo-alalc97 .
    
    bdr:TTF47EC3C8239DE585
        a                   bdo:Subtitle ;
        rdfs:label          "a detailed survey of comparative siddhanta in the context of mahayana buddhism and in particular the rnin-ma-pa tradition of tibet"@en-x-mixed .
}
