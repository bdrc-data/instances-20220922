@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW175 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG1481B9AE71DB62D3
        a                   adm:UpdateData ;
        adm:logDate         "2008-06-02T10:30:17.617000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00003 .
    
    bda:LGA0BBA9969F469CB8
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW175  a            adm:AdminData ;
        adm:adminAbout      bdr:MW175 ;
        adm:facetIndex      15 ;
        adm:gitPath         "d5/MW175.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW175 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG1481B9AE71DB62D3 , bda:LGA0BBA9969F469CB8 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EVC60E1FEAC800D017
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1989/1991"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID84570A72B6C42E91
        a                   bf:Isbn ;
        rdf:value           "7223002492" .
    
    bdr:IDB755D6653540A1A9
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7935.N347 A3 1989" .
    
    bdr:IDEBF5A962C9550432
        a                   bdr:HollisId ;
        rdf:value           "014254788" .
    
    bdr:IDF59879A52FA1F3D0
        a                   bf:Lccn ;
        rdf:value           "95906220" .
    
    bdr:MW175  a            bdo:Instance ;
        bf:identifiedBy     bdr:ID84570A72B6C42E91 , bdr:IDB755D6653540A1A9 , bdr:IDEBF5A962C9550432 , bdr:IDF59879A52FA1F3D0 ;
        bdo:authorshipStatement  "ngag dbang blo bzang rgya mtshos brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi 1"@bo-x-ewts ;
        bdo:extentStatement  "3 v." ;
        bdo:hasTitle        bdr:TT6E41AF81FFF23422 , bdr:TTA6EDC549BEFA08F3 , bdr:TTB1DD6EC0CA961F5E , bdr:TTD0516C0C54E4D7AF ;
        bdo:illustrations   "ill." ;
        bdo:instanceEvent   bdr:EVC60E1FEAC800D017 ;
        bdo:instanceHasReproduction  bdr:W175 ;
        bdo:instanceOf      bdr:WA175 ;
        bdo:note            bdr:NTB82825CC2D21EA23 ;
        bdo:numberOfVolumes  3 ;
        bdo:publisherLocation  "lha sa/"@bo-x-ewts ;
        bdo:publisherName   "bod ljongs mi dmangs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "wu shi da lai zhuan"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "ngag dbang blo bzang rgya mtsho'i rang rnam/"@bo-x-ewts .
    
    bdr:NTB82825CC2D21EA23
        a                   bdo:Note ;
        bdo:noteText        "vols. 5-7  in the 28 vol. printed gsung 'bum in the potala collection. The collection also includes a two volume manuscript of the dag snang rgya can.\ntoh 5588.\nismeo\nca (ka) . 364 ff.\ncha (kha). 281 ff.\nja (ga). 246 ff."@en .
    
    bdr:TT6E41AF81FFF23422
        a                   bdo:CoverTitle ;
        rdfs:label          "ngag dbang blo bzang rgya mtsho'i rnam thar/"@bo-x-ewts .
    
    bdr:TTA6EDC549BEFA08F3
        a                   bdo:TitlePageTitle ;
        rdfs:label          "za hor gyi bande ngag dbang blo bzang rgya mtsho'i 'di snang 'khrul pa'i rol rtsed rtogs brjod kyi tshul du bkod pa du kU la'i gos bzang /"@bo-x-ewts .
    
    bdr:TTB1DD6EC0CA961F5E
        a                   bdo:Title ;
        rdfs:label          "ngag dbang blo bzang rgya mtsho'i rang rnam/"@bo-x-ewts .
    
    bdr:TTD0516C0C54E4D7AF
        a                   bdo:ColophonTitle ;
        rdfs:label          "wu shi da lai zhuan"@zh-latn-pinyin-x-ndia .
}
