@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW19952 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG25372108E679CCE5
        a                   adm:UpdateData ;
        adm:logDate         "2008-06-19T11:37:40.609000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00003 .
    
    bda:LGB00F6627B3BFDA70
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW19952  a          adm:AdminData ;
        adm:adminAbout      bdr:MW19952 ;
        adm:facetIndex      15 ;
        adm:gitPath         "d5/MW19952.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW19952 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDS5ZNKT5O6YISF , bda:LG25372108E679CCE5 , bda:LGB00F6627B3BFDA70 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CR5F957F1A8CB1DBDE
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P6255 ;
        bdo:role            bdr:R0ER0013 .
    
    bdr:EVE397F28A365B3B5A
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1995"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID8AD31FD7D905DE9E
        a                   bf:Lccn ;
        rdf:value           "98901124" .
    
    bdr:IDA4E7AD2865D4B94E
        a                   bdr:HollisId ;
        rdf:value           "014254924" .
    
    bdr:IDBBFC33E432B8DC7E
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3639 .N6 1995" .
    
    bdr:IDMW19952_BI1VY016SQ2B
        a                   bf:Isbn ;
        rdf:value           "9787542103161" .
    
    bdr:IDMW19952_M89PDNUWBHIM
        a                   bf:Isbn ;
        rdf:value           "7542103164" .
    
    bdr:MW19952  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID8AD31FD7D905DE9E , bdr:IDA4E7AD2865D4B94E , bdr:IDBBFC33E432B8DC7E , bdr:IDMW19952_BI1VY016SQ2B , bdr:IDMW19952_M89PDNUWBHIM ;
        bdo:authorshipStatement  "go ba dbyig dang hri zhu'e lis nas phyogs bsgrigs byas ; pad grags kyi phyag bris mdzad/"@bo-x-ewts ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:creator         bdr:CR5F957F1A8CB1DBDE ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2, 51 p." ;
        bdo:hasTitle        bdr:TT4106BFD1C0C8162F , bdr:TT739D4DBB0A4CB990 , bdr:TTECBD4D73741CC2EF ;
        bdo:instanceEvent   bdr:EVE397F28A365B3B5A ;
        bdo:instanceHasReproduction  bdr:W19952 ;
        bdo:instanceOf      bdr:WA19952 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "lan kro'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "zang wen suo xie ci shou ce"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "skung yig be'u bum/"@bo-x-ewts .
    
    bdr:TT4106BFD1C0C8162F
        a                   bdo:Title ;
        rdfs:label          "zang wen suo xie ci shou ce"@zh-latn-pinyin-x-ndia .
    
    bdr:TT739D4DBB0A4CB990
        a                   bdo:TitlePageTitle ;
        rdfs:label          "ngo mtshar ba'i gangs can 'phrul ldan skung yig be'u bum/"@bo-x-ewts .
    
    bdr:TTECBD4D73741CC2EF
        a                   bdo:Title ;
        rdfs:label          "skung yig be'u bum/"@bo-x-ewts .
}
