@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21602 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG2C2B24F55E026D7E
        a                   adm:UpdateData ;
        adm:logDate         "2014-02-28T10:23:34.656000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold and completed cataloging"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG49C8FC75E747265F
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21602  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21602 ;
        adm:facetIndex      16 ;
        adm:gitPath         "66/MW21602.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21602 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG2C2B24F55E026D7E , bda:LG49C8FC75E747265F , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV7E9EF97AF26CEFC2
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2001"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID9FD3FCB152AAA772
        a                   bf:Lccn ;
        rdf:value           "2004309340" .
    
    bdr:IDBD19C9BA89AFA4EA
        a                   bdr:HollisId ;
        rdf:value           "014258351" .
    
    bdr:IDEC9E1CCC372A95E7
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3748.G4 S47 2001" .
    
    bdr:IDMW21602_87SMBX5HQ6RD
        a                   bf:Isbn ;
        rdf:value           "9787805890296" .
    
    bdr:IDMW21602_E2ECFC5Q1EKG
        a                   bf:Isbn ;
        rdf:value           "7805890293" .
    
    bdr:MW21602  a          bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID9FD3FCB152AAA772 , bdr:IDBD19C9BA89AFA4EA , bdr:IDEC9E1CCC372A95E7 , bdr:IDMW21602_87SMBX5HQ6RD , bdr:IDMW21602_E2ECFC5Q1EKG ;
        bdo:authorshipStatement  "krung go spyi tshogs tshan rig khang grangs nyung mi rigs rtsom rig so'u dang bod rang ljongs spyi tshogs tshan rig khang /"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "dpar gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "47, 358 p." ;
        bdo:hasTitle        bdr:TT0A60BFF2628116E1 , bdr:TT0CD5DCF43BDBCD98 , bdr:TT1E69B4774F777621 , bdr:TTCE74CC1C18D9663A ;
        bdo:instanceEvent   bdr:EV7E9EF97AF26CEFC2 ;
        bdo:instanceHasReproduction  bdr:W21602 ;
        bdo:instanceOf      bdr:WA21602 ;
        bdo:note            bdr:NT83A37D35E7A64FFC ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "lha sa/"@bo-x-ewts ;
        bdo:publisherName   "bod ljongs bod yig dpe rnying dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG16786 ;
        bdo:seriesNumber    "1" ;
        skos:altLabel       "天界篇"@zh-hans ;
        skos:prefLabel      "lha gling /"@bo-x-ewts .
    
    bdr:NT83A37D35E7A64FFC
        a                   bdo:Note ;
        bdo:noteText        "in tibetan"@en .
    
    bdr:TT0A60BFF2628116E1
        a                   bdo:Title ;
        rdfs:label          "lha gling /"@bo-x-ewts .
    
    bdr:TT0CD5DCF43BDBCD98
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "天界篇"@zh-hans .
    
    bdr:TT1E69B4774F777621
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "tian jie pian"@zh-latn-pinyin-x-ndia .
    
    bdr:TTCE74CC1C18D9663A
        a                   bdo:TitlePageTitle ;
        rdfs:label          "sgrung mkhan bsam grub kyis phab pa'i gling rje ge sar rgyal po'i sgrung / lha gling /"@bo-x-ewts .
}
