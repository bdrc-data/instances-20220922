@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW20181 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGDE0D40E27E0083A6
        a                   adm:UpdateData ;
        adm:logDate         "2015-11-05T05:38:45.763000+00:00"^^xsd:dateTime ;
        adm:logMessage      "deleted inProduct PR1COPYRIGHT"@en ;
        adm:logWho          bdu:U00006 .
    
    bda:LGF607669DC126D3AB
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW20181  a          adm:AdminData ;
        adm:adminAbout      bdr:MW20181 ;
        adm:facetIndex      16 ;
        adm:gitPath         "3c/MW20181.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW20181 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LGDE0D40E27E0083A6 , bda:LGF607669DC126D3AB , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV6D069B42E0BC1BB5
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1999"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID031A9E89F88FA919
        a                   bdr:HarvardShelfId ;
        rdf:value           "012362811-3" .
    
    bdr:ID68D2A52F1182F2F8
        a                   bdr:HollisId ;
        rdf:value           "014258079" .
    
    bdr:ID8896584F4551CF77
        a                   bf:Isbn ;
        rdf:value           "7542008595" .
    
    bdr:IDC3E1FAD6ECDC7DEB
        a                   bf:Lccn ;
        rdf:value           "2001291200" .
    
    bdr:IDC8AEE1C1CF2E1341
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3617 .S26 1999" .
    
    bdr:MW20181  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID031A9E89F88FA919 , bdr:ID68D2A52F1182F2F8 , bdr:ID8896584F4551CF77 , bdr:IDC3E1FAD6ECDC7DEB , bdr:IDC8AEE1C1CF2E1341 ;
        bdo:authorshipStatement  "dpa' ris sangs rgyas kyis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par thengs 1"@bo-x-ewts ;
        bdo:extentStatement  "2, 3, 736 p." ;
        bdo:hasTitle        bdr:TT064D5CC6B5C1A39D , bdr:TT0E9E8F640A716A6D , bdr:TT5C0967CA189562FB , bdr:TTFEA771F5F290EAD1 ;
        bdo:instanceEvent   bdr:EV6D069B42E0BC1BB5 ;
        bdo:instanceHasReproduction  bdr:IE20181 , bdr:W20181 ;
        bdo:instanceOf      bdr:WA20181 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "Dag yig rig paʾi gab pa mṅon phyuṅ /Dpaʾ-ris Saṅs-rgyas kyis brtsams."@en-x-mixed , "zang wen zheng zi xue fa yin"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "dag yig rig pa'i gab pa mngon phyung /"@bo-x-ewts .
    
    bdr:TT064D5CC6B5C1A39D
        a                   bdo:CoverTitle ;
        rdfs:label          "Dag yig rig paʾi gab pa mṅon phyuṅ /Dpaʾ-ris Saṅs-rgyas kyis brtsams."@en-x-mixed .
    
    bdr:TT0E9E8F640A716A6D
        a                   bdo:ColophonTitle ;
        rdfs:label          "zang wen zheng zi xue fa yin"@zh-latn-pinyin-x-ndia .
    
    bdr:TT5C0967CA189562FB
        a                   bdo:Title ;
        rdfs:label          "dag yig rig pa'i gab pa mngon phyung /"@bo-x-ewts .
    
    bdr:TTFEA771F5F290EAD1
        a                   bdo:ColophonTitle ;
        rdfs:label          "tsang wen cheng tzu hsueh fa yin"@zh-latn-wadegile .
}
