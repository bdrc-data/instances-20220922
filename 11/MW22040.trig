@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW22040 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG246A64329165B128
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGD76C4DEB90180A07
        a                   adm:UpdateData ;
        adm:logDate         "2015-10-08T04:40:22.218000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated catalog info"@en ;
        adm:logWho          bdu:U00006 .
    
    bda:LGE73CDFC401D848CB
        a                   adm:UpdateData ;
        adm:logDate         "2014-07-18T11:19:52.583000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold and completed cataloging"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW22040  a          adm:AdminData ;
        adm:adminAbout      bdr:MW22040 ;
        adm:facetIndex      14 ;
        adm:gitPath         "11/MW22040.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW22040 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG246A64329165B128 , bda:LGD76C4DEB90180A07 , bda:LGE73CDFC401D848CB , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CRFEEF315E523B94E0
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P5119 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV887F15CB3BFAD489
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1980,1981"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID3D4A1051C56AD5A9
        a                   bdr:HollisId ;
        rdf:value           "014258579" .
    
    bdr:ID70E8A65AEF628502
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3748.G425 D98 1981" .
    
    bdr:IDB8B532EF92159DAF
        a                   bf:Lccn ;
        rdf:value           "85906568" .
    
    bdr:MW22040  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID3D4A1051C56AD5A9 , bdr:ID70E8A65AEF628502 , bdr:IDB8B532EF92159DAF ;
        bdo:authorshipStatement  "dkon mchog tshe brtan gyis bsgrigs/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_TypeSet ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:creator         bdr:CRFEEF315E523B94E0 ;
        bdo:editionStatement  "par thengs gnyis pa/"@bo-x-ewts ;
        bdo:extentStatement  "6, 197 p." ;
        bdo:hasTitle        bdr:TT4CF6B7B3DFC08284 , bdr:TT7F6DB3A656149B43 , bdr:TT7FEDE6A65185DFC8 , bdr:TTF8839870583A5962 ;
        bdo:illustrations   "ill." ;
        bdo:instanceEvent   bdr:EV887F15CB3BFAD489 ;
        bdo:instanceHasReproduction  bdr:W22040 ;
        bdo:instanceOf      bdr:WA1KG16569 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "格萨尔 世界公桑"@zh-hans ;
        skos:prefLabel      "'dzam gling spyi bsang /"@bo-x-ewts .
    
    bdr:TT4CF6B7B3DFC08284
        a                   bdo:ColophonTitle ;
        rdfs:label          "ge sa er shi jie gong sang"@zh-latn-pinyin-x-ndia .
    
    bdr:TT7F6DB3A656149B43
        a                   bdo:TitlePageTitle ;
        rdfs:label          "gling rje ge sar rgyal po'i sgrung 'dzam gling spyi bsang /"@bo-x-ewts .
    
    bdr:TT7FEDE6A65185DFC8
        a                   bdo:Title ;
        rdfs:label          "'dzam gling spyi bsang /"@bo-x-ewts .
    
    bdr:TTF8839870583A5962
        a                   bdo:ColophonTitle ;
        rdfs:label          "格萨尔 世界公桑"@zh-hans .
}
