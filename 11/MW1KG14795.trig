@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG14795 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG50CFB1CDB437B31C
        a                   adm:UpdateData ;
        adm:logDate         "2016-08-20T11:41:50.136000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added Work to CTC 13"@en ;
        adm:logWho          bdu:U00006 .
    
    bda:LG76938E3B234567BD
        a                   adm:UpdateData ;
        adm:logDate         "2018-04-12T10:59:15.209000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added commentary on"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGA1D93D9571C957D5
        a                   adm:InitialDataCreation ;
        adm:logDate         "2013-07-25T15:27:52.155000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG14795  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG14795 ;
        adm:facetIndex      15 ;
        adm:gitPath         "11/MW1KG14795.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG14795 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG50CFB1CDB437B31C , bda:LG76938E3B234567BD , bda:LGA1D93D9571C957D5 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVA2A0DCE6AF53F2CB
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1997"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID5179C008E7EDBED1
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ1460 1997" .
    
    bdr:ID95B19A4C50D73F1A
        a                   bf:Lccn ;
        rdf:value           "98900262" .
    
    bdr:IDCAB26628633E2FB9
        a                   bdr:HollisId ;
        rdf:value           "014256240" .
    
    bdr:IDMW1KG14795_748FWK1D0EO8
        a                   bf:Isbn ;
        rdf:value           "9787542006493" .
    
    bdr:IDMW1KG14795_AENXN7CW734W
        a                   bf:Isbn ;
        rdf:value           "7542006495" .
    
    bdr:MW1KG14795  a       bdo:Instance ;
        bf:identifiedBy     bdr:ID5179C008E7EDBED1 , bdr:ID95B19A4C50D73F1A , bdr:IDCAB26628633E2FB9 , bdr:IDMW1KG14795_748FWK1D0EO8 , bdr:IDMW1KG14795_AENXN7CW734W ;
        bdo:authorshipStatement  "rtsa ba/ slob dpon dpa' bos mdzad/ 'grel pa/ slob dpon chos kyi grags pas mdzad/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2, 455 p." ;
        bdo:hasTitle        bdr:TT52C63951D4375048 , bdr:TT99DD44342C589A60 , bdr:TTC810283575711053 ;
        bdo:instanceEvent   bdr:EVA2A0DCE6AF53F2CB ;
        bdo:instanceHasReproduction  bdr:W1KG14795 ;
        bdo:instanceOf      bdr:WA1KG14795 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "三十四本生传"@zh-hans ;
        skos:prefLabel      "skyes rabs so bzhi ba'i rtsa 'grel/"@bo-x-ewts .
    
    bdr:TT52C63951D4375048
        a                   bdo:Title ;
        rdfs:label          "skyes rabs so bzhi ba'i rtsa 'grel/"@bo-x-ewts .
    
    bdr:TT99DD44342C589A60
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "san shi si ben sheng zhuan"@zh-latn-pinyin-x-ndia .
    
    bdr:TTC810283575711053
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "三十四本生传"@zh-hans .
}
