@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW27307 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG50BAC60293BC3FEF
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG8DBAE818320C69FD
        a                   adm:UpdateData ;
        adm:logDate         "2015-09-28T01:44:34.657000+00:00"^^xsd:dateTime ;
        adm:logMessage      "deleted inProduct PR1COPYRIGHT"@en ;
        adm:logWho          bdu:U00006 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW27307  a          adm:AdminData ;
        adm:adminAbout      bdr:MW27307 ;
        adm:facetIndex      17 ;
        adm:gitPath         "5d/MW27307.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW27307 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG50BAC60293BC3FEF , bda:LG8DBAE818320C69FD , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV8AEDCBDC8F3DC9E5
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1992"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID44FF502FC9D45E89
        a                   bf:ShelfMarkLcc ;
        rdf:value           "DS797.32.M855 N34 1992" .
    
    bdr:ID796A4F7AF95FA93A
        a                   bdr:HollisId ;
        rdf:value           "014259604" .
    
    bdr:ID9534B8AB1520F89C
        a                   bdr:HarvardShelfId ;
        rdf:value           "008109258-X" .
    
    bdr:IDBCFEAA76E4D3BC47
        a                   bf:Lccn ;
        rdf:value           "98900910" .
    
    bdr:IDMW27307_AMZRZ76WKQ82
        a                   bf:Isbn ;
        rdf:value           "754090884X" .
    
    bdr:MW27307  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID44FF502FC9D45E89 , bdr:ID796A4F7AF95FA93A , bdr:ID9534B8AB1520F89C , bdr:IDBCFEAA76E4D3BC47 , bdr:IDMW27307_AMZRZ76WKQ82 ;
        bdo:authorshipStatement  "ngag dbang mkhyen rab kyis brtsams/"@bo-x-ewts ;
        bdo:biblioNote      "in chinese and tibetan; history 1580-1735"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:extentStatement  "173, 58 p." ;
        bdo:hasTitle        bdr:TT2302F4888AC0CFA3 , bdr:TT377A4FCA0209915B , bdr:TT651ECE1555BF1EB5 , bdr:TT9E4BCE42F588C15D ;
        bdo:instanceEvent   bdr:EV8AEDCBDC8F3DC9E5 ;
        bdo:instanceHasReproduction  bdr:W27307 ;
        bdo:instanceOf      bdr:WA27307 ;
        bdo:note            bdr:NT71EFFBCA094D8F4A ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "Mu-li chos ʾbyuṅ =Mu-li cheng chiao shih /Ṅag-dbaṅ-mkhyen-rab kyis brtsams."@en-x-mixed , "mu li cheng chiao shih"@zh-latn-wadegile ;
        skos:prefLabel      "mu li chos 'byung /"@bo-x-ewts .
    
    bdr:NT71EFFBCA094D8F4A
        a                   bdo:Note ;
        bdo:noteText        "tbrc holds digital preservation scans; not to be distributed\n4269"@en .
    
    bdr:TT2302F4888AC0CFA3
        a                   bdo:OtherTitle ;
        rdfs:label          "muli zheng jiao shi"@zh-latn-wadegile .
    
    bdr:TT377A4FCA0209915B
        a                   bdo:CoverTitle ;
        rdfs:label          "Mu-li chos ʾbyuṅ =Mu-li cheng chiao shih /Ṅag-dbaṅ-mkhyen-rab kyis brtsams."@en-x-mixed .
    
    bdr:TT651ECE1555BF1EB5
        a                   bdo:Title ;
        rdfs:label          "mu li chos 'byung /"@bo-x-ewts .
    
    bdr:TT9E4BCE42F588C15D
        a                   bdo:OtherTitle ;
        rdfs:label          "mu li cheng chiao shih"@zh-latn-wadegile .
}
