@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21700 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG1FC346DD2BC81982
        a                   adm:UpdateData ;
        adm:logDate         "2015-08-01T03:50:37.236000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated catalog info"@en ;
        adm:logWho          bdu:U00006 .
    
    bda:LG4C0356F6EEBAA403
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG995A509141B49E6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-08-01T03:51:32.362000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated catalog info"@en ;
        adm:logWho          bdu:U00006 .
    
    bda:LGD8C91A8660550BCF
        a                   adm:UpdateData ;
        adm:logDate         "2014-02-03T14:52:26.916000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGE73B327CA945D9BB
        a                   adm:UpdateData ;
        adm:logDate         "2015-10-21T05:02:20.940000+00:00"^^xsd:dateTime ;
        adm:logMessage      "updated catalog info"@en ;
        adm:logWho          bdu:U00006 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21700  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21700 ;
        adm:facetIndex      16 ;
        adm:gitPath         "5d/MW21700.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21700 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG1FC346DD2BC81982 , bda:LG4C0356F6EEBAA403 , bda:LG995A509141B49E6C , bda:LGD8C91A8660550BCF , bda:LGE73B327CA945D9BB , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV398E3E082461C9C3
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2001"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID041F9E2CC3693841
        a                   bdr:HollisId ;
        rdf:value           "014258395" .
    
    bdr:IDMW21700_2PNMVEBSF8VT
        a                   bf:Isbn ;
        rdf:value           "7105034343" .
    
    bdr:IDMW21700_Y458WVUCB7RX
        a                   bf:Isbn ;
        rdf:value           "9787105034345" .
    
    bdr:MW21700  a          bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID041F9E2CC3693841 , bdr:IDMW21700_2PNMVEBSF8VT , bdr:IDMW21700_Y458WVUCB7RX ;
        bdo:authorshipStatement  "bskal bzang ngag dbang dam chos kyis brtsams/ krung go bod kyi mtho rim nang bstan slob gling bod brgyud nang bstan zhib 'jug khang nas bsgrigs/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "3, 652 p." ;
        bdo:hasTitle        bdr:TT4C397B012A0965D1 , bdr:TTEBDDEA08CBB88637 , bdr:TTF3972AC8161EDF4F , bdr:TTFFE7C60EDF12A052 ;
        bdo:instanceEvent   bdr:EV398E3E082461C9C3 ;
        bdo:instanceHasReproduction  bdr:W21700 ;
        bdo:instanceOf      bdr:WA21700 ;
        bdo:note            bdr:NT6D63F7DAEDA1DACC ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG11702 ;
        bdo:seriesNumber    "deb 29" ;
        skos:altLabel       "妙音声明学广注"@zh-hans ;
        skos:prefLabel      "dbyangs can sgra mdo'i rgyas 'grel/"@bo-x-ewts .
    
    bdr:NT6D63F7DAEDA1DACC
        a                   bdo:Note ;
        bdo:noteText        "in tibetan"@en .
    
    bdr:TT4C397B012A0965D1
        a                   bdo:CoverTitle ;
        rdfs:label          "gangs can rig brgya'i sgo 'byed lde mig (deb nyer dgu pa/)"@bo-x-ewts .
    
    bdr:TTEBDDEA08CBB88637
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "miao yin sheng ming xue guang zhu"@zh-latn-pinyin-x-ndia .
    
    bdr:TTF3972AC8161EDF4F
        a                   bdo:Title ;
        rdfs:label          "dbyangs can sgra mdo'i rgyas 'grel/"@bo-x-ewts .
    
    bdr:TTFFE7C60EDF12A052
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "妙音声明学广注"@zh-hans .
}
