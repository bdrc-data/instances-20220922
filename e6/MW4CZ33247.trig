@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW4CZ33247 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG577B88F80AE8D96B
        a                   adm:InitialDataCreation ;
        adm:logDate         "2014-01-28T12:11:04.792000+00:00"^^xsd:dateTime ;
        adm:logMessage      "published,"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:LG5F9FF286DD3CE529
        a                   adm:UpdateData ;
        adm:logDate         "2014-01-28T13:17:54.682000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW4CZ33247  a       adm:AdminData ;
        adm:adminAbout      bdr:MW4CZ33247 ;
        adm:facetIndex      15 ;
        adm:gitPath         "e6/MW4CZ33247.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW4CZ33247 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG577B88F80AE8D96B , bda:LG5F9FF286DD3CE529 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CR6D5196457ED263AF
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P4CZ33248 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV4DB2F86DADABAACE
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2011"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID84BD5863B5310F1A
        a                   bdr:HollisId ;
        rdf:value           "014260951" .
    
    bdr:ID9D13E95624A957A4
        a                   bf:Isbn ;
        rdf:value           "9783882800951" .
    
    bdr:IDEDD976A4263D3697
        a                   bf:Lccn ;
        rdf:value           "no call number available" .
    
    bdr:MW4CZ33247  a       bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID84BD5863B5310F1A , bdr:ID9D13E95624A957A4 , bdr:IDEDD976A4263D3697 ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:creator         bdr:CR6D5196457ED263AF ;
        bdo:extentStatement  "extendedWylie" ;
        bdo:hasTitle        bdr:TT9ECE85D5DAAD484E , bdr:TTEF13757FBA1A395D ;
        bdo:illustrations   "(some colored), maps ; 24 cm." ;
        bdo:instanceEvent   bdr:EV4DB2F86DADABAACE ;
        bdo:instanceHasReproduction  bdr:W4CZ33247 ;
        bdo:instanceOf      bdr:WA4CZ33247 ;
        bdo:note            bdr:NT109CFFD03AAF99E4 , bdr:NT4BFD332FA4977941 , bdr:NTA4F9FF96B8CFB19E ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "andiast, switzerland"@en ;
        bdo:publisherName   "international institute for tibetan and buddhist studies gmbh"@en ;
        bdo:serialInstanceOf  bdr:WAS4CZ33249 ;
        bdo:seriesNumber    ": bd 24" ;
        skos:prefLabel      "mapping the modern in tibet"@en .
    
    bdr:NT109CFFD03AAF99E4
        a                   bdo:Note ;
        bdo:noteText        "piais 2006: tibetan studies: proceedings of the eeventh seminar of the international association for tibetan studies, königswinter 2006. includes bibliographical references."@en .
    
    bdr:NT4BFD332FA4977941
        a                   bdo:Note ;
        bdo:noteText        "in english"@en .
    
    bdr:NTA4F9FF96B8CFB19E
        a                   bdo:Note ;
        bdo:noteText        "With gratitude and apppreciation, this volume is dedicated to the memory of Gene Smith"@en .
    
    bdr:TT9ECE85D5DAAD484E
        a                   bdo:SpineTitle ;
        rdfs:label          "gray tuttle: mapping the modern in tibet"@en .
    
    bdr:TTEF13757FBA1A395D
        a                   bdo:TitlePageTitle ;
        rdfs:label          "mapping the modern in tibet"@en .
}
