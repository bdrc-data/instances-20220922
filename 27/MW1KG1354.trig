@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG1354 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG27DE097308F6A1CD
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-02T15:02:33.796000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added lccn 2001436912"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LG3BEFB09C10BCCDB0
        a                   adm:UpdateData ;
        adm:logDate         "2021-07-22T13:02:47.040000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type and fixed lang encoding and typo"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG4F006375B4D756E5
        a                   adm:InitialDataCreation ;
        adm:logDate         "2009-01-14T15:04:00.650000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged and released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG73966081CB19B1E7
        a                   adm:UpdateData ;
        adm:logDate         "2009-03-09T09:48:55.597000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00016 .
    
    bda:LGF48C26484D68806F
        a                   adm:UpdateData ;
        adm:logDate         "2009-03-06T17:05:20.928000+00:00"^^xsd:dateTime ;
        adm:logMessage      "qc"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG1354  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1KG1354 ;
        adm:facetIndex      16 ;
        adm:gitPath         "27/MW1KG1354.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG1354 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG27DE097308F6A1CD , bda:LG3BEFB09C10BCCDB0 , bda:LG4F006375B4D756E5 , bda:LG73966081CB19B1E7 , bda:LGF48C26484D68806F , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:CR0568A566F7B816CB
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P1KG1355 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV1577EEDAC22643AB
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2001"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID460D3CB5D0D0701B
        a                   bdr:HarvardShelfId ;
        rdf:value           "009372352-0" .
    
    bdr:IDC3C52DD25A19D3CE
        a                   bf:Lccn ;
        rdf:value           "2001436912" .
    
    bdr:IDE77E4DEB64829D21
        a                   bdr:HollisId ;
        rdf:value           "014256033" .
    
    bdr:MW1KG1354  a        bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID460D3CB5D0D0701B , bdr:IDC3C52DD25A19D3CE , bdr:IDE77E4DEB64829D21 ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:creator         bdr:CR0568A566F7B816CB ;
        bdo:editionStatement  "1st ed."@en ;
        bdo:extentStatement  "xiv, 227 p." ;
        bdo:hasTitle        bdr:TT21E94BEA05D35AD6 , bdr:TTC5BAD8B8F3895E1B ;
        bdo:illustrations   "ill." ;
        bdo:instanceEvent   bdr:EV1577EEDAC22643AB ;
        bdo:instanceHasReproduction  bdr:W1KG1354 ;
        bdo:instanceOf      bdr:WA1KG1354 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "dharamsala, h.p."@en ;
        bdo:publisherName   "bod kyi dgu bcu gsum las 'gul tshogs pa/"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG1350 ;
        bdo:seriesNumber    "3" ;
        skos:altLabel       "Khrag daṅ mig chuʾi rgyun ma chad paʾi Grwa-bźi btson khaṅ."@en-x-mixed ;
        skos:prefLabel      "khrag dang mig chu'i rgyun ma chad pa'i grwa bzhi btson khang /"@bo-x-ewts .
    
    bdr:TT21E94BEA05D35AD6
        a                   bdo:Title ;
        rdfs:label          "khrag dang mig chu'i rgyun ma chad pa'i grwa bzhi btson khang /"@bo-x-ewts .
    
    bdr:TTC5BAD8B8F3895E1B
        a                   bdo:CoverTitle ;
        rdfs:label          "Khrag daṅ mig chuʾi rgyun ma chad paʾi Grwa-bźi btson khaṅ."@en-x-mixed .
}
