@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW19970 {
    bda:LG04D463C3952384E7
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGF673D80345DC34EE
        a                   adm:UpdateData ;
        adm:logDate         "2015-03-30T16:48:28.233000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:MW19970  a          adm:AdminData ;
        adm:adminAbout      bdr:MW19970 ;
        adm:facetIndex      12 ;
        adm:gitPath         "88/MW19970.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW19970 ;
        adm:logEntry        bda:LG04D463C3952384E7 , bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LGF673D80345DC34EE ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVBA07461D92B4BD78
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1994,1995"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID0BAE09AB5D6B6EB6
        a                   bf:ShelfMarkLcc ;
        rdf:value           "RA776.95 .T74 1994" .
    
    bdr:ID59862CDF3E5761B5
        a                   bf:Lccn ;
        rdf:value           "97916088" .
    
    bdr:IDMW19970_9XNOPO3SU5YM
        a                   bf:Isbn ;
        rdf:value           "7542102109" .
    
    bdr:IDMW19970_ZLIFZGZIJVL6
        a                   bf:Isbn ;
        rdf:value           "9787542102102" .
    
    bdr:MW19970  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID0BAE09AB5D6B6EB6 , bdr:ID59862CDF3E5761B5 , bdr:IDMW19970_9XNOPO3SU5YM , bdr:IDMW19970_ZLIFZGZIJVL6 ;
        bdo:authorshipStatement  "tshul khrims rgya mtsho/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "3, 256 p." ;
        bdo:hasTitle        bdr:TT1BD39506251C7F2C , bdr:TT20161880DB93581E , bdr:TT2F73C17750445605 , bdr:TT662A02AC1B83B092 ;
        bdo:instanceEvent   bdr:EVBA07461D92B4BD78 ;
        bdo:instanceHasReproduction  bdr:W19970 ;
        bdo:instanceOf      bdr:WA19970 ;
        bdo:note            bdr:NTF83DBAFDCDA67165 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "常见病治疗手册"@zh-hans ;
        skos:prefLabel      "rgyun mthong nad rigs gso thabs/"@bo-x-ewts .
    
    bdr:NTF83DBAFDCDA67165
        a                   bdo:Note ;
        bdo:noteText        "copy made available from the library of tibetan works and archives; ltwa acc. no. 5717"@en .
    
    bdr:TT1BD39506251C7F2C
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "chang jian bing zhi liao shou ce"@zh-latn-pinyin-x-ndia .
    
    bdr:TT20161880DB93581E
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "常见病治疗手册"@zh-hans .
    
    bdr:TT2F73C17750445605
        a                   bdo:TitlePageTitle ;
        rdfs:label          "rgyun mthong nad rigs gso thabs bdud rtsi'i gzegs ma/"@bo-x-ewts .
    
    bdr:TT662A02AC1B83B092
        a                   bdo:Title ;
        rdfs:label          "rgyun mthong nad rigs gso thabs/"@bo-x-ewts .
}
