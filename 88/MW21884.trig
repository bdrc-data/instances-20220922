@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21884 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG333F8C8886C01823
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG4BEA6FE263B97FF9
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-31T16:16:22.755000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGA4FFF4A41D29C7EB
        a                   adm:UpdateData ;
        adm:logDate         "2015-03-23T14:31:40.588000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGD0AA52D386F0070A
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-08T23:22:19.539000+00:00"^^xsd:dateTime ;
        adm:logMessage      "changed encoding value extendedWylie to In Tibetan"@en ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21884  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21884 ;
        adm:facetIndex      14 ;
        adm:gitPath         "88/MW21884.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21884 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG333F8C8886C01823 , bda:LG4BEA6FE263B97FF9 , bda:LGA4FFF4A41D29C7EB , bda:LGD0AA52D386F0070A , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV0DC4D725252234BA
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1997"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID4A3B11E9EE8A3292
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3611 .B725 1997" .
    
    bdr:ID762BEC526E93F1E5
        a                   bf:Lccn ;
        rdf:value           "98904633" .
    
    bdr:IDF6F7EC04FBEAF5A3
        a                   bdr:HollisId ;
        rdf:value           "014258484" .
    
    bdr:IDMW21884_25A8GEUNFJSP
        a                   bf:Isbn ;
        rdf:value           "7105027460" .
    
    bdr:IDMW21884_FE17VGY4GIYV
        a                   bf:Isbn ;
        rdf:value           "9787105027460" .
    
    bdr:MW21884  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID4A3B11E9EE8A3292 , bdr:ID762BEC526E93F1E5 , bdr:IDF6F7EC04FBEAF5A3 , bdr:IDMW21884_25A8GEUNFJSP , bdr:IDMW21884_FE17VGY4GIYV ;
        bdo:authorshipStatement  "bu phrug dang gnas brtan gnyis kyis rtsom sgrig byas/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "5, 212 p." ;
        bdo:hasTitle        bdr:TT5D08910BFAA252AD , bdr:TT7826444E5EFD4095 , bdr:TTEFDC83949C2F1369 ;
        bdo:instanceEvent   bdr:EV0DC4D725252234BA ;
        bdo:instanceHasReproduction  bdr:W21884 ;
        bdo:instanceOf      bdr:WA21884 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "藏语文文法教材"@zh-hans ;
        skos:prefLabel      "'bru 'grel 'phrul lde'i gsal 'grel thu mi'i dgongs gnad gsal ba'i me long /"@bo-x-ewts .
    
    bdr:TT5D08910BFAA252AD
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "藏语文文法教材"@zh-hans .
    
    bdr:TT7826444E5EFD4095
        a                   bdo:Title ;
        rdfs:label          "'bru 'grel 'phrul lde'i gsal 'grel thu mi'i dgongs gnad gsal ba'i me long /"@bo-x-ewts .
    
    bdr:TTEFDC83949C2F1369
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "zang yu wen wen fa jiao cai"@zh-latn-pinyin-x-ndia .
}
