@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW20013 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGFDA0FD5CED426E1B
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW20013  a          adm:AdminData ;
        adm:adminAbout      bdr:MW20013 ;
        adm:facetIndex      12 ;
        adm:gitPath         "c1/MW20013.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW20013 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LGFDA0FD5CED426E1B , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV70B2346888F6DE86
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1995"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID2629DBFA1C75558A
        a                   bf:ShelfMarkLcc ;
        rdf:value           "R123 .G86 1995" .
    
    bdr:IDA16CB6F14C2BE157
        a                   bf:Lccn ;
        rdf:value           "97915585" .
    
    bdr:IDC3B843B867E14711
        a                   bdr:HollisId ;
        rdf:value           "014258000" .
    
    bdr:IDMW20013_JMWT579HV311
        a                   bf:Isbn ;
        rdf:value           "9787542005311" .
    
    bdr:IDMW20013_XG5B62HWIX38
        a                   bf:Isbn ;
        rdf:value           "7542005316" .
    
    bdr:MW20013  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID2629DBFA1C75558A , bdr:IDA16CB6F14C2BE157 , bdr:IDC3B843B867E14711 , bdr:IDMW20013_JMWT579HV311 , bdr:IDMW20013_XG5B62HWIX38 ;
        bdo:authorshipStatement  "rdo rjes bsgrigs/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "3, 253 p." ;
        bdo:hasTitle        bdr:TT2FAE8F759CA49316 , bdr:TT41C325AED7C3FA1F , bdr:TT9449E4001F957A4C ;
        bdo:instanceEvent   bdr:EV70B2346888F6DE86 ;
        bdo:instanceHasReproduction  bdr:W20013 ;
        bdo:instanceOf      bdr:WA20013 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "zang yi xue zong chen shi yi"@zh-latn-wadegile ;
        skos:prefLabel      "gso rig shes bya'i rnam grangs/"@bo-x-ewts .
    
    bdr:TT2FAE8F759CA49316
        a                   bdo:Title ;
        rdfs:label          "gso rig shes bya'i rnam grangs/"@bo-x-ewts .
    
    bdr:TT41C325AED7C3FA1F
        a                   bdo:ColophonTitle ;
        rdfs:label          "zang yi xue zong chen shi yi"@zh-latn-wadegile .
    
    bdr:TT9449E4001F957A4C
        a                   bdo:TitlePageTitle ;
        rdfs:label          "gso rig rgyud bzhi las 'phros pa'i shes bya'i rnam grangs 'tsho byed dgyes pa'i dga' ston/"@bo-x-ewts .
}
