@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG1428 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG3E597C781A244A09
        a                   adm:UpdateData ;
        adm:logDate         "2021-07-22T12:25:45.296000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type and corrected lang encodings"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGB3FDEBE3BD8EEF6C
        a                   adm:InitialDataCreation ;
        adm:logDate         "2009-01-16T16:27:24.516000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged and released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGD122BF7FEB0E9AB8
        a                   adm:UpdateData ;
        adm:logDate         "2009-03-09T16:30:09.032000+00:00"^^xsd:dateTime ;
        adm:logMessage      "qc"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGFE8C31A5AEC0866E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-08T10:10:15.328000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added lccn 2008308199"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG1428  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1KG1428 ;
        adm:facetIndex      15 ;
        adm:gitPath         "c1/MW1KG1428.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG1428 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LG3E597C781A244A09 , bda:LGB3FDEBE3BD8EEF6C , bda:LGD122BF7FEB0E9AB8 , bda:LGFE8C31A5AEC0866E , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:CRD43908F39A9FC70C
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P1KG1430 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV4BEC238D26378C9B
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2007"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID176ECC1DC737394E
        a                   bdr:HarvardShelfId ;
        rdf:value           "011981715-2" .
    
    bdr:ID4535278DC7DFC71A
        a                   bdr:HollisId ;
        rdf:value           "014256114" .
    
    bdr:IDE0AFFB59FD156F60
        a                   bf:Lccn ;
        rdf:value           "2008308199" .
    
    bdr:MW1KG1428  a        bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID176ECC1DC737394E , bdr:ID4535278DC7DFC71A , bdr:IDE0AFFB59FD156F60 ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:creator         bdr:CRD43908F39A9FC70C ;
        bdo:editionStatement  "1st ed."@en ;
        bdo:extentStatement  "xiii, 106 p." ;
        bdo:hasTitle        bdr:TT63789BABC963019E , bdr:TTB62AC1D0E18DC408 ;
        bdo:illustrations   "ill." ;
        bdo:instanceEvent   bdr:EV4BEC238D26378C9B ;
        bdo:instanceHasReproduction  bdr:IE1KG1428 , bdr:W1KG1428 ;
        bdo:instanceOf      bdr:WA1KG1428 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "dharamsala, h.p."@en ;
        bdo:publisherName   "bod kyi dgu bcu gsum las 'gul tshogs pa/"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG1350 ;
        bdo:seriesNumber    "17" ;
        skos:altLabel       "Char rluṅ khrod kyi me stag /[A-ni Dpal-chen-sgrol-ma] ; rtsom sgrig pa, Zla-ba-bkra-śis."@en-x-mixed ;
        skos:prefLabel      "char rlung khrod kyi me stag"@bo-x-ewts .
    
    bdr:TT63789BABC963019E
        a                   bdo:CoverTitle ;
        rdfs:label          "Char rluṅ khrod kyi me stag /[A-ni Dpal-chen-sgrol-ma] ; rtsom sgrig pa, Zla-ba-bkra-śis."@en-x-mixed .
    
    bdr:TTB62AC1D0E18DC408
        a                   bdo:Title ;
        rdfs:label          "char rlung khrod kyi me stag"@bo-x-ewts .
}
