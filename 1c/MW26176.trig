@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW26176 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0MW26176_TY5UFQOUIVX6
        a                   adm:UpdateData ;
        adm:logDate         "2024-03-01T17:53:14.555171Z"^^xsd:dateTime ;
        adm:logMessage      "corrected series name"@en ;
        adm:logWho          bdu:U1426146149 .
    
    bda:LG7AB1724657695296
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG80BE4635200E2FF3
        a                   adm:UpdateData ;
        adm:logDate         "2022-06-15T11:55:31.053000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added subject, cat info, series"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW26176  a          adm:AdminData ;
        adm:adminAbout      bdr:MW26176 ;
        adm:facetIndex      17 ;
        adm:gitPath         "1c/MW26176.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW26176 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0MW26176_TY5UFQOUIVX6 , bda:LG7AB1724657695296 , bda:LG80BE4635200E2FF3 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV309D1D2AD04AB826
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2003"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID42B7127C23684C8E
        a                   bdr:HollisId ;
        rdf:value           "014259518" .
    
    bdr:IDCD62A01E6CEB1429
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7672.4 .B63 2003 V. 13 BQ2910.M365" .
    
    bdr:IDF693B1697BBB78B7
        a                   bf:Lccn ;
        rdf:value           "2005323052" .
    
    bdr:IDMW26176_QMX9PWS098WD
        a                   bf:Isbn ;
        rdf:value           "9787105055418" .
    
    bdr:IDMW26176_XHG1Q5U5UE12
        a                   bf:Isbn ;
        rdf:value           "7105055413" .
    
    bdr:MW26176  a          bdo:Instance ;
        skos:altLabel       "zhongguan"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "dbu ma/"@bo-x-ewts ;
        bf:identifiedBy     bdr:ID42B7127C23684C8E , bdr:IDCD62A01E6CEB1429 , bdr:IDF693B1697BBB78B7 , bdr:IDMW26176_QMX9PWS098WD , bdr:IDMW26176_XHG1Q5U5UE12 ;
        bdo:authorshipStatement  "red mda' ba gzhon nu blo gros dang kun mkhyen go ram bsod nams seng ges mdzad/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "5, 14, 3, 516 p." ;
        bdo:hasTitle        bdr:TT3FA9CCA649BF188D , bdr:TT4BC7AB90E09853E1 , bdr:TTAC395C75EBAA609C , bdr:TTB71D841C2DB4A48D , bdr:TTCB56176D617F45CC ;
        bdo:instanceEvent   bdr:EV309D1D2AD04AB826 ;
        bdo:instanceHasReproduction  bdr:W26176 ;
        bdo:instanceOf      bdr:WA26176 ;
        bdo:material        bdr:MaterialPaper ;
        bdo:note            bdr:NT5B42BAF302FF8867 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/ zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang / mtsho sngon mi rigs dpe skurn khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptDbuCan ;
        bdo:serialInstanceOf  bdr:WAS3KG38 ;
        bdo:seriesNumber    "v. 13" .
    
    bdr:NT5B42BAF302FF8867
        a                   bdo:Note ;
        bdo:noteText        "Contents: \n1. dbu ma la 'jug pa'i rnam bshad de kho na nyid gsal ba'i sgron ma / red mda' ba gzhon nu blo gros. \n2. dbu ma la 'jug pa'i dkyus kyi sa bcad pa dang gzhung so so'i dka' ba'i gnas la dpyad pa lta ba ngan sel / go rams pa bsod nams seng+ge"@en .
    
    bdr:TT3FA9CCA649BF188D
        a                   bdo:CoverTitle ;
        rdfs:label          "bod kyi bcu phrag rig mdzod chen mo las sa skya pa'i gsung rab/ dbu ma/"@bo-x-ewts .
    
    bdr:TT4BC7AB90E09853E1
        a                   bdo:OtherTitle ;
        rdfs:label          "dbu ma la 'jug pa'i rnam bshad de kho na nyid gsal ba'i sgron me/"@bo-x-ewts .
    
    bdr:TTAC395C75EBAA609C
        a                   bdo:ColophonTitle ;
        rdfs:label          "zhongguan"@zh-latn-pinyin-x-ndia .
    
    bdr:TTB71D841C2DB4A48D
        a                   bdo:BibliographicalTitle ;
        rdfs:label          "dbu ma/"@bo-x-ewts .
    
    bdr:TTCB56176D617F45CC
        a                   bdo:OtherTitle ;
        rdfs:label          "dbu ma la 'jug pa'i dkyus kyi sa bcad pa lta ba ngan sel/"@bo-x-ewts .
}
