@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1GS44028 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG9B4A0BC6EC4F9136
        a                   adm:UpdateData ;
        adm:logDate         "2007-05-08T18:20:02.068000+00:00"^^xsd:dateTime ;
        adm:logMessage      "first published"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGA35DF5A1CAC66E4B
        a                   adm:UpdateData ;
        adm:logDate         "2021-04-12T15:57:37.073000+00:00"^^xsd:dateTime ;
        adm:logMessage      "Reviewed Catalog , Language encoding, and print type"@en ;
        adm:logWho          bdu:U00017 .
    
    bda:LGB752F12D712DA9D2
        a                   adm:UpdateData ;
        adm:logDate         "2007-05-09T09:48:42.055000+00:00"^^xsd:dateTime ;
        adm:logMessage      "linked to series"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGC494C7E03370DB80
        a                   adm:UpdateData ;
        adm:logDate         "2007-05-08T16:14:58.781000+00:00"^^xsd:dateTime ;
        adm:logMessage      "work published"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGCDCC89D12B1D0F92
        a                   adm:InitialDataCreation ;
        adm:logDate         "2007-05-08T16:09:06.784000+00:00"^^xsd:dateTime ;
        adm:logMessage      "checking on publication"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1GS44028  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1GS44028 ;
        adm:facetIndex      13 ;
        adm:gitPath         "fc/MW1GS44028.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1GS44028 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG9B4A0BC6EC4F9136 , bda:LGA35DF5A1CAC66E4B , bda:LGB752F12D712DA9D2 , bda:LGC494C7E03370DB80 , bda:LGCDCC89D12B1D0F92 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EVAD8C481AF2A49FB9
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1996"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:IDE5A1E7E516F167DD
        a                   bdr:HollisId ;
        rdf:value           "014255454" .
    
    bdr:IDMW1GS44028_P1KGHVCMVWO5
        a                   bf:Isbn ;
        rdf:value           "7105025964" .
    
    bdr:IDMW1GS44028_VCOTL688KFKX
        a                   bf:Isbn ;
        rdf:value           "9787105025961" .
    
    bdr:MW1GS44028  a       bdo:SerialInstance ;
        bf:identifiedBy     bdr:IDE5A1E7E516F167DD , bdr:IDMW1GS44028_P1KGHVCMVWO5 , bdr:IDMW1GS44028_VCOTL688KFKX ;
        bdo:authorshipStatement  "bod rang ljongs srid gros lo rgyus rig gnas dpyad gzhi'i rgyu cha u yon lhan khang gis rtsom sgrig byas pa/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightUndetermined ;
        bdo:extentStatement  "3, 265 p." ;
        bdo:hasTitle        bdr:TT4647A2BDE4B7D891 , bdr:TTA3DDE241E31A475E ;
        bdo:instanceEvent   bdr:EVAD8C481AF2A49FB9 ;
        bdo:instanceHasReproduction  bdr:IE1GS44028 , bdr:W1GS44028 ;
        bdo:instanceOf      bdr:WA1GS44028 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS19813 ;
        bdo:seriesNumber    "19" ;
        skos:prefLabel      "bod kyi rig gnas lo rgyus dpyad gzhi'i rgyu cha bdams bsgrigs : 10 (19)"@bo-x-ewts .
    
    bdr:TT4647A2BDE4B7D891
        a                   bdo:OtherTitle ;
        rdfs:label          "bod kyi lo rgyus rig gnas dpyad gzhi'i rgyu cha bdams bsgrigs : 10 (19)"@bo-x-ewts .
    
    bdr:TTA3DDE241E31A475E
        a                   bdo:Title ;
        rdfs:label          "bod kyi rig gnas lo rgyus dpyad gzhi'i rgyu cha bdams bsgrigs : 10 (19)"@bo-x-ewts .
}
