@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG4286 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG21E5A77BF59F3145
        a                   adm:UpdateData ;
        adm:logDate         "2016-04-10T07:55:46.381000+00:00"^^xsd:dateTime ;
        adm:logMessage      "deleted inProduct PR1COPYRIGHT"@en ;
        adm:logWho          bdu:U00006 .
    
    bda:LG4A1A8949C4F1AB71
        a                   adm:UpdateData ;
        adm:logDate         "2010-03-10T16:58:17.579000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00016 .
    
    bda:LG52288E5876DB1289
        a                   adm:UpdateData ;
        adm:logDate         "2010-07-28T16:55:00.225000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00020 .
    
    bda:LGC507A20FF50DB461
        a                   adm:UpdateData ;
        adm:logDate         "2021-03-15T15:56:53.393000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added subject"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGF62FECA5C0228436
        a                   adm:InitialDataCreation ;
        adm:logDate         "2010-03-10T12:18:25.166000+00:00"^^xsd:dateTime ;
        adm:logMessage      "first released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG4286  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1KG4286 ;
        adm:facetIndex      17 ;
        adm:gitPath         "a9/MW1KG4286.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG4286 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG21E5A77BF59F3145 , bda:LG4A1A8949C4F1AB71 , bda:LG52288E5876DB1289 , bda:LGC507A20FF50DB461 , bda:LGF62FECA5C0228436 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV44CF63CE43B0B2E2
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2009"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID05201C75CF58A819
        a                   bf:Isbn ;
        rdf:value           "9787540942007" .
    
    bdr:ID892072335BBD0071
        a                   bdr:HollisId ;
        rdf:value           "014256890" .
    
    bdr:IDD169CB492FA5B47A
        a                   bdr:HarvardShelfId ;
        rdf:value           "013107702-3" .
    
    bdr:MW1KG4286  a        bdo:Instance ;
        bf:identifiedBy     bdr:ID05201C75CF58A819 , bdr:ID892072335BBD0071 , bdr:IDD169CB492FA5B47A ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/ par thengs dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2 v." ;
        bdo:hasTitle        bdr:TT0B991DEC858AB0DD , bdr:TT3D79753BFE29B0FD , bdr:TT52BD7AAC1B3B43EB , bdr:TTE013367E35F8DAAF ;
        bdo:instanceEvent   bdr:EV44CF63CE43B0B2E2 ;
        bdo:instanceHasReproduction  bdr:W1KG4286 ;
        bdo:instanceOf      bdr:WA1KG4286 ;
        bdo:numberOfVolumes  2 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "*khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron dpe skrun tshogs pa/ si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "Mkhas dbaṅ Dge-ʼdun-chos-ʼphel gyi gsuṅ ʼbum bźugs so."@en-x-mixed , "更敦群培文集"@zh-hans ;
        skos:prefLabel      "gsung 'bum/_dge 'dun chos 'phel/"@bo-x-ewts .
    
    bdr:TT0B991DEC858AB0DD
        a                   bdo:OtherTitle ;
        rdfs:label          "更敦群培文集"@zh-hans .
    
    bdr:TT3D79753BFE29B0FD
        a                   bdo:Title ;
        rdfs:label          "gsung 'bum/_dge 'dun chos 'phel/"@bo-x-ewts .
    
    bdr:TT52BD7AAC1B3B43EB
        a                   bdo:TitlePageTitle ;
        rdfs:label          "mkhas dbang dge 'dun chos 'phel gyi gsung 'bum/"@bo-x-ewts .
    
    bdr:TTE013367E35F8DAAF
        a                   bdo:CoverTitle ;
        rdfs:label          "Mkhas dbaṅ Dge-ʼdun-chos-ʼphel gyi gsuṅ ʼbum bźugs so."@en-x-mixed .
}
