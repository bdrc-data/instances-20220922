@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW29147 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG294EB22395CFE4DE
        a                   adm:UpdateData ;
        adm:logDate         "2011-12-20T14:55:43.562000+00:00"^^xsd:dateTime ;
        adm:logMessage      "changed author and added copyrightpagetitle"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG838F56772597B0D9
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW29147  a          adm:AdminData ;
        adm:adminAbout      bdr:MW29147 ;
        adm:facetIndex      12 ;
        adm:gitPath         "c9/MW29147.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW29147 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG294EB22395CFE4DE , bda:LG838F56772597B0D9 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVCF64CB57FB4D98EE
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2003"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:IDC1C4B70CC6F834BF
        a                   bdr:HollisId ;
        rdf:value           "014259915" .
    
    bdr:IDMW29147_ICQTJQWR8139
        a                   bf:Isbn ;
        rdf:value           "7223016043" .
    
    bdr:IDMW29147_YWDWM4DCBT5E
        a                   bf:Isbn ;
        rdf:value           "9787223016049" .
    
    bdr:MW29147  a          bdo:Instance ;
        bf:identifiedBy     bdr:IDC1C4B70CC6F834BF , bdr:IDMW29147_ICQTJQWR8139 , bdr:IDMW29147_YWDWM4DCBT5E ;
        bdo:authorshipStatement  "ra se dkon mchog rgya mtshos brtsams/"@bo-x-ewts ;
        bdo:biblioNote      "printed from computerized input"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "146 p." ;
        bdo:hasTitle        bdr:TT0F4BBCF04FC0DC3C , bdr:TT2FE65F0FB0739DF6 , bdr:TT8707810A30534F3D , bdr:TTC9CB178A0A99B7F2 ;
        bdo:instanceEvent   bdr:EVCF64CB57FB4D98EE ;
        bdo:instanceHasReproduction  bdr:W29147 ;
        bdo:instanceOf      bdr:WA29147 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "lha sa/"@bo-x-ewts ;
        bdo:publisherName   "lha sa bod ljongs mi dmangs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "觉巴仁钦拜传"@zh-hans ;
        skos:prefLabel      "'bri gung skyob pa 'jig rten gsum mgon gyi rnam thar snying bsdus/"@bo-x-ewts .
    
    bdr:TT0F4BBCF04FC0DC3C
        a                   bdo:CoverTitle ;
        rdfs:label          "skyob pa 'jig rten mgon po'i rnam thar/"@bo-x-ewts .
    
    bdr:TT2FE65F0FB0739DF6
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "jue ba ren qin bai zhuan"@zh-latn-pinyin-x-ndia .
    
    bdr:TT8707810A30534F3D
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "觉巴仁钦拜传"@zh-hans .
    
    bdr:TTC9CB178A0A99B7F2
        a                   bdo:Title ;
        rdfs:label          "'bri gung skyob pa 'jig rten gsum mgon gyi rnam thar snying bsdus/"@bo-x-ewts .
}
