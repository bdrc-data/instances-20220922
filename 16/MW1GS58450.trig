@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1GS58450 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGA225499E73C8875F
        a                   adm:InitialDataCreation ;
        adm:logDate         "2007-09-11T07:39:48.984000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged for scan request"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1GS58450  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1GS58450 ;
        adm:facetIndex      11 ;
        adm:gitPath         "16/MW1GS58450.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1GS58450 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LGA225499E73C8875F , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVFF1E201E7E546638
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2006"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID53E0A793F5A0216F
        a                   bdr:HollisId ;
        rdf:value           "014255496" .
    
    bdr:ID860D4687C6598935
        a                   bf:Isbn ;
        rdf:value           "7542111388" .
    
    bdr:IDB4417A8F10AA3DA0
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3705 .G98 2006" .
    
    bdr:IDE3F991D4CEBEB6BA
        a                   bf:Lccn ;
        rdf:value           "2007391500" .
    
    bdr:MW1GS58450  a       bdo:Instance ;
        bf:identifiedBy     bdr:ID53E0A793F5A0216F , bdr:ID860D4687C6598935 , bdr:IDB4417A8F10AA3DA0 , bdr:IDE3F991D4CEBEB6BA ;
        bdo:authorshipStatement  "gzungs 'bum thar gyis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2, 161 p." ;
        bdo:hasTitle        bdr:TTDCE5DFEDC4FC8DF1 ;
        bdo:instanceEvent   bdr:EVFF1E201E7E546638 ;
        bdo:instanceHasReproduction  bdr:IE1GS58450 , bdr:W1GS58450 ;
        bdo:instanceOf      bdr:WA1GS58450 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "*lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:prefLabel      "rig rtsom skad cha'i rigs dang rtsom 'jug gi rnam gzhag rin chen sil ma/"@bo-x-ewts .
    
    bdr:TTDCE5DFEDC4FC8DF1
        a                   bdo:Title ;
        rdfs:label          "rig rtsom skad cha'i rigs dang rtsom 'jug gi rnam gzhag rin chen sil ma/"@bo-x-ewts .
}
