@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG422 {
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGDE249E4513C1A20C
        a                   adm:InitialDataCreation ;
        adm:logDate         "2008-08-26T15:13:14.051000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged and released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG422  a         adm:AdminData ;
        adm:adminAbout      bdr:MW1KG422 ;
        adm:facetIndex      13 ;
        adm:gitPath         "0b/MW1KG422.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG422 ;
        adm:logEntry        bda:LG0BDSSNALYNIFEBKX , bda:LGDE249E4513C1A20C , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV0B48DDE14F3B922A
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2007"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID5528E6EE7F1B8D4A
        a                   bf:Isbn ;
        rdf:value           "9787105085019" .
    
    bdr:ID70E959AF74DE59EE
        a                   bf:Lccn ;
        rdf:value           "2008308717" .
    
    bdr:ID81D8C955AEA77400
        a                   bdr:HollisId ;
        rdf:value           "014256837" .
    
    bdr:IDE907E2CA8DE0B1B0
        a                   bf:ShelfMarkLcc ;
        rdf:value           "DS797.82.B75 B77 2007" .
    
    bdr:MW1KG422  a         bdo:Instance ;
        bf:identifiedBy     bdr:ID5528E6EE7F1B8D4A , bdr:ID70E959AF74DE59EE , bdr:ID81D8C955AEA77400 , bdr:IDE907E2CA8DE0B1B0 ;
        bdo:authorshipStatement  "'gom bstan 'dzin chos dbyings kyis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "4, 3, 362 p." ;
        bdo:hasTitle        bdr:TT04DED89B8D256CE8 , bdr:TT27E2B28B4F6E5B3A , bdr:TTB5415271DF07C50E ;
        bdo:instanceEvent   bdr:EV0B48DDE14F3B922A ;
        bdo:instanceHasReproduction  bdr:IE1KG422 , bdr:W1KG422 ;
        bdo:instanceOf      bdr:WA1KG422 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "na xu xian ren zu ji"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "mes po'i byung ba/"@bo-x-ewts .
    
    bdr:TT04DED89B8D256CE8
        a                   bdo:Subtitle ;
        rdfs:label          "btsan po'i bar gyi dpal sde nags shod stong bu chung dang 'gom sde leng drug"@bo-x-ewts .
    
    bdr:TT27E2B28B4F6E5B3A
        a                   bdo:ColophonTitle ;
        rdfs:label          "na xu xian ren zu ji"@zh-latn-pinyin-x-ndia .
    
    bdr:TTB5415271DF07C50E
        a                   bdo:Title ;
        rdfs:label          "mes po'i byung ba/"@bo-x-ewts .
}
