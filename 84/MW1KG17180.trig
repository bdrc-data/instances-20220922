@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG17180 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG56199287C31C2DF7
        a                   adm:UpdateData ;
        adm:logDate         "2014-06-25T15:21:24.438000+00:00"^^xsd:dateTime ;
        adm:logMessage      "republished"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGC96BD35AC4B405C7
        a                   adm:InitialDataCreation ;
        adm:logDate         "2014-04-17T09:49:16.137000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG17180  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG17180 ;
        adm:facetIndex      15 ;
        adm:gitPath         "84/MW1KG17180.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG17180 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG56199287C31C2DF7 , bda:LGC96BD35AC4B405C7 , bda:LGIM3B24E310 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EVC99040CD59F24F9B
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2008"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID71946F68AFC852C1
        a                   bdr:HollisId ;
        rdf:value           "014256456" .
    
    bdr:IDBA24BA8EED761E33
        a                   bf:Lccn ;
        rdf:value           "2009347113" .
    
    bdr:IDC2E044C782386A15
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3748 .G4 2008" .
    
    bdr:IDMW1KG17180_ZVAJV6YD0DCG
        a                   bf:Isbn ;
        rdf:value           "9787540941246" .
    
    bdr:MW1KG17180  a       bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID71946F68AFC852C1 , bdr:IDBA24BA8EED761E33 , bdr:IDC2E044C782386A15 , bdr:IDMW1KG17180_ZVAJV6YD0DCG ;
        bdo:authorshipStatement  "bstan 'dzin grags pas phab/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2 v." ;
        bdo:hasTitle        bdr:TT00D25BD6A423E928 , bdr:TT683BF62125D0CD67 , bdr:TT72D6B26203FF3BE9 , bdr:TTB7738E8531683492 , bdr:TTF315420CCE614778 ;
        bdo:illustrations   "ill. (1 col. plate)" ;
        bdo:instanceEvent   bdr:EVC99040CD59F24F9B ;
        bdo:instanceHasReproduction  bdr:W1KG17180 ;
        bdo:instanceOf      bdr:WA1KG17180 ;
        bdo:numberOfVolumes  2 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron dpe skrun tshogs pa/ si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG17180 ;
        bdo:seriesNumber    "v. 1-2" ;
        skos:altLabel       "格萨尔王传 --雪山伏藏库"@zh-hans ;
        skos:prefLabel      "gangs gter rin po che'i rdzong /"@bo-x-ewts .
    
    bdr:TT00D25BD6A423E928
        a                   bdo:Title ;
        rdfs:label          "gangs gter rin po che'i rdzong /"@bo-x-ewts .
    
    bdr:TT683BF62125D0CD67
        a                   bdo:TitlePageTitle ;
        rdfs:label          "gling ge sar rgyal po'i sgrungHdkar dmar 'od kyi dum bu lasHgangs gter rin po che'i rdzong /"@bo-x-ewts .
    
    bdr:TT72D6B26203FF3BE9
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "ge sa er wang zhuan-- xue shan fu zang ku"@zh-latn-pinyin-x-ndia .
    
    bdr:TTB7738E8531683492
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "格萨尔王传 --雪山伏藏库"@zh-hans .
    
    bdr:TTF315420CCE614778
        a                   bdo:OtherTitle ;
        rdfs:label          "gling ge sar rgyal po'i sgrungHdkar dmar 'od kyi dum bu lasHwer ma'i brda sgrung phyogs bsgrigs/"@bo-x-ewts .
}
