@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21678 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG184CA2CF8698A909
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21678  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21678 ;
        adm:facetIndex      12 ;
        adm:gitPath         "84/MW21678.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21678 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG184CA2CF8698A909 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVE860C33DC9D5F214
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1995"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID2E67D2C32D04D29D
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3717 .C47 1995" .
    
    bdr:ID4152BA98F88AFA0E
        a                   bf:Lccn ;
        rdf:value           "98900301" .
    
    bdr:IDAC5BF00A5DD9F53A
        a                   bdr:HollisId ;
        rdf:value           "014258386" .
    
    bdr:IDMW21678_BPRP9IWB4JLT
        a                   bf:Isbn ;
        rdf:value           "9787800572234" .
    
    bdr:IDMW21678_SXNVAB0ILQKZ
        a                   bf:Isbn ;
        rdf:value           "7800572234" .
    
    bdr:MW21678  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID2E67D2C32D04D29D , bdr:ID4152BA98F88AFA0E , bdr:IDAC5BF00A5DD9F53A , bdr:IDMW21678_BPRP9IWB4JLT , bdr:IDMW21678_SXNVAB0ILQKZ ;
        bdo:authorshipStatement  "chos 'phel gyis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2, 5, 408 p." ;
        bdo:hasTitle        bdr:TT9BB45DABFCEADC0A , bdr:TTA2147DD8BC697C87 ;
        bdo:instanceEvent   bdr:EVE860C33DC9D5F214 ;
        bdo:instanceHasReproduction  bdr:W21678 ;
        bdo:instanceOf      bdr:WA21678 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "pe cin/"@bo-x-ewts ;
        bdo:publisherName   "krung go'i bod kyi shes rig dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "xiu ci xue zhu shu"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "snyan ngag gi rnam bshad sgra dbyangs lha mo dgyes pa'i rol mtsho/"@bo-x-ewts .
    
    bdr:TT9BB45DABFCEADC0A
        a                   bdo:Title ;
        rdfs:label          "snyan ngag gi rnam bshad sgra dbyangs lha mo dgyes pa'i rol mtsho/"@bo-x-ewts .
    
    bdr:TTA2147DD8BC697C87
        a                   bdo:ColophonTitle ;
        rdfs:label          "xiu ci xue zhu shu"@zh-latn-pinyin-x-ndia .
}
