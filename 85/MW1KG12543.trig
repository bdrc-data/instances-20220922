@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG12543 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGA1C5A952FCF446D1
        a                   adm:InitialDataCreation ;
        adm:logDate         "2012-06-28T10:50:24.634000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGC7482C37604BFA61
        a                   adm:UpdateData ;
        adm:logDate         "2012-11-14T15:36:46.551000+00:00"^^xsd:dateTime ;
        adm:logMessage      "changed accessioned to released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG12543  a       adm:AdminData ;
        adm:adminAbout      bdr:MW1KG12543 ;
        adm:facetIndex      14 ;
        adm:gitPath         "85/MW1KG12543.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG12543 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LGA1C5A952FCF446D1 , bda:LGC7482C37604BFA61 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV2C75D6C73F0C52F4
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1979"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID34707C324A33BCAE
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ4890.V343 T5275 1979" .
    
    bdr:ID4FAAD6C0BEDA1AD4
        a                   bf:Lccn ;
        rdf:value           "80900873" .
    
    bdr:ID8467E781C492CCB7
        a                   bdr:HarvardShelfId ;
        rdf:value           "011210194-1" .
    
    bdr:ID8BDE447E1BF792D3
        a                   bdr:HollisId ;
        rdf:value           "014255906" .
    
    bdr:MW1KG12543  a       bdo:Instance ;
        bf:identifiedBy     bdr:ID34707C324A33BCAE , bdr:ID4FAAD6C0BEDA1AD4 , bdr:ID8467E781C492CCB7 , bdr:ID8BDE447E1BF792D3 ;
        bdo:extentStatement  "455 p." ;
        bdo:hasTitle        bdr:TT01287C6938681ADA , bdr:TT7B22C195F8CEBEF8 , bdr:TTB4D991147A737F74 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV2C75D6C73F0C52F4 ;
        bdo:instanceHasReproduction  bdr:W1KG12543 ;
        bdo:instanceOf      bdr:WA1KG12543 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "bir, h.p."@en ;
        bdo:publisherName   "d. tsondu senghe"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        bdo:sourceNote      "reproduced from a rare manuscript from the library of bla-ma chos-grags-rgya-mtsho"@en ;
        skos:altLabel       "dpal sa skya'i mkha' spyod ma'i sgrub skor : a collection of sadhana practices focussing upon the realization of vajrayogini"@en ;
        skos:prefLabel      "dpal sa skya'i mkha' spyod ma'i sgrub skor/"@bo-x-ewts .
    
    bdr:TT01287C6938681ADA
        a                   bdo:TitlePageTitle ;
        rdfs:label          "dpal sa skya'i mkha' spyod ma'i sgrub skor : a collection of sadhana practices focussing upon the realization of vajrayogini"@en .
    
    bdr:TT7B22C195F8CEBEF8
        a                   bdo:Title ;
        rdfs:label          "dpal sa skya'i mkha' spyod ma'i sgrub skor/"@bo-x-ewts .
    
    bdr:TTB4D991147A737F74
        a                   bdo:CoverTitle ;
        rdfs:label          "Sa skya paʼi lugs Mkhaʼ spyod maʼi sgrub skor :a collection of sādhana practices focussing upon the realization of Vajrayoginī /by Sa-skya-pa Goṅ-ma Kun-dgaʼ-blo-gros and other masters of the Sa-skya-pa tradition."@en-x-mixed .
}
