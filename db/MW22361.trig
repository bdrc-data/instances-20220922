@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW22361 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG4149F8829A75A7B6
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW22361  a          adm:AdminData ;
        adm:adminAbout      bdr:MW22361 ;
        adm:facetIndex      9 ;
        adm:gitPath         "db/MW22361.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW22361 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG4149F8829A75A7B6 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EV6A4C1FFD2C0181D2
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2001"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID622CE9E7F5E016D4
        a                   bdr:HollisId ;
        rdf:value           "014258762" .
    
    bdr:IDMW22361_9UMCKYHWU02K
        a                   bf:Isbn ;
        rdf:value           "9789624504149" .
    
    bdr:IDMW22361_LDWRC6J5E3KM
        a                   bf:Isbn ;
        rdf:value           "9624504148" .
    
    bdr:MW22361  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID622CE9E7F5E016D4 , bdr:IDMW22361_9UMCKYHWU02K , bdr:IDMW22361_LDWRC6J5E3KM ;
        bdo:authorshipStatement  "bla nag pa ye shes bzang pos brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "6, 329 p." ;
        bdo:hasTitle        bdr:TT4E599D921472D4C7 , bdr:TTC94013F6C762C7CB ;
        bdo:illustrations   "8 [p.] col. plates" ;
        bdo:instanceEvent   bdr:EV6A4C1FFD2C0181D2 ;
        bdo:instanceHasReproduction  bdr:W22361 ;
        bdo:instanceOf      bdr:WA22361 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "zhang kang /"@bo-x-ewts ;
        bdo:publisherName   "zhang kang then mA dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "mang la bu luo zhi"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "mang ra'i lo rgyus/"@bo-x-ewts .
    
    bdr:TT4E599D921472D4C7
        a                   bdo:ColophonTitle ;
        rdfs:label          "mang la bu luo zhi"@zh-latn-pinyin-x-ndia .
    
    bdr:TTC94013F6C762C7CB
        a                   bdo:Title ;
        rdfs:label          "mang ra'i lo rgyus/"@bo-x-ewts .
}
