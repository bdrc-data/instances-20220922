@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21506 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG47528AF90C5EE3E5
        a                   adm:UpdateData ;
        adm:logDate         "2011-04-14T14:59:04.796000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added shad and annotation"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG90395988401A3C76
        a                   adm:UpdateData ;
        adm:logDate         "2013-02-27T09:24:24.960000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type and source printery"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGA756F7B1662CA2BB
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21506  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21506 ;
        adm:facetIndex      13 ;
        adm:gitPath         "f5/MW21506.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21506 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG47528AF90C5EE3E5 , bda:LG90395988401A3C76 , bda:LGA756F7B1662CA2BB , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV0DAC099873127F17
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1969/1971"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID325A9DDBD92CC366
        a                   bf:Lccn ;
        rdf:value           "73902408" .
    
    bdr:ID49FB2D365885D0F0
        a                   bdr:HollisId ;
        rdf:value           "014258314" .
    
    bdr:IDE7C08250E5C2573A
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7550 .T48 1969" .
    
    bdr:MW21506  a          bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID325A9DDBD92CC366 , bdr:ID49FB2D365885D0F0 , bdr:IDE7C08250E5C2573A ;
        bdo:authorshipStatement  "edited and reproduced by ngawang gelek demo"@en ;
        bdo:biblioNote      "reproduced from the lhasa zol par-khang edition"@en ;
        bdo:extentStatement  "10 v." ;
        bdo:hasSourcePrintery  bdr:G1PD129185 ;
        bdo:hasTitle        bdr:TT5B666020CECF19EB , bdr:TTC2EEC6DE37CA6496 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV0DAC099873127F17 ;
        bdo:instanceHasReproduction  bdr:W21506 ;
        bdo:instanceOf      bdr:WA21506 ;
        bdo:numberOfVolumes  10 ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:publisherLocation  "new delhi"@en ;
        bdo:publisherName   "ngawang gelek demo"@en ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS11818 ;
        bdo:seriesNumber    "v. 1-10" ;
        skos:altLabel       "collected works of thu'u bkwan blo bzang chos kyi nyi ma"@en ;
        skos:prefLabel      "gsung 'bum/_blo bzang chos kyi nyi ma/ （zhol par ma/）"@bo-x-ewts .
    
    bdr:TT5B666020CECF19EB
        a                   bdo:Title ;
        rdfs:label          "gsung 'bum/_blo bzang chos kyi nyi ma/ （zhol par ma/）"@bo-x-ewts .
    
    bdr:TTC2EEC6DE37CA6496
        a                   bdo:TitlePageTitle ;
        rdfs:label          "collected works of thu'u bkwan blo bzang chos kyi nyi ma"@en .
}
