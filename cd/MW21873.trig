@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21873 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGA26BAFC715CBF8D8
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGA5F86F07BD978CEE
        a                   adm:UpdateData ;
        adm:logDate         "2010-04-06T10:16:46.971000+00:00"^^xsd:dateTime ;
        adm:logMessage      "corrected extent"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGBCBBA226F00D1688
        a                   adm:UpdateData ;
        adm:logDate         "2016-04-04T15:48:39.885000+00:00"^^xsd:dateTime ;
        adm:logMessage      "released from onhold and completed cataloging"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:MW21873  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21873 ;
        adm:facetIndex      11 ;
        adm:gitPath         "cd/MW21873.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21873 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LGA26BAFC715CBF8D8 , bda:LGA5F86F07BD978CEE , bda:LGBCBBA226F00D1688 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EVA5661EF9410431E9
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1996"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID692D040BB2A31AAA
        a                   bf:Lccn ;
        rdf:value           "98900273" .
    
    bdr:ID82BF6CC702B0B52C
        a                   bf:ShelfMarkLcc ;
        rdf:value           "B162 .T76 1996" .
    
    bdr:IDMW21873_8UXUK450QN7L
        a                   bf:Isbn ;
        rdf:value           "9787542104533" .
    
    bdr:IDMW21873_ZKWCD67SSMIA
        a                   bf:Isbn ;
        rdf:value           "7542104535" .
    
    bdr:MW21873  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID692D040BB2A31AAA , bdr:ID82BF6CC702B0B52C , bdr:IDMW21873_8UXUK450QN7L , bdr:IDMW21873_ZKWCD67SSMIA ;
        bdo:authorshipStatement  "'brong bu tshe ring rdo rjes brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2, 8, 5, 238 p." ;
        bdo:hasTitle        bdr:TT2E86DFA95CEB0857 , bdr:TT45C30D5B1206877D , bdr:TT6A0C91D6DCFB2435 ;
        bdo:instanceEvent   bdr:EVA5661EF9410431E9 ;
        bdo:instanceHasReproduction  bdr:W21873 ;
        bdo:instanceOf      bdr:WA21873 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "藏族哲学的理智"@zh-hans ;
        skos:prefLabel      "bod kyi sher rtogs rig pa'i rigs shes/"@bo-x-ewts .
    
    bdr:TT2E86DFA95CEB0857
        a                   bdo:Title ;
        rdfs:label          "bod kyi sher rtogs rig pa'i rigs shes/"@bo-x-ewts .
    
    bdr:TT45C30D5B1206877D
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "zang zu zhe xue de li zhi"@zh-latn-pinyin-x-ndia .
    
    bdr:TT6A0C91D6DCFB2435
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "藏族哲学的理智"@zh-hans .
}
