@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW22142 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGA04286B2012B5821
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGD64BC55303B57EC5
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-12T11:45:51.919000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW22142  a          adm:AdminData ;
        adm:adminAbout      bdr:MW22142 ;
        adm:facetIndex      15 ;
        adm:gitPath         "d3/MW22142.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW22142 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LGA04286B2012B5821 , bda:LGD64BC55303B57EC5 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV2AA012042C87294A
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1985"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID1A84F377C50C0236
        a                   bf:Lccn ;
        rdf:value           "85902426" .
    
    bdr:ID1A95D3A42A5F4DF9
        a                   bdr:HollisId ;
        rdf:value           "014258656" .
    
    bdr:ID8E3C0AC5C734CA60
        a                   bf:ShelfMarkLcc ;
        rdf:value           "TC169 .N38 1984" .
    
    bdr:ID94F8D2B34655C311
        a                   bdr:PL480 ;
        rdf:value           "bhu-tib-443" .
    
    bdr:MW22142  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID1A84F377C50C0236 , bdr:ID1A95D3A42A5F4DF9 , bdr:ID8E3C0AC5C734CA60 , bdr:ID94F8D2B34655C311 ;
        bdo:authorshipStatement  "edited and completed by dharma-dzU-ha"@en ;
        bdo:biblioNote      "reproduced from tha rare manuscript preserved in the national library of bhutan"@en ;
        bdo:extentStatement  "184 p" ;
        bdo:hasTitle        bdr:TT14C414C013F87BE7 , bdr:TTCA405BFFFF3484AA , bdr:TTD463BC498A73C10E ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV2AA012042C87294A ;
        bdo:instanceHasReproduction  bdr:W22142 ;
        bdo:instanceOf      bdr:WA22142 ;
        bdo:note            bdr:NTC9D42CBF9F485D99 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "thimphu"@en ;
        bdo:publisherName   "national library of bhutan"@en ;
        bdo:script          bdr:ScriptDbuCan ;
        skos:altLabel       "the autobiography and instructions of the stod 'brug-pa dkar-brgyud-pa master, chos-rje dpyil-dkar-ba (1228-1300("@en ;
        skos:prefLabel      "spyil dkar ba'i rnam thar/"@bo-x-ewts .
    
    bdr:NTC9D42CBF9F485D99
        a                   bdo:Note ;
        bdo:noteText        "tbrc holds scanned images, tiffs and pdf files"@en .
    
    bdr:TT14C414C013F87BE7
        a                   bdo:TitlePageTitle ;
        rdfs:label          "the autobiography and instructions of the stod 'brug-pa dkar-brgyud-pa master, chos-rje dpyil-dkar-ba (1228-1300("@en .
    
    bdr:TTCA405BFFFF3484AA
        a                   bdo:Title ;
        rdfs:label          "spyil dkar ba'i rnam thar/"@bo-x-ewts .
    
    bdr:TTD463BC498A73C10E
        a                   bdo:TitlePageTitle ;
        rdfs:label          "chos rje rin po che spyil dkar ba'i rnam thar chen mo bstan pa'i byung tshul mdzad pa dri ma med pa rin po che rgyan gyi phreng ba/"@bo-alalc97 .
}
