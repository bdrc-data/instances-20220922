@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW30019 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGBF6C764B8E3B1FDC
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGCDDFB35BD8C0ED0C
        a                   adm:UpdateData ;
        adm:logDate         "2007-11-27T10:19:27.678000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00003 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW30019  a          adm:AdminData ;
        adm:adminAbout      bdr:MW30019 ;
        adm:facetIndex      16 ;
        adm:gitPath         "b2/MW30019.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW30019 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LGBF6C764B8E3B1FDC , bda:LGCDDFB35BD8C0ED0C , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CR681C39C44054547A
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P9469 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV3F798372029390ED
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2004"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID71F87834A97F2020
        a                   bdr:HollisId ;
        rdf:value           "014260572" .
    
    bdr:ID89CE6413C2DC13A9
        a                   bf:Lccn ;
        rdf:value           "2004314767" .
    
    bdr:IDD6AD830BB05FDF3C
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ7679.4 .B64 2004 VOL. 10, ETC. BQ6115" .
    
    bdr:MW30019  a          bdo:Instance , bdo:SerialInstance ;
        bf:identifiedBy     bdr:ID71F87834A97F2020 , bdr:ID89CE6413C2DC13A9 , bdr:IDD6AD830BB05FDF3C ;
        bdo:authorshipStatement  "[karma pa mi bskyod rdo rjes mdzad] ; mkhan po dam chos zla bas bsgrigs/"@bo-x-ewts ;
        bdo:biblioNote      "v. 2 isbn: 7542009265"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:creator         bdr:CR681C39C44054547A ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2 v." ;
        bdo:hasTitle        bdr:TT5A196FCE4BC29F77 , bdr:TTDAB68031031438A0 , bdr:TTE379518DC1424E5F ;
        bdo:instanceEvent   bdr:EV3F798372029390ED ;
        bdo:instanceHasReproduction  bdr:IE30019 , bdr:W30019 ;
        bdo:instanceOf      bdr:WA30019 ;
        bdo:note            bdr:NTDE7F00FC52B121FA ;
        bdo:numberOfVolumes  2 ;
        bdo:publisherLocation  "zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS26176 ;
        bdo:seriesNumber    "v. 10-11" ;
        skos:prefLabel      "'dul ba/"@bo-x-ewts .
    
    bdr:NTDE7F00FC52B121FA
        a                   bdo:Note ;
        bdo:noteText        "-5750\n\ncontents: so so thar pa'i mdo'i 'grel pa rin chen 'byung gnas / karma pa mi bskyod rdo rje. --'dul ba mdo rtsa ba'i rgya cher 'grel spyi'i don mtha' dpyad dang bsdus don sa bcad dang 'bru yi don mthar chags su gnyer ba bcas 'dzam bu'i gling gsal bar byed pa'i rgyan nyi ma'i dkyil 'khor (v. 1-2) / karma pa mi bskyod rdo rje."@en .
    
    bdr:TT5A196FCE4BC29F77
        a                   bdo:Title ;
        rdfs:label          "'dul ba/"@bo-x-ewts .
    
    bdr:TTDAB68031031438A0
        a                   bdo:OtherTitle ;
        rdfs:label          "'dul ba mdo rtsa ba'i rgya cher 'grel spyi'i don mtha' dpyad dang bsdus don sa bcad dang 'bru gyi don mthar chags su gnyer ba bcas 'dzam bu'i gling gsal bar byed pa'i rgyan nyi ma'i dkyil 'khor/"@bo-x-ewts .
    
    bdr:TTE379518DC1424E5F
        a                   bdo:OtherTitle ;
        rdfs:label          "so so thar pa'i mdo'i 'grel ba rin chen 'byung gnas/"@bo-x-ewts .
}
