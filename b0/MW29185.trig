@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW29185 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG3C4225E7CD04A82D
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGA383A251C25F461D
        a                   adm:UpdateData ;
        adm:logDate         "2007-02-23T15:00:31.614000+00:00"^^xsd:dateTime ;
        adm:logMessage      "note sogs bcug"@en ;
        adm:logWho          bdu:U00021 .
    
    bda:LGF9C3BCD72EC33885
        a                   adm:UpdateData ;
        adm:logDate         "2021-05-07T09:13:53.028000+00:00"^^xsd:dateTime ;
        adm:logMessage      "reviewed"@en ;
        adm:logWho          bdu:U00019 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW29185  a          adm:AdminData ;
        adm:adminAbout      bdr:MW29185 ;
        adm:facetIndex      20 ;
        adm:gitPath         "b0/MW29185.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW29185 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG3C4225E7CD04A82D , bda:LGA383A251C25F461D , bda:LGF9C3BCD72EC33885 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CR8FB3B20935168094
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P9565 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV4A7E677FD6E3826F
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2000"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID4DAA9069546680D9
        a                   bf:ShelfMarkLcc ;
        rdf:value           "QA76.15 .B624 2003" .
    
    bdr:ID509C876E869EB38C
        a                   bdr:HarvardShelfId ;
        rdf:value           "010259567-4" .
    
    bdr:ID5C3ECAD752501A22
        a                   bdr:HollisId ;
        rdf:value           "014259920" .
    
    bdr:IDFC1053B1D85FC468
        a                   bf:Lccn ;
        rdf:value           "2004326513" .
    
    bdr:IDMW29185_DM8DVCD63II1
        a                   bf:Isbn ;
        rdf:value           "7805883637" .
    
    bdr:IDMW29185_WKXG7RLM9FVE
        a                   bf:Isbn ;
        rdf:value           "9787805883632" .
    
    bdr:MW29185  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID4DAA9069546680D9 , bdr:ID509C876E869EB38C , bdr:ID5C3ECAD752501A22 , bdr:IDFC1053B1D85FC468 , bdr:IDMW29185_DM8DVCD63II1 , bdr:IDMW29185_WKXG7RLM9FVE ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:creator         bdr:CR8FB3B20935168094 ;
        bdo:extentStatement  "18, 42, 562, 51 p." ;
        bdo:hasTitle        bdr:TT1F4F4142ED4200E5 , bdr:TT314DA070D74E5951 , bdr:TT4B4FE9E2AAF6CCDE , bdr:TTB5FED6449A37D2EF ;
        bdo:instanceEvent   bdr:EV4A7E677FD6E3826F ;
        bdo:instanceHasReproduction  bdr:W29185 ;
        bdo:instanceOf      bdr:WA29185 ;
        bdo:note            bdr:NTE24A3603B92BE990 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi dmangs mdzes rtsal dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptHani , bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS29185 ;
        skos:prefLabel      "bod rgya dbyin skad kyi rtsis 'khor tshig mdzod/"@bo-x-ewts , "tibetan-chinese-english computer dictionary"@en , "zang han ying ji suan ji ci dian"@zh-latn-pinyin-x-ndia .
    
    bdr:NTE24A3603B92BE990
        a                   bdo:Note ;
        bdo:noteText        "tbrc holds inkprint copy and digitally scanned images, tiffs and pdf files;"@en .
    
    bdr:TT1F4F4142ED4200E5
        a                   bdo:OtherTitle ;
        rdfs:label          "tibetan-chinese-english computer dictionary"@en .
    
    bdr:TT314DA070D74E5951
        a                   bdo:Title ;
        rdfs:label          "bod rgya dbyin skad kyi rtsis 'khor tshig mdzod/"@bo-x-ewts .
    
    bdr:TT4B4FE9E2AAF6CCDE
        a                   bdo:CoverTitle ;
        rdfs:label          "Bod Rgya Dbyin skad kyi rtsis ʼkhor tshig mdzod =Zang Han Ying ji suan ji ci dian = Tibetan-Chinese-English computer dictionary /[rtsom sgrig gtso bo, Zla-ba-blo-gros ; rtsom sgrig gźon pa, Skal-bzaṅ-ʼphrin-las, Bum-pa-tshe-brtan ; rtsom sgrig pa, Btsun-thar-rgyal, Saṅs-rgyas-don-grub, Rin-chen-rdo-rje]."@en-x-mixed .
    
    bdr:TTB5FED6449A37D2EF
        a                   bdo:OtherTitle ;
        rdfs:label          "zang han ying ji suan ji ci dian"@zh-latn-pinyin-x-ndia .
}
