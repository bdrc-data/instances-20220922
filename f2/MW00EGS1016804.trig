@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW00EGS1016804 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGEE63DDD7B6A1839B
        a                   adm:UpdateData ;
        adm:logDate         "2014-01-06T12:20:17.782000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGF9E38817D5D49BCF
        a                   adm:InitialDataCreation ;
        adm:logDate         "2006-12-07T09:16:44.390000+00:00"^^xsd:dateTime ;
        adm:logMessage      "first published"@en ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW00EGS1016804
        a                   adm:AdminData ;
        adm:adminAbout      bdr:MW00EGS1016804 ;
        adm:facetIndex      15 ;
        adm:gitPath         "f2/MW00EGS1016804.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW00EGS1016804 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LGEE63DDD7B6A1839B , bda:LGF9E38817D5D49BCF , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV161585BE555A9C97
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1984"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID9020F6A1A739ECB3
        a                   bf:ShelfMarkLcc ;
        rdf:value           "PL3748.G425 D78 1984" .
    
    bdr:IDB7B106330F1AB78B
        a                   bdr:HollisId ;
        rdf:value           "014253899" .
    
    bdr:IDFC1ED8BFEC5B87BD
        a                   bf:Lccn ;
        rdf:value           "84901287" .
    
    bdr:IDFEEAF3547E4D892F
        a                   bdr:PL480 ;
        rdf:value           "i tib 2502" .
    
    bdr:MW00EGS1016804
        a                   bdo:Instance ;
        bf:identifiedBy     bdr:ID9020F6A1A739ECB3 , bdr:IDB7B106330F1AB78B , bdr:IDFC1ED8BFEC5B87BD , bdr:IDFEEAF3547E4D892F ;
        bdo:biblioNote      "reproduced from a rare manusript from the collection of ven. ngag dbang rten 'byung of khams pa sgar monastery, tashi jong."@en ;
        bdo:extentStatement  "2 v." ;
        bdo:hasTitle        bdr:TT1E8D8A4BA917E410 , bdr:TT6D167FD579F9D0A4 , bdr:TT9BCD38042CB100CF , bdr:TTE392B17A49D9DAD8 ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EV161585BE555A9C97 ;
        bdo:instanceHasReproduction  bdr:W00EGS1016804 ;
        bdo:instanceOf      bdr:WA00EGS1016804 ;
        bdo:note            bdr:NT7E7877C4281E639C ;
        bdo:numberOfVolumes  2 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "dharamsala, h.p."@en ;
        bdo:publisherName   "library of tibetan works & archives"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "an episode from the gesar epic recounting the mighty king of ling's conquest of the kingdom of dru-gu"@en ;
        skos:prefLabel      "dru gu go rdzong /"@bo-x-ewts .
    
    bdr:NT7E7877C4281E639C
        a                   bdo:Note ;
        bdo:noteText        "in tibetan"@en .
    
    bdr:TT1E8D8A4BA917E410
        a                   bdo:Subtitle ;
        rdfs:label          "an episode from the gesar epic recounting the mighty king of ling's conquest of the kingdom of dru-gu"@en .
    
    bdr:TT6D167FD579F9D0A4
        a                   bdo:CoverTitle ;
        rdfs:label          "gling ge sar rgyal po'i rtogs brjod las dru gu go rdzong /"@bo-x-ewts .
    
    bdr:TT9BCD38042CB100CF
        a                   bdo:TitlePageTitle ;
        rdfs:label          "'dzam gling skyong ba'i pho lha ge sar dmag gi rgyal po'i rtogs brjd las byang bdud dru gu g.yul rgyal stobs chen thog rgod rgyal po mnga' 'bangs bcas dbang du bsdus shing go mtshan g.yang du blangs pa'i rnam thar yid 'phrog snying gi dga' ston/"@bo-x-ewts .
    
    bdr:TTE392B17A49D9DAD8
        a                   bdo:Title ;
        rdfs:label          "dru gu go rdzong /"@bo-x-ewts .
}
