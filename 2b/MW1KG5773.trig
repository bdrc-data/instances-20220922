@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG5773 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGCCF0320C13BAB9F9
        a                   adm:InitialDataCreation ;
        adm:logDate         "2010-08-11T11:34:16.513000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG5773  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1KG5773 ;
        adm:facetIndex      10 ;
        adm:gitPath         "2b/MW1KG5773.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG5773 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LGCCF0320C13BAB9F9 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EVC756AFA8EE59D19D
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2000"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID155E6C72730EBBCD
        a                   bdr:HollisId ;
        rdf:value           "014257192" .
    
    bdr:IDMW1KG5773_9Q9W65C4VM5T
        a                   bf:Isbn ;
        rdf:value           "7536719671" .
    
    bdr:IDMW1KG5773_S4TDUYWKR2YA
        a                   bf:Isbn ;
        rdf:value           "9787536719675" .
    
    bdr:MW1KG5773  a        bdo:Instance ;
        bf:identifiedBy     bdr:ID155E6C72730EBBCD , bdr:IDMW1KG5773_9Q9W65C4VM5T , bdr:IDMW1KG5773_S4TDUYWKR2YA ;
        bdo:authorshipStatement  "ldong ka dge shis chos grags kyis brtsams/"@bo-x-ewts ;
        bdo:biblioNote      "printed from computerized input"@en ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2, 7, 301 p." ;
        bdo:hasTitle        bdr:TT54176223DF641F47 , bdr:TT591BB2D0CC79EECA ;
        bdo:instanceEvent   bdr:EVC756AFA8EE59D19D ;
        bdo:instanceHasReproduction  bdr:IE1KG5773 , bdr:W1KG5773 ;
        bdo:instanceOf      bdr:WA1KG5773 ;
        bdo:note            bdr:NTFFA318E74F5D5D3F ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "kun ming /"@bo-x-ewts ;
        bdo:publisherName   "yun nan mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptHani , bdr:ScriptTibt ;
        skos:prefLabel      "bod kyi dpal 'byor lo rgyus mdor bsdus/"@bo-x-ewts , "zang zu jing ji jian shi"@zh-latn-pinyin-x-ndia .
    
    bdr:NTFFA318E74F5D5D3F
        a                   bdo:Note ;
        bdo:noteText        "written in tibetan and chinese"@en .
    
    bdr:TT54176223DF641F47
        a                   bdo:Title ;
        rdfs:label          "bod kyi dpal 'byor lo rgyus mdor bsdus/"@bo-x-ewts .
    
    bdr:TT591BB2D0CC79EECA
        a                   bdo:OtherTitle ;
        rdfs:label          "zang zu jing ji jian shi"@zh-latn-pinyin-x-ndia .
}
