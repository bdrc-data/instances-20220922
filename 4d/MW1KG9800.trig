@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG9800 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG4BB7EF943348E9DF
        a                   adm:InitialDataCreation ;
        adm:logDate         "2011-06-06T10:59:43.793000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG86462777189E80EA
        a                   adm:UpdateData ;
        adm:logDate         "2013-02-25T14:41:49.124000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type and source printery"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG9800  a        adm:AdminData ;
        adm:adminAbout      bdr:MW1KG9800 ;
        adm:facetIndex      13 ;
        adm:gitPath         "4d/MW1KG9800.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG9800 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG4BB7EF943348E9DF , bda:LG86462777189E80EA , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV116746A5CD1076D2
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "199X"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID04FBCDFF2729B6DC
        a                   bdr:HarvardShelfId ;
        rdf:value           "008389341-5" .
    
    bdr:IDE1DA5FD093768DC1
        a                   bdr:HollisId ;
        rdf:value           "014257632" .
    
    bdr:MW1KG9800  a        bdo:Instance ;
        bf:identifiedBy     bdr:ID04FBCDFF2729B6DC , bdr:IDE1DA5FD093768DC1 ;
        bdo:biblioNote      "impression from the blocks preserved at bkra shis lhun po monastery in tibet"@en ;
        bdo:extentStatement  "10 v." ;
        bdo:hasSourcePrintery  bdr:G2CN10913 ;
        bdo:hasTitle        bdr:TT41473EE08B9FE0DB , bdr:TTC6E30D83F1C3CD0D , bdr:TTF3B12669F1D00821 ;
        bdo:instanceEvent   bdr:EV116746A5CD1076D2 ;
        bdo:instanceHasReproduction  bdr:W1KG9800 ;
        bdo:instanceOf      bdr:WA1KG9800 ;
        bdo:note            bdr:NTFAA9126119F9085C ;
        bdo:numberOfVolumes  10 ;
        bdo:printMethod     bdr:PrintMethod_Relief_WoodBlock ;
        bdo:publisherLocation  "*gzhis ka rtse/"@bo-x-ewts ;
        bdo:publisherName   "*bkra shis lhun dgon/"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "Paṇ chen thams cad mkhyen pa Blo bzaṅ dpal ldan ye śe dpal bzaṅ poʾi gsuṅ ʾbum."@en-x-mixed ;
        skos:prefLabel      "gsung 'bum/_dpal ldan ye shes/ (bkras lhun par ma/)"@bo-x-ewts .
    
    bdr:NTFAA9126119F9085C
        a                   bdo:Note ;
        bdo:noteText        "the volume marked ka has two volumes and the last volume is bka' rgya ma"@en .
    
    bdr:TT41473EE08B9FE0DB
        a                   bdo:Title ;
        rdfs:label          "gsung 'bum/_dpal ldan ye shes/ (bkras lhun par ma/)"@bo-x-ewts .
    
    bdr:TTC6E30D83F1C3CD0D
        a                   bdo:CoverTitle ;
        rdfs:label          "Paṇ chen thams cad mkhyen pa Blo bzaṅ dpal ldan ye śe dpal bzaṅ poʾi gsuṅ ʾbum."@en-x-mixed .
    
    bdr:TTF3B12669F1D00821
        a                   bdo:TitlePageTitle ;
        rdfs:label          "paN chen thams cad mkhyen pa blo bzang dpal ldan ye shes dpal bzang po'i gsung 'bum/"@bo-x-ewts .
}
