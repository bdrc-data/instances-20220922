@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW29060 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG3963CA0311C670BE
        a                   adm:UpdateData ;
        adm:logDate         "2007-11-27T14:37:15.646000+00:00"^^xsd:dateTime ;
        adm:logMessage      "bcos"@en ;
        adm:logWho          bdu:U00003 .
    
    bda:LG3B96D256D54E666D
        a                   adm:UpdateData ;
        adm:logDate         "2007-11-27T14:09:37.615000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added note"@en ;
        adm:logWho          bdu:U00003 .
    
    bda:LG65D4B3FA1BB36B83
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW29060  a          adm:AdminData ;
        adm:adminAbout      bdr:MW29060 ;
        adm:facetIndex      11 ;
        adm:gitPath         "97/MW29060.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW29060 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG3963CA0311C670BE , bda:LG3B96D256D54E666D , bda:LG65D4B3FA1BB36B83 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV4D57B8021244D902
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2004"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID5CFF7691B6C80418
        a                   bdr:HollisId ;
        rdf:value           "014259897" .
    
    bdr:IDMW29060_ETEIO4TRN26F
        a                   bf:Isbn ;
        rdf:value           "9787540929664" .
    
    bdr:IDMW29060_MGKUOJKNYLXW
        a                   bf:Isbn ;
        rdf:value           "7540929669" .
    
    bdr:MW29060  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID5CFF7691B6C80418 , bdr:IDMW29060_ETEIO4TRN26F , bdr:IDMW29060_MGKUOJKNYLXW ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "7, 2, 325 pp." ;
        bdo:hasTitle        bdr:TT06D24C93FE159CE7 , bdr:TT728609512EBDDEF0 ;
        bdo:instanceEvent   bdr:EV4D57B8021244D902 ;
        bdo:instanceHasReproduction  bdr:W29060 ;
        bdo:instanceOf      bdr:WA29060 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "zanla awangcuocheng lunwen ji"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "btsan lha ngag dbang tshul khrims kyi dpyad rtsom phyogs bsgrigs/"@bo-x-ewts .
    
    bdr:TT06D24C93FE159CE7
        a                   bdo:Title ;
        rdfs:label          "btsan lha ngag dbang tshul khrims kyi dpyad rtsom phyogs bsgrigs/"@bo-x-ewts .
    
    bdr:TT728609512EBDDEF0
        a                   bdo:ColophonTitle ;
        rdfs:label          "zanla awangcuocheng lunwen ji"@zh-latn-pinyin-x-ndia .
}
