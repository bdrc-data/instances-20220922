@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG513 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG6F49AD811BBD6920
        a                   adm:InitialDataCreation ;
        adm:logDate         "2008-09-08T13:04:14.118000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged and released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG513  a         adm:AdminData ;
        adm:adminAbout      bdr:MW1KG513 ;
        adm:facetIndex      16 ;
        adm:gitPath         "b9/MW1KG513.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG513 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LG6F49AD811BBD6920 , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV90A0A7DA2884640A
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2008"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID2F32B4870AE9903D
        a                   bdr:HollisId ;
        rdf:value           "014257064" .
    
    bdr:ID39959105264AD7D5
        a                   bf:Isbn ;
        rdf:value           "9787542113184" .
    
    bdr:ID62E3CD340125E3E3
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ6349.A25 S47 2008" .
    
    bdr:ID6514AED7CDE4C832
        a                   bf:Lccn ;
        rdf:value           "2008308682" .
    
    bdr:ID6BB5831A292422D4
        a                   bdr:HarvardShelfId ;
        rdf:value           "012032038-X" .
    
    bdr:MW1KG513  a         bdo:Instance ;
        bf:identifiedBy     bdr:ID2F32B4870AE9903D , bdr:ID39959105264AD7D5 , bdr:ID62E3CD340125E3E3 , bdr:ID6514AED7CDE4C832 , bdr:ID6BB5831A292422D4 ;
        bdo:authorshipStatement  "shes rab rgya mtshos brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "4, 122 p." ;
        bdo:hasTitle        bdr:TT4DAFFB4A6CB720D1 , bdr:TT5E586B503CA3F1DD , bdr:TT7EE200320AFE1EA0 , bdr:TTBAC48B212A6653C4 ;
        bdo:instanceEvent   bdr:EV90A0A7DA2884640A ;
        bdo:instanceHasReproduction  bdr:IE1KG513 , bdr:W1KG513 ;
        bdo:instanceOf      bdr:WA1KG513 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "*lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "Phyi dar bstan paʼi gnas mchog Mdo-smad A-chuṅ-gnam-rdzoṅ gi lo rgyus bden gtam lha yi rol mo źes bya ba bźugs so /Śes-rab-rgya-mtshos brtsams."@en-x-mixed , "zang chuan fo jiao hou huang sheng di a qiong nan zong si jian shi"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "a chung gnam rdzong gi lo rgyus/"@bo-x-ewts .
    
    bdr:TT4DAFFB4A6CB720D1
        a                   bdo:TitlePageTitle ;
        rdfs:label          "phyi dar bstan pa'i gnas mchog mdo smad a chung gnam rdzong gi lo rgyus bden gtam lha yi rol mo/"@bo-x-ewts .
    
    bdr:TT5E586B503CA3F1DD
        a                   bdo:CoverTitle ;
        rdfs:label          "Phyi dar bstan paʼi gnas mchog Mdo-smad A-chuṅ-gnam-rdzoṅ gi lo rgyus bden gtam lha yi rol mo źes bya ba bźugs so /Śes-rab-rgya-mtshos brtsams."@en-x-mixed .
    
    bdr:TT7EE200320AFE1EA0
        a                   bdo:ColophonTitle ;
        rdfs:label          "zang chuan fo jiao hou huang sheng di a qiong nan zong si jian shi"@zh-latn-pinyin-x-ndia .
    
    bdr:TTBAC48B212A6653C4
        a                   bdo:Title ;
        rdfs:label          "a chung gnam rdzong gi lo rgyus/"@bo-x-ewts .
}
