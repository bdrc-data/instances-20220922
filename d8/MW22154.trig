@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW22154 {
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG88AB925E7F207307
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:MW22154  a          adm:AdminData ;
        adm:adminAbout      bdr:MW22154 ;
        adm:facetIndex      12 ;
        adm:gitPath         "d8/MW22154.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW22154 ;
        adm:logEntry        bda:LG0BDS5ZNKT5O6YISF , bda:LG88AB925E7F207307 , bda:LGIMA45794A9D2 ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusOnHold .
    
    bdr:EV0CA3EBCC0CD4D9A8
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1996"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID16DB93C31B781934
        a                   bf:ShelfMarkLcc ;
        rdf:value           "RS403 .K27 1996" .
    
    bdr:ID1E2E321DF1D4A9C5
        a                   bf:Lccn ;
        rdf:value           "97916102" .
    
    bdr:ID8403E6F9337F5747
        a                   bf:Isbn ;
        rdf:value           "7542104616" .
    
    bdr:ID948C307193096494
        a                   bdr:HarvardShelfId ;
        rdf:value           "008079334-7" .
    
    bdr:MW22154  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID16DB93C31B781934 , bdr:ID1E2E321DF1D4A9C5 , bdr:ID8403E6F9337F5747 , bdr:ID948C307193096494 ;
        bdo:authorshipStatement  "karma rgyal mtshan gyis brtsams/"@bo-x-ewts ;
        bdo:copyrightStatus  bdr:CopyrightClaimed ;
        bdo:editionStatement  "par gzhi dang po/"@bo-x-ewts ;
        bdo:extentStatement  "2, 2, 215 p." ;
        bdo:hasTitle        bdr:TT083E1D820A6F51AB , bdr:TT1DE27744CE1308DB , bdr:TT395E792B86650013 , bdr:TTCB8CCD6D2343B64A ;
        bdo:instanceEvent   bdr:EV0CA3EBCC0CD4D9A8 ;
        bdo:instanceOf      bdr:WA29480 ;
        bdo:publisherLocation  "lan kru'u/"@bo-x-ewts ;
        bdo:publisherName   "kan su'u mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptLatn ;
        skos:prefLabel      "yang tig sman gyi sbyor sde 'chi med bdud rtsi'i bcud len/"@bo-x-ewts , "ʾTsho byed las daṅ po ba la ñe bar mkho baʾi zin tig gces par bsdus paʾi kha skoṅ yaṅ tig sman gyi sbyor sde ʾchi med bdud rtsiʾi bcud len /Karma-rgyal-mtshan gyis brtsams."@en-x-mixed , "tsang yao i liao ching hua"@zh-latn-wadegile .
    
    bdr:TT083E1D820A6F51AB
        a                   bdo:Title ;
        rdfs:label          "yang tig sman gyi sbyor sde 'chi med bdud rtsi'i bcud len/"@bo-x-ewts .
    
    bdr:TT1DE27744CE1308DB
        a                   bdo:TitlePageTitle ;
        rdfs:label          "'tsho byed las dang po ba la nye bar mkho ba'i zin tig gces par bsdus pa'i kha skong yang tig sman gyi sbyor sde 'chi med bdud rtsi'i bcud len/"@bo-x-ewts .
    
    bdr:TT395E792B86650013
        a                   bdo:ColophonTitle ;
        rdfs:label          "tsang yao i liao ching hua"@zh-latn-wadegile .
    
    bdr:TTCB8CCD6D2343B64A
        a                   bdo:CoverTitle ;
        rdfs:label          "ʾTsho byed las daṅ po ba la ñe bar mkho baʾi zin tig gces par bsdus paʾi kha skoṅ yaṅ tig sman gyi sbyor sde ʾchi med bdud rtsiʾi bcud len /Karma-rgyal-mtshan gyis brtsams."@en-x-mixed .
}
