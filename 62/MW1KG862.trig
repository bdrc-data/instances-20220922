@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW1KG862 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0CE224F0763FBCB1
        a                   adm:UpdateData ;
        adm:logDate         "2009-03-04T12:50:06.024000+00:00"^^xsd:dateTime ;
        adm:logWho          bdu:U00016 .
    
    bda:LG89124060250B6909
        a                   adm:UpdateData ;
        adm:logDate         "2009-03-04T12:48:41.760000+00:00"^^xsd:dateTime ;
        adm:logMessage      "qc"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LG8C251AA86FC90C6B
        a                   adm:InitialDataCreation ;
        adm:logDate         "2008-10-24T17:01:23.189000+00:00"^^xsd:dateTime ;
        adm:logMessage      "cataloged and released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW1KG862  a         adm:AdminData ;
        adm:adminAbout      bdr:MW1KG862 ;
        adm:facetIndex      16 ;
        adm:gitPath         "62/MW1KG862.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW1KG862 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG0CE224F0763FBCB1 , bda:LG89124060250B6909 , bda:LG8C251AA86FC90C6B , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:CRE97C587D52CA962B
        a                   bdo:AgentAsCreator ;
        bdo:agent           bdr:P1KG867 ;
        bdo:role            bdr:R0ER0015 .
    
    bdr:EV7F754CB13819C232
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2005"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID8A952734BC11CB0C
        a                   bdr:HarvardShelfId ;
        rdf:value           "011896784-3" .
    
    bdr:IDB259A86E8BB90520
        a                   bdr:HollisId ;
        rdf:value           "014257422" .
    
    bdr:IDMW1KG862_Q2M2D7NIZE1Q
        a                   bf:Isbn ;
        rdf:value           "7508507622" .
    
    bdr:IDMW1KG862_XCSVKTR8VYRF
        a                   bf:Isbn ;
        rdf:value           "9787508507620" .
    
    bdr:MW1KG862  a         bdo:Instance ;
        bf:identifiedBy     bdr:ID8A952734BC11CB0C , bdr:IDB259A86E8BB90520 , bdr:IDMW1KG862_Q2M2D7NIZE1Q , bdr:IDMW1KG862_XCSVKTR8VYRF ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:creator         bdr:CRE97C587D52CA962B ;
        bdo:extentStatement  "208 p." ;
        bdo:hasTitle        bdr:TT0863ECCACE6652B1 , bdr:TT913B90BBEC7F36A0 , bdr:TTA6D2438AAD538B82 , bdr:TTD509705AFB01E1E1 ;
        bdo:illustrations   "ill. (col.)" ;
        bdo:instanceEvent   bdr:EV7F754CB13819C232 ;
        bdo:instanceHasReproduction  bdr:IE1KG862 , bdr:W1KG862 ;
        bdo:instanceOf      bdr:WA1KG862 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "beijing"@en ;
        bdo:publisherName   "gling lnga khyab spel dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        bdo:serialInstanceOf  bdr:WAS1KG857 ;
        skos:altLabel       "Bod ljoṅs kyi sgyu rtsal /Źuṅ-wun-pin gyis brtsoms ; Dros-dmar-tshe-riṅ-rdo-rje gyis bsgyur."@en-x-mixed , "西藏艺术"@zh-hans ;
        skos:prefLabel      "bod ljongs kyi sgyu rtsal/"@bo-x-ewts .
    
    bdr:TT0863ECCACE6652B1
        a                   bdo:CoverTitle ;
        rdfs:label          "Bod ljoṅs kyi sgyu rtsal /Źuṅ-wun-pin gyis brtsoms ; Dros-dmar-tshe-riṅ-rdo-rje gyis bsgyur."@en-x-mixed .
    
    bdr:TT913B90BBEC7F36A0
        a                   bdo:Title ;
        rdfs:label          "bod ljongs kyi sgyu rtsal/"@bo-x-ewts .
    
    bdr:TTA6D2438AAD538B82
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "xi zang yi shu"@zh-latn-pinyin-x-ndia .
    
    bdr:TTD509705AFB01E1E1
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "西藏艺术"@zh-hans .
}
