@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW21902 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG4CA4E30CE6AD9212
        a                   adm:UpdateData ;
        adm:logDate         "2015-11-04T03:48:55.764000+00:00"^^xsd:dateTime ;
        adm:logMessage      "deleted inProduct PR1COPYRIGHT"@en ;
        adm:logWho          bdu:U00006 .
    
    bda:LG6D2E5AFB456A7FE4
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG7C87C2A8C68D81DA
        a                   adm:UpdateData ;
        adm:logDate         "2012-09-21T14:32:05.007000+00:00"^^xsd:dateTime ;
        adm:logMessage      "changed onhold to released"@en ;
        adm:logWho          bdu:U00016 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW21902  a          adm:AdminData ;
        adm:adminAbout      bdr:MW21902 ;
        adm:facetIndex      14 ;
        adm:gitPath         "62/MW21902.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW21902 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDSSNALYNIFEBKX , bda:LG4CA4E30CE6AD9212 , bda:LG6D2E5AFB456A7FE4 , bda:LG7C87C2A8C68D81DA , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EV45B3CA18421E3AF0
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1990,2000"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID91C9DD968EAF13B3
        a                   bdr:HollisId ;
        rdf:value           "014258493" .
    
    bdr:IDA2142A32F3D75013
        a                   bf:Lccn ;
        rdf:value           "2002477120" .
    
    bdr:IDAA3D97DB6155D71B
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BC25 .B76 1990 TIB" .
    
    bdr:IDMW21902_21R3V7P6T8RD
        a                   bf:Isbn ;
        rdf:value           "7542008439" .
    
    bdr:IDMW21902_OXMGM1C9RGG8
        a                   bf:Isbn ;
        rdf:value           "9787542008435" .
    
    bdr:MW21902  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID91C9DD968EAF13B3 , bdr:IDA2142A32F3D75013 , bdr:IDAA3D97DB6155D71B , bdr:IDMW21902_21R3V7P6T8RD , bdr:IDMW21902_OXMGM1C9RGG8 ;
        bdo:authorshipStatement  "dmu dge bsam gtan gyis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:contentMethod   bdr:ContentMethod_ComputerInput ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:editionStatement  "par gzhi gnyis pa/"@bo-x-ewts ;
        bdo:extentStatement  "3, 200 p." ;
        bdo:hasTitle        bdr:TT11B7A8D1AFC4F8C3 , bdr:TT1387B944FFEAA2BB , bdr:TTF9AF745FBAC4F744 ;
        bdo:instanceEvent   bdr:EV45B3CA18421E3AF0 ;
        bdo:instanceHasReproduction  bdr:W21902 ;
        bdo:instanceOf      bdr:WA21902 ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Modern ;
        bdo:publisherLocation  "zi ling /"@bo-x-ewts ;
        bdo:publisherName   "mtsho sngon mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "因明学入门"@zh-hans ;
        skos:prefLabel      "shes bya'i rnam grangs blo gsal rigs sgo"@bo-x-ewts .
    
    bdr:TT11B7A8D1AFC4F8C3
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "yin ming xue ru men"@zh-latn-pinyin-x-ndia .
    
    bdr:TT1387B944FFEAA2BB
        a                   bdo:CopyrightPageTitle ;
        rdfs:label          "因明学入门"@zh-hans .
    
    bdr:TTF9AF745FBAC4F744
        a                   bdo:Title ;
        rdfs:label          "shes bya'i rnam grangs blo gsal rigs sgo"@bo-x-ewts .
}
