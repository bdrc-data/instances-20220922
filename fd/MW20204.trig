@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW20204 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDSSNALYNIFEBKX
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/import/binding/add_binding.py" ;
        adm:logDate         "2025-01-15T16:49:50.611741"^^xsd:dateTime ;
        adm:logMessage      "add binding (batch)"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGC8F0B0083B16278C
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW20204  a          adm:AdminData ;
        adm:adminAbout      bdr:MW20204 ;
        adm:facetIndex      12 ;
        adm:gitPath         "fd/MW20204.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW20204 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDS5ZNKT5O6YISF , bda:LG0BDSSNALYNIFEBKX , bda:LGC8F0B0083B16278C , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVFBC68B3640E5F5CE
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1999"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID50BB6E573FAFC19C
        a                   bf:Lccn ;
        rdf:value           "2003323374" .
    
    bdr:IDB05E72EB44CDE007
        a                   bdr:HollisId ;
        rdf:value           "014258087" .
    
    bdr:IDE9FF14D0C43001CE
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ950.G47 T74 1999" .
    
    bdr:IDMW20204_EMQBUN6WHUV0
        a                   bf:Isbn ;
        rdf:value           "9787540922801" .
    
    bdr:IDMW20204_Q0CN87XVMJHV
        a                   bf:Isbn ;
        rdf:value           "754092280X" .
    
    bdr:MW20204  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID50BB6E573FAFC19C , bdr:IDB05E72EB44CDE007 , bdr:IDE9FF14D0C43001CE , bdr:IDMW20204_EMQBUN6WHUV0 , bdr:IDMW20204_Q0CN87XVMJHV ;
        bdo:authorshipStatement  "tshe ring dbang rgyal dang lcang zhabs pa 'gyur med tshe dbang gis brtsams/"@bo-x-ewts ;
        bdo:binding         bdr:Binding_Codex_Perfect ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:extentStatement  "2, 10, 129 p." ;
        bdo:hasTitle        bdr:TT49A54E8EB9C64A7B , bdr:TTA5EF672FA3DDF070 , bdr:TTF17AE864B43D294D ;
        bdo:instanceEvent   bdr:EVFBC68B3640E5F5CE ;
        bdo:instanceHasReproduction  bdr:W20204 ;
        bdo:instanceOf      bdr:WA20204 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "khreng tu'u/"@bo-x-ewts ;
        bdo:publisherName   "si khron mi rigs dpe skrun khang /"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "gengdunqunpei ping zhuan"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "dge 'dun chos 'phel/"@bo-x-ewts .
    
    bdr:TT49A54E8EB9C64A7B
        a                   bdo:Title ;
        rdfs:label          "dge 'dun chos 'phel/"@bo-x-ewts .
    
    bdr:TTA5EF672FA3DDF070
        a                   bdo:ColophonTitle ;
        rdfs:label          "gengdunqunpei ping zhuan"@zh-latn-pinyin-x-ndia .
    
    bdr:TTF17AE864B43D294D
        a                   bdo:ColophonTitle ;
        rdfs:label          "mkhas dbang dge 'dun chos 'phel skor la dpyad pa tshangs pa'i drang thig"@bo-x-ewts .
}
