@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW23167 {
    bda:LG0BDS7T15T6YORU18
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/batch-change.py" ;
        adm:logDate         "2024-05-16T08:10:13.218778"^^xsd:dateTime ;
        adm:logMessage      "add to PR1PL480"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG148CBB538A43AE5D
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LG7733FE21C306428F
        a                   adm:UpdateData ;
        adm:logDate         "2013-07-15T11:47:50.945000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added print type"@en ;
        adm:logWho          bdu:U00013 .
    
    bda:LGIM3B24E310  a     adm:UpdateData ;
        adm:logAgent        "pubinfo-update-lcCallNumber-fromCSV.xql" ;
        adm:logDate         "2019-06-17T21:07:27.001000+00:00"^^xsd:dateTime ;
        adm:logMessage      "correct mw:lcCallNumber field for pub info"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW23167  a          adm:AdminData ;
        adm:adminAbout      bdr:MW23167 ;
        adm:facetIndex      14 ;
        adm:gitPath         "57/MW23167.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW23167 ;
        adm:logEntry        bda:LG0BDS7T15T6YORU18 , bda:LG148CBB538A43AE5D , bda:LG7733FE21C306428F , bda:LGIM3B24E310 , bda:LGIM6B433BA01E , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:status          bda:StatusReleased .
    
    bdr:EVA9B06440EA5982EB
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "1985"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID0B3FB86E9DF842E8
        a                   bf:ShelfMarkLcc ;
        rdf:value           "BQ4860.C353 N36 1985" .
    
    bdr:ID30A38FC811B67834
        a                   bf:Lccn ;
        rdf:value           "85903340" .
    
    bdr:ID71C56689ADDFC8FD
        a                   bdr:HollisId ;
        rdf:value           "014258809" .
    
    bdr:IDC8A5B03F35DD495B
        a                   bdr:PL480 ;
        rdf:value           "i-tib-2744" .
    
    bdr:MW23167  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID0B3FB86E9DF842E8 , bdr:ID30A38FC811B67834 , bdr:ID71C56689ADDFC8FD , bdr:IDC8A5B03F35DD495B ;
        bdo:biblioNote      "reproduced from a rare manuscript from the library of yudra rinpoche"@en ;
        bdo:extentStatement  "251 p." ;
        bdo:hasTitle        bdr:TT1DBF28C2E543F50E , bdr:TT3B8EF5A00ABBE0DA ;
        bdo:inCollection    bdr:PR1PL480 ;
        bdo:instanceEvent   bdr:EVA9B06440EA5982EB ;
        bdo:instanceHasReproduction  bdr:W23167 ;
        bdo:instanceOf      bdr:WA23167 ;
        bdo:note            bdr:NT4FC16A10BED2BC8E ;
        bdo:numberOfVolumes  1 ;
        bdo:printMethod     bdr:PrintMethod_Manuscript ;
        bdo:publisherLocation  "bir, h.p."@en ;
        bdo:publisherName   "d. tsondu senghe"@en ;
        bdo:script          bdr:ScriptDbuMed ;
        skos:altLabel       "nam rdzon snan brgyud kyi skor : a collection of texts connected with the cakrasamvara practices orally transmitted through the masters in the lineage of the nam rdzon snan brgyud"@en-x-mixed ;
        skos:prefLabel      "ngam rdzong snyan rgyud kyi skor/"@bo-x-ewts .
    
    bdr:NT4FC16A10BED2BC8E
        a                   bdo:Note ;
        bdo:noteText        "tbrc holds scanned images, tiff and pdf files.\n0373"@en .
    
    bdr:TT1DBF28C2E543F50E
        a                   bdo:TitlePageTitle ;
        rdfs:label          "nam rdzon snan brgyud kyi skor : a collection of texts connected with the cakrasamvara practices orally transmitted through the masters in the lineage of the nam rdzon snan brgyud"@en-x-mixed .
    
    bdr:TT3B8EF5A00ABBE0DA
        a                   bdo:Title ;
        rdfs:label          "ngam rdzong snyan rgyud kyi skor/"@bo-x-ewts .
}
