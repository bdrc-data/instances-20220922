@prefix adm:   <http://purl.bdrc.io/ontology/admin/> .
@prefix bda:   <http://purl.bdrc.io/admindata/> .
@prefix bdg:   <http://purl.bdrc.io/graph/> .
@prefix bdo:   <http://purl.bdrc.io/ontology/core/> .
@prefix bdr:   <http://purl.bdrc.io/resource/> .
@prefix bdu:   <http://purl.bdrc.io/resource-nc/user/> .
@prefix bf:    <http://id.loc.gov/ontologies/bibframe/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix skos:  <http://www.w3.org/2004/02/skos/core#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .

bdg:MW25120 {
    bda:LG0BDS13QEGEUONMD5
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_isbn_mw.py" ;
        adm:logDate         "2024-01-22T15:42:49.265854"^^xsd:dateTime ;
        adm:logMessage      "update ISBN from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG0BDS5ZNKT5O6YISF
        a                   adm:UpdateData ;
        adm:logAgent        "buda-scripts/batch/import_publishers.py" ;
        adm:logDate         "2024-02-13T13:58:46.272633"^^xsd:dateTime ;
        adm:logMessage      "update publisher name / location from spreadsheet"@en ;
        adm:logMethod       bda:BatchMethod .
    
    bda:LG4C374F59FEBC6BFA
        a                   adm:UpdateData ;
        adm:logDate         "2007-11-27T16:24:26.539000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added note"@en ;
        adm:logWho          bdu:U00003 .
    
    bda:LG7DA82E75E0770C69
        a                   adm:InitialDataCreation ;
        adm:logWho          bdu:U00001 .
    
    bda:LGIM6B433BA01E
        a                   adm:UpdateData ;
        adm:logDate         "2014-04-22T00:30:13.577000+00:00"^^xsd:dateTime ;
        adm:logMessage      "normalize encoding field for pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMA45794A9D2
        a                   adm:UpdateData ;
        adm:logDate         "2014-05-02T18:19:14.334000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added seeHarvard to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00012 .
    
    bda:LGIMD5FAEE0C6C
        a                   adm:UpdateData ;
        adm:logDate         "2015-01-05T17:22:13.356000+00:00"^^xsd:dateTime ;
        adm:logMessage      "added hollis field to pub info"@en ;
        adm:logMethod       bda:BatchMethod ;
        adm:logWho          bdu:U00006 .
    
    bda:MW25120  a          adm:AdminData ;
        adm:adminAbout      bdr:MW25120 ;
        adm:facetIndex      19 ;
        adm:gitPath         "72/MW25120.trig" ;
        adm:gitRepo         bda:GR0012 ;
        adm:graphId         bdg:MW25120 ;
        adm:logEntry        bda:LG0BDS13QEGEUONMD5 , bda:LG0BDS5ZNKT5O6YISF , bda:LG4C374F59FEBC6BFA , bda:LG7DA82E75E0770C69 , bda:LGIM6B433BA01E , bda:LGIMA45794A9D2 , bda:LGIMD5FAEE0C6C ;
        adm:metadataLegal   bda:LD_BDRC_CC0 ;
        adm:restrictedInChina  true ;
        adm:status          bda:StatusReleased .
    
    bdr:EVEC2D87BEB596E195
        a                   bdo:PublishedEvent ;
        bdo:eventWhen       "2002"^^<http://id.loc.gov/datatypes/edtf/EDTF> .
    
    bdr:ID04EF62D819F8483E
        a                   bdr:HollisId ;
        rdf:value           "014259404" .
    
    bdr:ID231171B32C9B35B2
        a                   bdr:HarvardShelfId ;
        rdf:value           "009532069-5" .
    
    bdr:ID278EDB9215FB9351
        a                   bdr:HarvardShelfId ;
        rdf:value           "012191329-5" .
    
    bdr:IDMW25120_32ASO5Y1OCKI
        a                   bf:Isbn ;
        rdf:value           "9789624509212" .
    
    bdr:IDMW25120_UNAIWRA051KQ
        a                   bf:Isbn ;
        rdf:value           "9624509212" .
    
    bdr:MW25120  a          bdo:Instance ;
        bf:identifiedBy     bdr:ID04EF62D819F8483E , bdr:ID231171B32C9B35B2 , bdr:ID278EDB9215FB9351 , bdr:IDMW25120_32ASO5Y1OCKI , bdr:IDMW25120_UNAIWRA051KQ ;
        bdo:authorshipStatement  "smar 'ja' dam chos rgya mtshos brtsams/"@bo-x-ewts ;
        bdo:copyrightStatus  bdr:CopyrightInCopyright ;
        bdo:extentStatement  "192 p." ;
        bdo:hasTitle        bdr:TT1DF38769E038B66F , bdr:TT413D34CAF3B123ED , bdr:TT53C240BA944B8B99 , bdr:TTE343BD52D4257EF8 , bdr:TTE7D6886267E8C207 , bdr:TTF3F6B0CF772193E5 ;
        bdo:illustrations   "ill." ;
        bdo:instanceEvent   bdr:EVEC2D87BEB596E195 ;
        bdo:instanceHasReproduction  bdr:W25120 ;
        bdo:instanceOf      bdr:WA25120 ;
        bdo:note            bdr:NT3780A9F1D0CB6E35 ;
        bdo:numberOfVolumes  1 ;
        bdo:publisherLocation  "*zhang kang /"@bo-x-ewts ;
        bdo:publisherName   "zhang kang gyi ling dpe skrun kung si/"@bo-x-ewts ;
        bdo:script          bdr:ScriptTibt ;
        skos:altLabel       "history of maer kang"@en , "maerkang shi"@zh-latn-pinyin-x-ndia ;
        skos:prefLabel      "smar khams lo rgyus/"@bo-x-ewts .
    
    bdr:NT3780A9F1D0CB6E35
        a                   bdo:Note ;
        bdo:noteText        "history of the smar khams area and nomadic group in the area of bya gzhung in amdo"@en .
    
    bdr:TT1DF38769E038B66F
        a                   bdo:ColophonTitle ;
        rdfs:label          "maerkang shi"@zh-latn-pinyin-x-ndia .
    
    bdr:TT413D34CAF3B123ED
        a                   bdo:CoverTitle ;
        rdfs:label          "history of maer kang"@en .
    
    bdr:TT53C240BA944B8B99
        a                   bdo:CoverTitle ;
        rdfs:label          "Gaṅs rgyal Ti-ser phul baʼi glu dbyaṅs /Rṅa-ba Kun-grol-rgya-mtsho."@en-x-mixed .
    
    bdr:TTE343BD52D4257EF8
        a                   bdo:Title ;
        rdfs:label          "smar khams lo rgyus/"@bo-x-ewts .
    
    bdr:TTE7D6886267E8C207
        a                   bdo:TitlePageTitle ;
        rdfs:label          "bya stod smar khams gsang sngags bstan pa'i dpal khyim gyi lo rgyus lugs gnyis dar rgud gsal ba'i me long /"@bo-x-ewts .
    
    bdr:TTF3F6B0CF772193E5
        a                   bdo:CoverTitle ;
        rdfs:label          "Laṅ tshoʾi mchod yon /Gʾyaṅ-ʾbum-tshe-riṅ gis brtsams."@en-x-mixed .
}
